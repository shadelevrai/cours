const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const cors = require('cors');
const mongoose = require('mongoose');
const port = 8880;
const rateLimit = require("express-rate-limit");
const xss = require('xss-clean')
//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;
//Configure isProduction variable
// const isProduction = process.env.NODE_ENV === 'production';

//Initiate our app
const app = express();

//Configure our app
app.use(express.json({ limit: '100kb' })); // Body limit is 10
app.use(cors());
app.use(xss())
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'passport-tutorial',
    cookie: {
        maxAge: 60000
    },
    resave: false,
    saveUninitialized: false
}));
app.use('/routeName', rateLimit({
    max: 1000,// max requests
    windowMs: 60 * 60 * 1000, // 1 Hour
    message: 'Too many requests' // message to send
})); // Setting limiter on specific route

//Configure Mongoose
mongoose.connect('mongodb://cernnunos:Zmfgvknrzs11@ds141209.mlab.com:41209/jeux', {
    useNewUrlParser: true
});
mongoose.set('debug', true);

//Models and routes
require('./models/Users');
require('./models/Djohers');
require('./models/UsersJennyfers');
require('./models/userAnimeLists');
require('./models/userParagonTeams');
require('./models/paragonSessions');
require('./config/passport');
require('./config/passportjennyfer');
require('./config/passportanimelist');
require('./config/passportParagonTeam');
require('./config/passportParagonSession');
require('./config/passportDjoher');

app.use(require('./routes'))

app.use((err, req, res) => {
    res.status(err.status || 500);

    res.json({
        errors: {
            message: err.message,
            error: {},
        },
    });
});

app.listen(port, () => console.log(`Server running on http://localhost:${port}/`));