const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local');

const UsersParagonTeams = mongoose.model('UsersParagonTeams');

passport.use(new LocalStrategy({
  usernameField: 'user[email]',
  passwordField: 'user[password]',
}, (email, password, done) => {
    UsersParagonTeams.findOne({ email })
    .then((user) => {
      if(!user || !user.validatePassword(password)) {
        return done(null, false, { errors: { 'email or password': 'is invalid' } });
      }

      return done(null, user);
    }).catch(done);
}));