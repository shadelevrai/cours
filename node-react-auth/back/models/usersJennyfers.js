const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const UsersJennyfersSchema = new Schema({
  email: String,
  hash: String,
  basket: Array,
  salt: String,
});

UsersJennyfersSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UsersJennyfersSchema.methods.validatePassword = function (password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.hash === hash;
};

UsersJennyfersSchema.methods.generateJWT = function () {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);

  return jwt.sign({
    email: this.email,
    id: this._id,
    exp: parseInt(expirationDate.getTime() / 1000, 10),
  }, 'secret');
}

UsersJennyfersSchema.methods.toAuthJSON = function () {
  return {
    _id: this._id,
    email: this.email,
    basket: { item: [{ id: '456456456', name: 'chaussures' }, { id: '78789', name: 'chapeaux' }] },
    token: this.generateJWT(),
  };
};

mongoose.model('UsersJennyfers', UsersJennyfersSchema);