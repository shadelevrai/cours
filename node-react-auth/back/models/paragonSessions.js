const mongoose = require('mongoose');

const {
    Schema
} = mongoose;

const ParagonSessionsSchema = new Schema({
    game: String,
    date: String,
    hour: String,
    dateCreation: String,
    creator:{type:Schema.Types.ObjectId, ref:'UsersParagonTeams'}
});
// },{strict:false});

ParagonSessionsSchema.methods.toAuthJSON = function () {
    return {
        _id: this._id,
        game: this.game,
        date: this.date,
        hour: this.hour,
        dateCreation: this.dateCreation,
        creator:this.creator
    };
};

mongoose.model('ParagonSessions',ParagonSessionsSchema)