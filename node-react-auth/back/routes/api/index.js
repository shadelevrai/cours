
const express = require('express');
const router = express.Router();

router.use('/users', require('./users'));
router.use('/usersJennyfers', require('./usersJennyfers'));
router.use('/usersAnimeLists', require('./usersAnimeLists'));
router.use('/djohers', require('./djohers'));
router.use('/usersParagonTeams', require('./usersParagonTeams'));
router.use('/paragonSessions', require('./paragonSessions'));

module.exports = router;