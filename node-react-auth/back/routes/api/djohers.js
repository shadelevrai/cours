const mongoose = require('mongoose');
const moment = require('moment')
const Djohers = mongoose.model('Djohers')
// const passport = require('passport');
const router = require('express').Router();
const multer = require('multer')

const hardward = ['Playstation 4', 'Xbox One', 'Switch']
const heading = ['News', 'Test', 'Sorties']

let name;

let number = 0;

//GET current route (required, only authenticated users have access)
router.get('/langage', (req, res) => {

    const langage = [
        {
            "name": "Node",
            "link": "node"
        }, {
            "name": "Mongo",
            "link": "mongo"
        }, {
            "name": "Socket.io",
            "link": "socketio"
        }, {
            "name": "React",
            "link": "react"
        }, {
            "name": "Angular",
            "link": "angular"
        }, {
            "name": "Vue",
            "link": "vue"
        }, {
            "name": "Git",
            "link": "git"
        },{
            "name": "CSS",
            "link": "css"
        },{
            "name": "Javascript",
            "link": "javascript"
        }
    ]
    res.json(langage)
});

router.get('/category', (req, res) => {
    const category = ["Tuto", "Astuce"]
    console.log('category a été appellé')
    res.json(category)
})

router.get('/hardware', (req, res) => {
    console.log('hardware a été appellé')
    res.json(hardward)
});

router.get('/heading', (req, res) => {
    console.log('heading a été appellé')
    res.json(heading)
})

router.post('/article', (req, res) => {
    console.log('article a été appellé')
    const {data} = req.body
    data.createDate = moment().format('L');
    console.log("data", data)
    const newArticle = new Djohers(data)
    name = data.title
    res
        .status(200)
        .end()
    return newArticle.save()
})

router.get('/articlePreview', (req, res) => {
    console.log('articlePreview a été appellé')
    Djohers.find((err, data) => {
        if (err) 
            return console.log(err)
            number++;
            console.log(number)
        return res.json(data)
    })
})

router.post('/upload-image', (req, res) => {
    
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'public')
        },
        filename: function (req, file, cb) {
            cb(null,
            /*Date.now() + '-' + file.originalname*/
            name + '.png')
        }
    })
    const upload = multer({storage: storage}).single('file')

    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res
                .status(500)
                .json(err)
        } else if (err) {
            return res
                .status(500)
                .json(err)
        }
        return res
            .status(200)
            .end()

    })
})

router.get('/article', (req, res) => {
    console.log('article a été appellé')
    const {titleLink} = req.query
    Djohers
        .find({titleLink})
        .then(article => {
            if (!article) {
                return res.sendStatus(400);
            }
            console.log("article", article)
            return res.json(article)
        })
})

router.get('/all-article', (req, res) => {
    console.log('article a été appellé')
    Djohers
        .find()
        .then(article => {
            if (!article) {
                return res.sendStatus(400);
            }
            console.log("article", article)
            return res.json(article)
        })
})

router.post('/upload-image-article',(req,res)=>{
    const storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'public')
        },
        filename: function (req, file, cb) {
            cb(null, file.originalname)
        }
    })

    const upload = multer({storage: storage}).single('file')
    
    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res
                .status(500)
                .json(err)
        } else if (err) {
            return res
                .status(500)
                .json(err)
        }
        return res
            .status(200)
            .end()

    })
})

module.exports = router;