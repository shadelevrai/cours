const mongoose = require('mongoose');
const router = require('express').Router();
const auth = require('../auth');
var format = require('date-format');


const ParagonSessions = mongoose.model('ParagonSessions')

router.post('/create-session', auth.required, (req, res) => {

    const {
        payload: {
            id
        }
    } = req;

    const {
        game,
        date,
        hour
    } = req.body

    const newSession = new ParagonSessions({
        game,
        date,
        hour,
        dateCreation: format.asString(new Date()),
        creator:id
    })
    return newSession.save()
        .then(()=>res.json({
            session: newSession.toAuthJSON()
        }))
})

module.exports = router;