const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('../auth');
const UsersAnimeLists = mongoose.model('UsersAnimeLists');

//POST new user route (optional, everyone has access)
router.post('/', auth.optional, (req, res, next) => {
    const { body: { user } } = req;

    if (!user.email) {
        return res.status(422).json({
            errors: {
                email: 'is required',
            },
        });
    }

    if (!user.password) {
        return res.status(422).json({
            errors: {
                password: 'is required',
            },
        });
    }

    UsersAnimeLists.findOne({ email: user.email }, {}, function (err, userFind) {
        if (err) console.log(err)
        if (userFind) {
            return res.status('422').json({
                errors: {
                    email: 'is exist'
                }
            })
        } else {
            const finalUser = new UsersAnimeLists(user);
            finalUser.setPassword(user.password);
            return finalUser.save()
                .then(() => res.json({ user: finalUser.toAuthJSON() }));
        }
    })
});

//POST login route (optional, everyone has access)
router.post('/login', auth.optional, (req, res, next) => {

    const { body: { user } } = req;

    if (!user.email) {
        return res.status(422).json({
            errors: {
                email: 'is required',
            },
        });
    }

    if (!user.password) {
        return res.status(422).json({
            errors: {
                password: 'is required',
            },
        });
    }


    return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
        if (err) {
            console.log(info)
            return res.status(422).json({
                errors: {
                    message: 'Wrong log',
                },
            })
        }

        if (passportUser) {
            const user = passportUser;
            user.token = passportUser.generateJWT();

            return res.json({ user: user.toAuthJSON() });
        }

        return status(400).info;
    })(req, res, next);
});

//GET current route (required, only authenticated users have access)
router.get('/current', auth.required, (req, res, next) => {
    const { payload: { id } } = req;

    return UsersAnimeLists.findById(id)
        .then((user) => {
            if (!user) {
                console.log(id)
                return res.sendStatus(400);
            }

            return res.json({ user: user.toAuthJSON() });
        });
});

router.put('/upload-anime', auth.required, (req, res, next) => {
    const { payload: { id } } = req;
    UsersAnimeLists.findById(id)
        .then((user) => {
            user.anime.push(req.body)
            user.save((err, data) => {
                if (err) console.log(err)
                return res.json({ user })
            })
        })
})

router.put('/upload-episode', auth.required, (req, res, next) => {
    const { mal_id, episode } = req.body
    const { payload: { id } } = req;
    UsersAnimeLists.findById(id)
        .then((user) => {
            // if (user) {
                // console.log(user.anime[0].episode_seen)
                // user.anime.forEach(element => {
                //     console.log(element)
                // });

                // console.log(user)
                for (let i = 0; i < user.anime.length; i++) {
                    if (user.anime[i].mal_id === mal_id) {
                        user.anime[i].episode_seen[episode] = true
                        // console.log(user.anime[0])
                        // console.log(user.anime[1])
                    }
                }

                // user.anime.filter(anime=>anime.mal_id === mal_id).map(anime=>{
                //     anime.episode_seen[episode] = true
                //     console.log(anime);

                // })

                // let a = user.anime.find((item)=>item.mal_id === mal_id)
                // console.log(a)
            // }
             user.save((err, data) => {
                if (err) console.log(err)
                return res.json({ user })
            })
        })
})

module.exports = router;