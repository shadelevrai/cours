import React from 'react';
import LogIn from './Component/LogIn'
import SignUp from './Component/SignUp'
import Home from './Component/Home'
import { BrowserRouter as Router, Route } from "react-router-dom"

function App() {
  return (
    <div>
      <Router>
        <Route exact path='/' component={LogIn} />
        <Route exact path='/signup' component={SignUp} />
        <Route exact path='/home' component={Home}/>
        </Router>
    </div>
  );
}

export default App;
