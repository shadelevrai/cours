import React from 'react'
import Cookies from 'universal-cookie';
const cookies = new Cookies();

export default class SignUp extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            email: '',
            password: '',
            passwordVerify: '',
            infoWarning: ''
        }
    }

    componentDidUpdate(){
        console.log(this.state)
    }

    postSubmit = e =>{
        e.preventDefault();
        (async () => {
            const rawResponse = await fetch('/api/users/', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    'user': {
                        'email': this.state.email,
                        'password': this.state.password
                    }
                })
            })
            if (rawResponse.status === 422) {
                return this.setState({
                    email: '',
                    password: '',
                    infoWarning: 'Incorrect. Maybe email taken'
                })
            }
            const content = await rawResponse.json();
            console.log(content);
            cookies.set('myToken', content.user.token, {
                path: '/'
            });
            this.props.history.push('/'); 
        })();
    }
    render(){
        return(
            <div>
                <div>
                    <form onSubmit={this.postSubmit}>
                        <div>Mail :</div>
                        <input type='text' value={this.state.email} onChange={e=>{this.setState({email:e.target.value})}}/>
                        <div>Password</div>
                        <input type='password' value={this.state.password} onChange={e=>{this.setState({password:e.target.value})}} />
                        <div>Retaper votre password</div>
                        <input type='password' value={this.state.passwordVerify} onChange={e=>{this.setState({passwordVerify:e.target.value})}}/>
                        <button type='submit' disabled={!this.state.email || !this.state.password || this.state.password !== this.state.passwordVerify}>Envoyer</button>
                        {this.state.infoWarning}
                    </form>
                </div>
            </div>
        )
    }
}