import React from 'react'
import { Link } from "react-router-dom"
import Cookies from 'universal-cookie';
const cookies = new Cookies();

export default class LogIn extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            email: '',
            password: '',
            infoWarning: ''
        }
    }

    componentWillMount() {

        //inscription
        // (async () => {
        //     const rawResponse = await fetch('/api/users', {
        //       method: 'POST',
        //       headers: {
        //         'Accept': 'application/json',
        //         'Content-Type': 'application/json'
        //       },
        //       body: JSON.stringify({"user":{
        //                     "email":"shadethejewel@gmail.cri",
        //                     "password": "test"
        //                 }})
        //     });
        //     const content = await rawResponse.json();

        //     console.log(content);
        //   })();

        // get pour récup les infos utilisateurs
        // fetch('/api/users/current')
        // .then(data => data.json())
        // .then((data)=>console.log(data))

        //post pour se connecter
        // (async () => {
        //     const rawResponse = await fetch('/api/users/login', {
        //         method: 'POST',
        //         headers: {
        //             'Accept': 'application/json',
        //             'Content-Type': 'application/json'
        //         },
        //         body: JSON.stringify({
        //             "user": {
        //                 "email": "cernnunos@hotmail.fr",
        //                 "password": "test"
        //             }
        //         })
        //     })
        //     const content = await rawResponse.json();
        //     console.log(content);
        //     cookies.set('myCook', content.user.token, { path: '/' });
        //     console.log(cookies.get('myCook'));
        // })();
    }

    postSubmit = (e) => {
        e.preventDefault();
        (async () => {
            const rawResponse = await fetch('/api/users/login', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "user": {
                        "email": this.state.email,
                        "password": this.state.password
                    }
                })
            })
            if (rawResponse.status === 422) {
                return this.setState({
                    email: '',
                    password: '',
                    infoWarning: 'Log is incorrect'
                })
            }
            const content = await rawResponse.json();
            cookies.set('myToken', content.user.token, { path: '/' });
            // console.log(cookies.get('myCook'));
            this.props.history.push('/home');
        })();
    }

    componentDidUpdate(){
        console.log(this.state)
    }
    
    render(){
        return(
            <div>
                <div>Se connecter</div>
                <div>{this.state.infoWarning}</div>
                <div>
                    <form onSubmit={this.postSubmit}>
                        <div>Mail :</div>
                        <input type='text' value={this.state.email} onChange={e=>this.setState({email:e.target.value})} />
                        <div>Password :</div>
                        <input type='password' value={this.state.password} onChange={e=>this.setState({password:e.target.value})} />
                        <button type='submit' disabled={!this.state.email || !this.state.password}>Log in</button>
                    </form>
                    <div>
                        <p>Pas de compte ? <Link to='/signup'>Cliquez ici</Link></p>
                    </div>
                </div>
            </div>
        )
    }
}