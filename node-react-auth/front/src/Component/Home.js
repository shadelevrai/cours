import React from 'react'
import Cookies from 'universal-cookie';
const cookies = new Cookies();

export default class Home extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            isConnected:false
        }
    }

    componentWillMount(){
        this.checkIfConnected();
    }

    checkIfConnected = () =>{
        fetch('/api/users/current',{
            method: 'GET',
            headers : {
                'Authorization': 'Token '+ cookies.get('myToken'),
                'Content-type': 'application/json'
            }
        })
        .then(res => {
            if(res.status === 401){
                return this.setState({isConnected:false})
            }
            return res.json()
        })
        .then(data=>{
            console.log(data)
        })
    }

    disconnect=()=>{
        cookies.remove('myToken');
        this.props.history.push('/');
    }

    render(){
        return(
            <div>
                <div>{this.state.isConnected ? 'connecté' : 'Non connecté'}</div>
                <div>
                    <div onClick={this.disconnect}>Se déconnecter</div>
                </div>
            </div>
        )
    }
}