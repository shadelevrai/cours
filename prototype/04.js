// on créé un objet littéral
var o = {a: "b", b: "c"};

// on ajoute une propriété à son prototype. Attention, ça ne rajoute pas la propriété d dans l'objet o, mais ça le rajoute dans son prototype. Le prototype est une espèce de sous objet dans l'objet.
o.__proto__ = {d: "e"};

// si la propriété d n'est pas trouvé dans l'objet o, alors la recherche s'effectuera dans son prototype (espèce de objet enfant).
console.log(o.d); // "e"

// vérification des propriétés propres (donc dans l'objet o et non ses prototype). La recherche se fait avec la function hasOwnProperty()
console.log(o.hasOwnProperty('a')); // true parce que la propriété a existe dans l'objet o
console.log(o.hasOwnProperty('d')); // false parce que la propriété d existe pas dans l'objet d

// ce console.log va afficher que les propriétés de l'objet o, et non ses prototype.
console.log(o);

//La chaine de prototype ressemble ici à ça : 
//{a: "b", b: "c"} --> {d: "e"} --> Object.prototype --> null

//Par ailleurs, notez bien que nous utilisons dans l’exemple __proto__ comme un setter, ce qui peut notablement impacter les performances. Dans la mesure du possible, on préférera recourir à d’autres méthodes, nous verrons plus loin comment définir le prototype.