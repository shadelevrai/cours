let primitif = 'un'; //Ceci est une variable primitive
let objectString = new String('un'); //Ceci est une variable objet
let objNumber = new Number(4);

console.log(typeof primitif); // "string"
console.log(typeof objectString); // "object"
console.log(typeof objNumber); // "object"
console.log(objNumber); // "Number {4}"


if (primitif === objectString) {
    console.log('==='); // n'affiche rien car === est une comparaison stricte. Aucune conversion n'est faite. Ce qui va être comparé, c'est le typeof (donc string === object)
}

if (primitif == objectString) {
    console.log('=='); // la console.log sera affichée car la comparaison est faire après conversion. Certes, les deux variables sont sont pas du même genre mais elles ont les mêmes données, à savoir "un"
}

// les variable primitives sont évaluées comme du code source
let prim = '2 * 6';
console.log(eval(prim)); // renvoie le nombre 12

// les objets comme des string
let obj = new String('2 * 6');
console.log(eval(obj)); // renvoie la chaîne "6 * 6"