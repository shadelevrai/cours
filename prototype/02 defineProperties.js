//
var mini = {
    model: 'mini',
    make: 'bmw',
    hp: 120,
    color: 'blue',
    tires: '17"'
};

//On peut mettre des propriétés dans les propriétés de l'objet mini
Object.defineProperties(mini, {
    model: {
        enumerable: false // enumerable rend la propriété model caché si elle est en false
    },
    make : {
        enumerable : true // cette fois, avec enumerable en true, elle est affichée
    },
    hp: {
        writable: false // writable est une propriété qui rend le hp pas modifiable si elle est en false
    }
});

// console.log(mini);


// liste toutes les propriétés sauf "model" car elle est en enumerable:false
for (let prop in mini) {
    console.log(prop);
}

mini.hp = 200; // on tente de modifier "hp"
console.log(mini.hp); // 120 … hp n'est pas modifiable car elle est en writable:false