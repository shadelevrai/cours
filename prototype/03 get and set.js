var mini = {
    model: 'mini',
    make: 'bmw',
    hp: 120,
    color: 'blue',
    tires: '17"',
    get getColor() {
        // on utilise ici la syntaxe ES6 des template literals
        return `${this.model}'s color is ${this.color}`;
    },
    set paint(newColor) {
        this.color = newColor;
    }
};

console.log(mini.getColor); // "mini's color is blue"

// vous remarquez qu'on ne l'appelle pas comme une fonction !
mini.paint = 'red';//On envoit une nouvelle valeur dans le set paint() et elle se lance, ce qui change color de l'objet mini
console.log(mini.getColor); // "red"

//Les setters et getters possèdent eux-mêmes des attributs, deux pour être exacts, il s’agit de [[Enumerable]] et de [[Configurable]], on les configure exactement de la même manière que que pour les autres attributs, avec Object.defineProperties.