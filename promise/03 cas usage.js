const c = console.log

const makeSandwich = () => {
    getChicken(function (chicken) {
        cookChicken(chicken, function (cookedChicken) {
            getBuns(function (buns) {
                putChickenBetweenBuns(buns, chicken, function (sandwich) {
                    console.log('what an awesome sandwich');
                });
            });
        });
    });
};

makeSandwich(function(sandwich){
    serve(sandwich)
})

//   EXO : FAIRE LA SYNTAXE PROMISE
// Ce que j'ai fait
makeSandwich()
    .then(getChicken(chicken))
    .then(cookChicken(cookChicken))
    .then(getBuns(buns))
    .then(putChickenBetweenBuns(sandwich))

    // corrigé



//   Ensuite en async await

// Corrigé