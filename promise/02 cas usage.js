function resolveAfter2secondes(x){
    return new Promise(resolve =>{
        setTimeout(() => {
            resolve(x)
        }, 2000);
    })
}

var add = async function (x) {
    var a = await resolveAfter2secondes(20)
    var b = await resolveAfter2secondes(30)
    return x + a + b;
}

add(10).then(v=>{
    console.log(v)
})