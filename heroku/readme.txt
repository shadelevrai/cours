Avant tout, il faut un "start":"node app.js" dans le package - script

1/ installer les programmes heroku et git
2/ se connecter sur le site heroku et créer une nouvelle adresse
3/ se connecter en ligne de commande : $ heroku login (pas en bash mais en cmd)
4/ faire $ heroku git:clone -a nomDeAdresse
5/ se mettre dans le dossier et faire $ git add .
6/ puis faire un commit : $ git commit -am "make it better"
7/ push : $ git push heroku master
8/ faire heroku ps:scale web=1
9/ Faire heroku open