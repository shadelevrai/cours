

var EventEmitter = require('events').EventEmitter;

var jeu = new EventEmitter();

//on écoute et on attend 'gameover'
jeu.on('gameover', function(message){
    console.log(message);
});

jeu.emit('gameover', 'Vous avez perdu !');

// La différence entre ces deux méthode, c'est que l'une va être appelé si le serveur est lancé, l'autre se lance toujours

jeu = function (p1){
    console.log(p1);
}

jeu('vous avez encore perdu');