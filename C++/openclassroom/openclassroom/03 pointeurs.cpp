#include<iostream>

using namespace std;

int main03()
{
	int ageUtilisateur(16);  //D�claration d'une variable.

	int& maVariable(ageUtilisateur); //D�claration d'une r�f�rence nomm�e maVariable qui est accroch�e � la variable ageUtilisateur

	cout << &maVariable << endl;

	return 0;

}

/*
La r�f�rence doit imp�rativement �tre du m�me type que la variable � laquelle elle est accroch�e ! Un int& 
ne peut faire r�f�rence qu'� un int, 
de m�me qu'unstring&ne peut �tre associ� qu'� une variable de type string.
*/
