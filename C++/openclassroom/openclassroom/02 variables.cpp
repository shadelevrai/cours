#include<iostream>

using namespace std;

int main02()
{
	/*
	bool : Une valeur parmi deux possibles, vrai (true) ou faux (false).
	char : Un caract�re.
	int : Un nombre entier.
	unsigned int : Un nombre entier positif ou nul.
	double : Un nombre � virgule.
	string : Une cha�ne de caract�res, c'est-�-dire un mot ou une phrase.
	*/
	int ageUtilisateur(16);
	int nombreAmis(432);      //Le nombre d'amis de l'utilisateur
	int variableSansValeur;

	double pi(3.14159);

	bool estMonAmi(true);    //Cet utilisateur est-il mon ami ?

	char lettre('a');

	string phrase("lala papa");

	cout << "Voici la phrase : " << phrase << endl;

	int a(2), b(4), c(-1);  //On d�clare trois cases m�moires nomm�es a, b et c et  qui contiennent respectivement les valeurs 2, 4 et -1

	string prenom("Albert"), nom("Einstein"); //On d�clare deux cases pouvant contenir des cha�nes de caract�res
	return 0;
}