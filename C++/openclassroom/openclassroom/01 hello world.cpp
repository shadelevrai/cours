#include<iostream>

using namespace std; // Indique quel espace de noms on va utiliser

int main01()
{
	//cout est une fonctionnalit� de iostream qui affiche un message dans la console
	//endl fait un retour � la ligne
	cout << "Bonjour tout le monde !" << endl << "Comment allez-vous ?" << endl;
	return 0;
}

/*
    On distingue deux types de programmes : les programmes graphiques (GUI) et les programmes console.

    Il est plus simple de r�aliser des programmes console pour commencer, c'est donc ce type de programme que nous �tudierons en premier.

    Un programme poss�de toujours une fonction main(): c'est son point de d�marrage.

    La directive coutpermet d'afficher du texte dans une console.

    On peut ajouter des commentaires dans son code source pour expliquer son fonctionnement. 
*/