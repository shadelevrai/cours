const data = [{
    "name": "sparrow",
    "level": 1,
    "Damage": 45,
    "Attack_speed": 1.05,
    "Critical": 1,
    "Damage_evolution_per_level": 6,
    "Attack_Speed_evolution_per_level": 0.018,
    "evolution": [
        {
            "name": "super-sparrow",
            "begin": 18,
            "Damage_evolution_per_level": 10
        }
    ]
}, {
    "name": "serath",
    "level": 1,
    "Damage": 45,
    "Attack_speed": 1.07,
    "Critical": 1,
    "Damage_evolution_per_level": 6,
    "Attack_Speed_evolution_per_level": 0.018,
    "evolution": [
        {
            "name": "super-serath",
            "begin": 15,
            "Damage_evolution_per_level": 9
        }
    ]
},{
    "name": "Nick",
    "level": 1,
    "Damage": 49,
    "Attack_speed": 1.01,
    "Critical": 2,
    "Damage_evolution_per_level": 8,
    "Attack_Speed_evolution_per_level": 0.011,
    "evolution": [
        {
            "name": "super-Nick",
            "begin": 14,
            "Damage_evolution_per_level": 10
        }
    ]
}]

//Exo 1 : Afficher dans un console log le nom du premier personnage dans un console log
console.log("nom du perso :",data[0].name)

//Exo 2 : Afficher tous les noms des personnages dans un console log
console.log("tous les noms des persos",nom)

//Exo 3 : Afficher tous les noms des personnages dans un console log, sauf le second

//Exo 4  Faire une fonction qui "return" tous les noms des personnages.

//Exo 5 : Faire une fonction qui nous dit (en console log) si le nom du personnage existe dans les données. Cette fonction devra prend un parametre. Ce parametre c'est le nom du personnage qu'on lui donne.

//Exo 6 : Afficher en console log le nom des personnages et leur attack speed respectif

//Exo 7 : Faire une fonction qui return le nom du perso qui a le plus grand attack speed