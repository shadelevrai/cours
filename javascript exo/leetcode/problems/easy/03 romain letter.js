var romanToInt = function (s) {

    let number = 0

    const letters = {
        I: 1,
        V: 5,
        X: 10,
        L: 50,
        C: 100,
        D: 500,
        M: 1000
    }

    for (let index = 0; index < s.length; index++) {
        let currentInt = letters[s.charAt(index)]
        // console.log("🚀 ~ file: 03 romain letter.js ~ line 19 ~ romanToInt ~ currentInt", currentInt)
        let nextInt = s[index + 1] ? letters[s.charAt(index + 1)] : null
        // console.log("🚀 ~ file: 03 romain letter.js ~ line 21 ~ romanToInt ~ nextInt", nextInt)

        if (currentInt >= nextInt) {
            number += currentInt
        } else {
            const num = currentInt - nextInt
            number = number - num
            index++
        }
        // console.log("🚀 ~ file: 03 romain letter.js ~ line 58 ~ romanToInt ~ number", number)
        console.log("------------------------------------")
    }

    // console.log("🚀 ~ file: 03 romain letter.js ~ line 58 ~ romanToInt ~ number", number)
    return number
};

console.log(romanToInt("III") === 3)
console.log(romanToInt("IV") === 4)
console.log(romanToInt("VI") === 6)
console.log(romanToInt("LVIII") === 58)
console.log(romanToInt("MCMXCIV") === 1994)