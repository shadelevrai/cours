/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function (x) {
    if (Math.sign(x) === -1) return false
    const numberString = x.toString()
    const numberStringArray = []
    const numberStringArrayNeverChanged = []
    for (let index = 0; index < numberString.length; index++) {
        numberStringArray.push(numberString[index]);
        numberStringArrayNeverChanged.push(numberString[index])
    }
    const numberStringReverse = numberStringArray.reverse()
    if (JSON.stringify(numberStringArrayNeverChanged) === JSON.stringify(numberStringReverse)) {
        return true
    } else {
        return false
    }

    // const lengthString = numberString.length
    // let numberfinal = lengthString - 1
    // if (lengthString % 2 === 0) {
    //     console.log("la longueur du nombre est pair")
    //     console.log("🚀 ~ file: 02 palindrome.js ~ line 13 ~ isPalindrome ~ numberfinal", numberfinal)
    //     for (let index = 0; index < numberString.length / 2; index++) {
    //         console.log("je suis dans la boucle ", index, " et ", numberfinal)
    //         if (numberString[index] === numberString[numberfinal]) {
    //             res = true
    //         } else {
    //             res = false
    //         }
    //         numberfinal = numberfinal - 1
    //     }
    // } else {
    //     console.log("la longueur du nombre est impair")
    //     console.log("🚀 ~ file: 25 palindrome.js ~ line 13 ~ isPalindrome ~ numberfinal", numberfinal)
    //     for (let index = 0; index < numberString.length; index++) {
    //         console.log("je suis dans la boucle ", index, " et ", numberfinal)
    //         if (index === parseInt(lengthString / 2) + 1) {

    //         } else {
    //             if (numberString[index] === numberString[numberfinal]) {
    //                 res = true
    //             } else {
    //                 res = false
    //             }
    //         }
    //         numberfinal = numberfinal - 1
    //     }
    // }

};

console.log(isPalindrome(121))
console.log("----------------")
console.log(isPalindrome(-121))
console.log("----------------")
console.log(isPalindrome(10))
console.log("----------------")
console.log(isPalindrome(1000021))
console.log("----------------")
console.log(isPalindrome(1001))
console.log("----------------")
console.log(isPalindrome(100001))
console.log("----------------")
console.log(isPalindrome(121005544))

console.log(isPalindrome(100))
console.log("----------------")
