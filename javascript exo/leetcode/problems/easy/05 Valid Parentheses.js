/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function (s) {

    let bracket = {
        "(": ")",
        "[": "]",
        "{": "}"
    }
    let heap = []

    for (let char of s) {
        if (bracket[char]) {
            heap.push(bracket[char])
        } else {
            if (heap.pop() !== char) return false
        }
    }
    return (!heap.length)

    // const charas = [{
    //     chara: "(",
    //     number: 1,
    //     place: "start"
    // }, {
    //     chara: ")",
    //     number: 1,
    //     place: "end"
    // }, {
    //     chara: "{",
    //     number: 2,
    //     place: "start"
    // }, {
    //     chara: "}",
    //     number: 2,
    //     place: "end"
    // }, {
    //     chara: "[",
    //     number: 3,
    //     place: "start"
    // }, {
    //     chara: "]",
    //     number: 3,
    //     place: "end"
    // }]

    // if (s.length < 2) {
    //     return false
    // }

    // const allResponsePlace = [];
    // let response
    // // const arrayString = s.split('');
    // const arrayForNumber = []
    // for (let index = 0; index < s.length; index++) {
    //     for (let index2 = 0; index2 < charas.length; index2++) {
    //         if (s[index] === charas[index2].chara) {
    //             arrayForNumber.push(charas[index2].number)
    //             allResponsePlace.push(charas[index2].place)
    //         }
    //     }
    // }
    // if (allResponsePlace.filter(place => place === "start").length !== allResponsePlace.filter(place => place === "end").length) {
    //     return false
    // }

    // if (JSON.stringify(arrayForNumber) === JSON.stringify(arrayForNumber.slice().reverse())) {
    //     response = true
    // } else {
    //     for (let index = 0; index < s.length; index++) {
    //         let currentInt = arrayForNumber[index]
    //         let nextInt = arrayForNumber[index + 1]
    //         if (currentInt === nextInt && response !== false) {
    //             response = true
    //             index++
    //         } else {
    //             response = false
    //         }
    //     }
    // }


    // return response

};

console.log(isValid("()[]{}"))
console.log(isValid("()[]{"))
console.log(isValid("{[]}"))
console.log(isValid("({[]})"))
console.log(isValid("["))
console.log(isValid("(("))
console.log(isValid("(){}}{"))