/* Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

 

Example 1:

Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].

Example 2:

Input: nums = [3,2,4], target = 6
Output: [1,2]

Example 3:

Input: nums = [3,3], target = 6
Output: [0,1]

 

Constraints:

    2 <= nums.length <= 104
    -109 <= nums[i] <= 109
    -109 <= target <= 109
    Only one valid answer exists.
 */

var twoSum = function (nums, target) {
    const response = []
    // for (let i = 0; i < nums.length; i++) {
    //     for (let y = 0; y < nums.length; y++) {
    //         // console.log("🚀 ~ file: 01.js ~ line 4 ~ twoSum ~ i", nums[i])
    //         // console.log("🚀 ~ file: 01.js ~ line 5 ~ twoSum ~ y", nums[y])
    //         console.log("i = ", nums.indexOf(nums[i]))
    //         console.log("y = ", nums.indexOf(nums[y]))
    //         if (nums.indexOf(nums[i]) !== nums.indexOf(nums[y])) {
    //             if (nums[i] + nums[y] === target) {
    //                 // console.log("yes " + nums[i], nums[y])
    //                 response.push(nums.indexOf(nums[i]))
    //                 response.push(nums.indexOf(nums[y]))
    //             }
    //         }
    //     }
    // }

    nums.map((item1, index1) => {
        nums.map((item2, index2) => {
            if (nums.indexOf(item1) !== nums.indexOf(item2) && new Set(nums).size === nums.length) {
                if (item1 + item2 === target) {
                    response.push(index1)
                    response.push(index2)
                }
            }
            if (new Set(nums).size !== nums.length) {
                if (index1 !== index2 && item1 + item2 === target) {
                    response.push(index1)
                    response.push(index2)
                }
            }
        })
    })

    response.length = 2
    return response
};


console.log("🚀 ~ file: 01.js ~ line 20 ~ twoSum ~ twoSum", twoSum([2, 7, 11, 15], 9))

console.log("🚀 ~ file: 01.js ~ line 20 ~ twoSum ~ twoSum", twoSum([3, 2, 4], 6))

console.log("🚀 ~ file: 01.js ~ line 20 ~ twoSum ~ twoSum", twoSum([3, 3], 6))


// solution

var twoSum = function(nums, target) {
    for(var i=0;i<nums.length;i++){
        for(var j = i+1;j<nums.length;j++){
            if(nums[i]+nums[j] == target){
                return [i,j]
            }
        }
    }
};



