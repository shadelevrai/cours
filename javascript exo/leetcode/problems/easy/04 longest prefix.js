/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function (strs) {

    function allAreEqual(array) {
        const result = array.every(element => {
            if (element === array[0]) {
                return true;
            }
        });

        return result;
    }

    let response = []
    let wordSmaller = 10000
    let stop = false
    strs.map((item) => {
        item.length < wordSmaller && (wordSmaller = item.length)
    })

    let sameLetter = []
    for (let index = 0; index < wordSmaller; index++) {
        if (!stop) {
            strs.map(item => {
                for (let index2 = 0; index2 < 1; index2++) {
                    sameLetter.push(item[index])
                }
            })
            if (allAreEqual(sameLetter)) {
                response.push(sameLetter[0])
                // console.log("🚀 ~ file: 04 longest prefix.js ~ line 59 ~ longestCommonPrefix ~ response", response)
                sameLetter = []
            }
            else {
                sameLetter = []
                stop = true
            }
        }
    }
    return response.join('')
};


console.log(longestCommonPrefix(["flower", "flow", "flight"]))
console.log(longestCommonPrefix(["flower", "flower", "flower", "flower"]))
console.log(longestCommonPrefix(["cir", "car"]))

/*

Trouver le nombre du mot le plus court
faire un for avec index < nombre du mot le plus court
exemple : 
le nombre du mot le plus court est 4
for 4
> for *nombre de mot* pour choper la lettre
*/

