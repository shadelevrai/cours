const express = require('express');
const app = express();
var server = require('http').createServer(app);
var port = process.env.PORT || 8080;

app.get('/api/hello', (req, res) => {
    res.send({ express: 'Hello From Express' });
  });

var server = app.listen(port, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
  });