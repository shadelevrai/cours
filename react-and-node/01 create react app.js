//https://medium.freecodecamp.org/how-to-make-create-react-app-work-with-a-node-backend-api-7c5c48acb1b0

//Se mettre dans le dossier choisi pour le projet et faire 'create-react-app nomDuProjet' en ligne de commande
//Dans le dossier nomDuProjet, créer un dossier 'client'
//Tout mettre dans le dossier 'client'
//toujours à la racinde du projet, créer un fichier package.json
//Dans le fichier package.json, mettre ce code :

{
    "name": "example-create-react-app-express",
    "version": "1.0.0",
    "scripts": {
      "client": "cd client && yarn start",
      "server": "nodemon server.js",
      "dev": "concurrently --kill-others-on-fail \"yarn server\" \"yarn client\""
    },
    "dependencies": {
      "express": "^4.16.2"
    },
    "devDependencies": {
      "concurrently": "^3.5.0"
    }
  }

  //installer nodemon en -g (si ce n'est pas déjà fait)
  //creer un fichier server ('app.js' ou 'server.js')
  //mettre ce code dans le fichier server :

  const express = require('express');

const app = express();
const port = process.env.PORT || 5000;

app.get('/api/hello', (req, res) => {
  res.send({ express: 'Hello From Express' });
});

app.listen(port, () => console.log(`Listening on port ${port}`));


//dans le fichier "package.json" du client, mettre "proxy": "http://localhost:5000/" (le numéro de port doit être le même que celui  du server)

//Ouvrir le fichier /client/src/app.js

//Remplacer le code par ce code :

import React, { Component } from 'react';

import logo from './logo.svg';

import './App.css';

class App extends Component {
  state = {
    response: ''
  };

  componentDidMount() {
    this.callApi()
      .then(res => this.setState({ response: res.express }))
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('/api/hello');
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">{this.state.response}</p>
      </div>
    );
  }
}

export default App;

//Revenir au NomDuProjet et lancer en ligne de commande 'yarn dev'