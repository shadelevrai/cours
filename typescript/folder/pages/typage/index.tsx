import type { NextPage } from "next";

const home02: NextPage = () => {
  //On peut faire simplement comme ça
  const mama: string = "mama";
  const titi: number = 56;

  //Ou créer un type avant de faire la variable
  type Lulu = string;
  const lulu: Lulu = "toto";

  type Popo = string[]
  const popo:Popo = ["lala","lili"]

  type Gigi = object[]
  const gigi:Gigi = [{},{}]

  //On peut aussi faire des objets
  type Tata = object;
  const tata: Tata = {};

  type Rere = {
    name: string;
    age: number;
  };

  const rere: Rere = {
    name: "shade",
    age: 56,
  };

  function tutu(parameter01: string) {
    console.log(parameter01);
  }

  //Avec les Type, on peut aussi faire des itérations
  type Keys = "firstname" | "surname";

  type DudeType = {
    [key in Keys]: string;
  };

  const test: DudeType = {
    firstname: "Pawel",
    surname: "Grzybek",
  };

  //Un type peut avoir plusieurs type
  type MyBool = true | false;

  const myBool: MyBool = true;

  //On peut être plus précis dans le typage
  type WindowStates = "open" | "closed" | "minimized";
  const windowStates:WindowStates = "open"

  function getLength(obj: string | string[]):number {
    return obj.length;
  }

  return <div>{getLength(["lala","lolo"])}</div>;
};

export default home02;
