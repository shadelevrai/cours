import type { NextPage } from "next";
import { useEffect } from "react";

const home01: NextPage = () => {

  //Avant de créer un object, on peut créer une interface
  //ATTENTION : les interfaces ne fonctionnent que pour les objets. Pour autre, voir le "type"
  interface Txt {
    name: string;
    age: number;
  }

  //Et cette interface fera office lors de la création de la variable
  const txt: Txt = {
    name: "Shade",
    age: 36,
  };

  //On peut aussi mettre une interface sur ce qu'une fonction va nous retourner
  function titi(): Txt {
    return { name: "shade", age: 36 };
  }

  return <div> {titi().name} </div>;
};

export default home01;
