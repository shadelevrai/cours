import type { NextPage } from "next";
import { useEffect } from "react";

const home04: NextPage = () => {
  //string
  let employeeName: string = "John Smith";

  //boolean
  let isPresent: boolean = true;

  //Array
  let fruits: string[] = ["Apple", "Orange", "Banana"];
  let fruits2: Array<string> = ["Apple", "Orange", "Banana"];

  //Tuples
  var employee: [number, string] = [1, "Steve"];
  var user: [number, string, boolean, number, string]; // declare tuple variable
  user = [1, "Steve", true, 20, "Admin"]; // initialize tuple variable

  //enum
  enum test {
    lala,
    lolo,
    lili,
  }

  //union
  let code: string | number;
  code = 123; // OK
  code = "ABC"; // OK
  code = false; // Compiler Error

  //any
  let something: any = "Hello World!";
  something = 23;
  something = true;

  //void
  function sayHi(): void {
    console.log("Hi!");
  }
  let speech: void = sayHi();
  console.log(speech); //Output: undefined

  //never
  //Un function qui ne doit jamais retourner quelque chose
  function worker(): never {
    while (true) {}
  }

  return <div></div>;
};

export default home04;
