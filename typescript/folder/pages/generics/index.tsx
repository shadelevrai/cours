import type { NextPage } from "next";

const home03: NextPage = () => {
  type StringArray = Array<string>;
  type NumberArray = Array<number>;
  type ObjectWithNameArray = Array<{ name: string }>;

  const stringArray: StringArray = ["alal", "lala"];
  const objectWithNameArray: ObjectWithNameArray = [{ name: "pascal" }];

  //On peut passer un Type en argument
  function fun<T>(args: T): T {
    return args;
  }
  let result = fun<string>("Hello World");
  let result2 = fun<number>(200);

  //On peut passer plusieurs type en argument
  function fun2<T, U, V>(args1:T, args2: U, args3: V): V {
    return args3;
  }

  let result3 = fun2<string, number, boolean>('hey', 3, false);

  return <div></div>;
};
