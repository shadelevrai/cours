Si on a pas l'onglet animation, le faire apparaitre (window > animation > animation )
De préférence, avoir les onglets Project et Animation séparé

Dans le jeu, selectionnez le personnage qu'on veut animé et dans l'onglet animation, cliquez sur le bouton Create animation.

Si le dossier Animations dans le dossier Asset n'existe pas, le créer.
Dans le dossier Animation, créer une animation (ex : Player_animation_down)

Dans l'onglet project, selectionnez le dosser Sprites > Player et glisser chaque sprite dans l'onglet Animation

Pour faire les autres animations de direction, on clique sur le menu déroulant de l'animation actuelle de l'onglet Animation et on fait Create New clip

Si tout s'est bien passé, dans le dossier Animations, il y a eu 4 nouveaux fichiers .anim

Faire une nouvelle animation qui portera le nom de Player_Mouvement_idle et mettre le sprite du perso sans mouvement

Allez dans l'onglet Animator
Pour que le perso ne bouge pas par défaut, il faut lui assigné le Player_Mouvement_idle en faisant clique droit > set as layer default

Dans l'onglet Animator, sous onglet Parameters, cliquez sur le + et ajouter un float avec comme nom : MoveX. Faire de même avec MoveY

Dans l'onglet Animator, dans le graphique, faire un clique droit sur Player_Mouvement_idle, Make transition et assigné la flèche au mouvement_Up. Faire de même pour les 3 autres directions.

Selectionnez la fleche de transition UP dans le graphique de longlet Animator et dans l'onglet Inspector, sous onglet Conditions, cliquez sur le + et dans MoveY, mettre la valeur GREATER 0.5
Selectionnez la fleche de transition DOWN dans le graphique de longlet Animator et dans l'onglet Inspector, sous onglet Conditions, cliquez sur le + et dans MoveY, mettre la valeur LESS -0.5
Selectionnez la fleche de transition RIGHT dans le graphique de longlet Animator et dans l'onglet Inspector, sous onglet Conditions, cliquez sur le + et dans MoveX, mettre la valeur GREATER 0.5
Selectionnez la fleche de transition LEFT dans le graphique de longlet Animator et dans l'onglet Inspector, sous onglet Conditions, cliquez sur le + et dans MoveX, mettre la valeur LESS -0.5

Ouvrir le script PlayerController

Créez une variable privé dans la classe PlayerController : private Animator anim;

Dans la function void start, mettre : anim = GetComponent<Animator>();
A la fin de la function Update, mettre : 
anim.SetFloat("MoveX", Input.GetAxisRaw("Horizontal"));
anim.SetFloat("MoveY", Input.GetAxisRaw("Vertical"));

Dans l'onglet animator dans le graphique, faire clique droit sur mouvement_Right et Make transition vers le Player_Mouvement_idle. Puis cliquez sur la nouvelle fleche d'animation et dans l'onglet inspector, conditions, choisir MOVEX LESS 0.5
faire clique droit sur mouvement_Up et Make transition vers le Player_Mouvement_idle. Puis cliquez sur la nouvelle fleche d'animation et dans l'onglet inspector, conditions, choisir MOVEY LESS 0.5
faire clique droit sur mouvement_Left et Make transition vers le Player_Mouvement_idle. Puis cliquez sur la nouvelle fleche d'animation et dans l'onglet inspector, conditions, choisir MOVEX Great -0.5
faire clique droit sur mouvement_down et Make transition vers le Player_Mouvement_idle. Puis cliquez sur la nouvelle fleche d'animation et dans l'onglet inspector, conditions, choisir MOVEY Great -0.5

Ensuite, cliquez sur les 8 fleches (de idle à chaque direction) et dans l'onglet inspector, dans l'onglet setting, décochez Has exit time, fixed duration et mettre 0 dans le exit time et transition duration.

Maintenant le Player va dans les directions avec les bonnes animations. Mais quand il s'arrète, il regarde toujours vers le bas.

Dans l'onglet Hierarchy, selectionner l'objet du perso que vous animez.
Dans l'onglet Animation, créez une animation en plus en déroulant le menu déroulant des animations (create new clip)
Nommez cette animation Mouvement_Face_Up
Une fois l'animation crée, dans l'onglet Prject, selectionnez la bonne animation dans le dossier Animation et glissez la dans la timeline de l'onglet animation
Faire la même chose pour les 3 autres directions 

Allez dans l'onglet Animator et supprimez les 8 animations 

Toujours dans l'onglet Animator, dans le sous onglet Parameters, créez deux nouvelle variable Float : LastMoveX et LasyMoveY
Créez aussi une variable Booleen qui se nommera PlayerMoving

Dans le graphique de l'onglet Animator, faites un clique droit sur du vide et choisissait Create state > from new blend tree

En ayant le blendtree fraichement crée, changer son nom en Player Mouvement 

Double cliquer sur le bleed tree. Selectionnez le Blend Tree et dans l'onglet inspector, choisir 2D simple Directional dans le Blend Type et dans parameters, choisir MoveX et MoveY 

Juste en dessous, dans l'onglet Motion, choisir une direction (exemple Mouvement_Face_Up) et mettre la bonne position (X = 0, Y = 1).
Faire aussi pour les 3 autres directions

Revenir dans le graphique general et faire un clique clique droit sur l'anim fraichement crée et choisir set as layer default set 