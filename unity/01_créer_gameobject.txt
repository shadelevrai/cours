GameObjects are the fundamental objects in Unity that represent characters, props and scenery. They do not accomplish much in themselves but they act as containers for ComponentsA functional part of a GameObject. A GameObject can contain any number of components. Unity has many built-in components, and you can create your own by writing scripts that inherit from MonoBehaviour. More info
See in Glossary, which implement the real functionality.

Pour créer un gameObject, il faut cliquer sur le + de la fenetre "hierarchy" qui es à gauche.
Create Empty
Et renommer par rapport à votre besoin (par exemple si c'est l'objet pour le héro, faire PlayerObject)