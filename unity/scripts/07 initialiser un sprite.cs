//Il faut déjà avoir un sprite en prefabs
// Creez un script dans le dossier scripts
// Créez un objet GameObject dans la hierarchy
//Attachez le script au gameObject 
// Tapez la ligne [SerializeField] private Transform pfCharacterBattle; 
// Dans unity, dans l'onglet inspector, dans le script, il y a un nouvel input du nom de pfCharacterBattle 
//Danse cet input, glisser le prefabs dessus (à partir de l'onglet project)

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleHandler : MonoBehaviour
{
    //You use the SerializeField attribute when you need your variable to be private but also want it to show up in the Editor. 
    [SerializeField] private Transform pfCharacterBattle; 
    // Start is called before the first frame update
    private void Start()
    {
        //The function Instantiate() makes a copy of an object in a similar way to the Duplicate command in the editor. If you are cloning a GameObject you can specify its position and rotation (these default to the original GameObject's position and rotation otherwise). If you are cloning a Component the GameObject it is attached to is also cloned, again with an optional position and rotation.
        Instantiate(pfCharacterBattle, new Vector3(-5, 0), Quaternion.identity);
        //Constructs new Quaternion with given x,y,z,w components.
        Instantiate(pfCharacterBattle, new Vector3(+5, 0), Quaternion.identity);
    }


}
