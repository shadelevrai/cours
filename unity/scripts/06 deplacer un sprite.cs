using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class MoveCube : MonoBehaviour
{
    public float speed = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float dir = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * dir * Time.deltaTime * speed);

        float dir2 = Input.GetAxis("Vertical");
        transform.Translate(Vector3.up * dir2 * Time.deltaTime * speed);
    }
}