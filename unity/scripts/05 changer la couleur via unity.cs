using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    //On créer la varibale sr qui sera un SpriteRenderer
    SpriteRenderer sr;

    //On créer une variable qui sera une couleur
    public Color couleur;
    // Start is called before the first frame update
    // Ne pas oubliez d'associer ce script à un objet
    void Start()
    {
        //Dans la variable sr, on met le composant SpriteRenderer
        sr = GetComponent<SpriteRenderer>();
    }

//OnMouseUp est la fonction qui se déclenche au click
    private void OnMouseUp(){
        //Au clic, le sprite prendra la couleur choisie sur unity, dans les variables du script
        sr.color = couleur;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
