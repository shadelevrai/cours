using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    //On créer la varibale sr qui sera un SpriteRenderer
    SpriteRenderer sr;
    // Start is called before the first frame update
    // Ne pas oubliez d'associer ce script à un objet
    void Start()
    {
        //Dans la variable sr, on met le composant SpriteRenderer
        sr = GetComponent<SpriteRenderer>();
    }

//OnMouseUp est la fonction qui se déclenche au click
    private void OnMouseUp(){
        sr.color = Color.red;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
