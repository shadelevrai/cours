**Avant de prendre une nouvelle tache Jira**
* Prendre le sujet sur Jira en s'affectant la tache
* Mettre la tache dans la colonne "dev in progress"

**Commencer une nouvelle tache**
* Créer une nouvelle branch en local qui part de develop
* Faire un rebase sur des branch en cours de dev dont on a besoin

**Avant de commit**
* Faire npm run format/lint/test/build

**Avant de push pour faire son merge request**
* Faire un merge request
* Remplir le changelog
* s'assignez la tache
* Mettre le label "needs GSP review"

**Avant de faire une merge request**
* Fusionner ses commit (squash)

**Après avoir eu les 2 approuve pour le merge request**
* Cliquez sur "merge with pipeline succeds"
* Deployez sur ENV,UAT, DEV02, UATR

**Après que notre branch ait été merge dans develop**
* Mettre le ticket en delivered in UAT