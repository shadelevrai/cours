var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser');
var outils = require('./outils');

//appel du fichier gérant le lien à la BDD
var db = require('./db');

//routes
var site = require('./routes/site');
var admin = require('./routes/admin');

var app = express();

//moteur de template
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//fichiers statiques
app.use(express.static(path.join(__dirname, 'public')));

//gestion des sessions
var sess;
app.use(cookieParser())
app.use(session({
    secret:'123456789SECRET',
    saveUninitialized : false,
    resave: false
}));

//Support des envois
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use('/', site);
app.use('/admin', admin);

app.use(function(req, res, next){
      res.status(404);
    // réponse avec une page html
    if (req.accepts('html')) {
      res.render('404',{ titrePage: 'Erreur', titreH1: 'Des erreurs'});
      return;
    }

    // réponse avec du json
    if (req.accepts('json')) {
      res.send({ error: 'Fichier absent' });
      return;
    }

    // Réponse avec une fichier texte
    res.type('txt').send('Pas de réponse');

});


db.connect('mongodb://localhost:27017/blog', function(err) {
  if (err) {
    console.log('Impossible de se connecter à la base de données.');
    process.exit(1);
  } else {
    var server = app.listen(8080, function () {
    var addresseHote = server.address().address;
    var portEcoute = server.address().port;

    console.log(' L\'application est disponible à l\'adresse http://%s:%s', addresseHote, portEcoute);
  });
  }
});