var boite = {
  mois : ['janvier','février','mars','avril','mai','juin','juillet','aout','septembre','octobre','novembre','decembre'],
  jours : ['dimanche','lundi','mardi','mercredi','jeudi','vendredi','samedi'],
  ajoutZero : function(nombre){
      if(nombre < 10){
        nombre = '0' + nombre;
      }
      return nombre;
    }
};

exports.dateComplete = function(date) {
  var conversion = new Date(date);
  return (boite.jours[conversion.getDay()] + ' ' + conversion.getDate() + ' ' + boite.mois[conversion.getMonth()] + ' ' + conversion.getFullYear());
};

exports.heureComplete = function(date) {
  var conversion = new Date(date);
  return (boite.ajoutZero(conversion.getHours())+ 'h' + boite.ajoutZero(conversion.getMinutes()));
};

exports.dateHeure = function(date) {
  return this.dateComplete(date) + ' - ' + this.heureComplete(date);
}