var express = require('express');
var router = express.Router();
var db = require('../db');
var outils = require('../outils');
var donnees = {};

/* Page d'accueil du site */
router.get('/', function(req, res, next) {
  var collection = db.get().collection('articles');
  collection.find().sort({date:-1}).limit(5).toArray(function(err,data) {
    for(var i = 0; data[i]; i++){
      data[i].date = outils.dateHeure(data[i].date);
    }
    donnees.data = data;
    res.render('index',{titrePage: 'Accueil du site', donnees : donnees});
  });
});

/* Article suivant */
router.get('/articleNg/:suivant', function(req,res, next){
  var suivant = req.params.suivant;
  suivant = parseFloat(suivant.substring(1,suivant.length));
  var collection = db.get().collection('articles');
  collection.find().sort({date:-1}).limit(1).skip(suivant).toArray(function(err,data) {
    if (data.length) {
      data[0].date = outils.dateHeure(data[0].date);
      if (req.headers.accept == 'application/json, text/plain, */*') {
        var reponse = JSON.stringify(data);
        res.send(reponse);
      } else {
        res.redirect('/');
      } 
    } else {
      res.status(204).send('');
    }
    
  });
});

/* Affichage d'un article */
router.get('/article/:id', function(req,res, next){
  var id = req.params.id;
  var collection = db.get().collection('articles');
  var mongo = require('mongodb');
  var mongoID = new mongo.ObjectID(id.substring(1,id.length));
  collection.find({_id:mongoID}).toArray(function(err,data) {
    for(var i = 0; data[i]; i++){
      data[i].date = outils.dateHeure(data[i].date);
    }
    donnees.data = data;
    if (req.headers.accept == 'application/json, text/plain, */*') {
      var reponse = JSON.stringify(data);
      res.send(reponse);
    } else {
      res.render('article',{titrePage: 'Un article', donnees : donnees});
    }
  });
});

module.exports = router;
