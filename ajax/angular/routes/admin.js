var express = require('express');
var router = express.Router();
var db = require('../db');
var outils = require('../outils');
var donnees = {message: ''};

/* Page d'accueil de l'administration*/
router.get('/', function(req, res, next) {
  res.render('adminIndex',{ H1: 'Administration', titrePage: 'Administration', donnees:donnees});
  donnees.message = '';
});

/* Liste des articles d'accueil du site */
router.get('/articles', function(req, res, next) {
  var collection = db.get().collection('articles');
  collection.find().toArray(function(err,data) {
    for(var i = 0; data[i]; i++){
      data[i].date = outils.dateHeure(data[i].date);
    }
    donnees.data = data;
    res.render('adminArticles',{titrePage: 'Articles du site', donnees : donnees});
  });
});


/* Formulaire d'ajout d'articles et traitement de l'ajout*/
router.get('/ajout', function(req, res, next) {
  res.render('ajout',{titrePage: 'Ajout d\'un article'});
});

router.post('/traitement',function(req,res,next){
  var collection = db.get().collection('articles');
  collection.insert({titre:req.body.titre, corps:req.body.corps, auteur: req.body.auteur, date: Date.now()},function(err,data){
    if (err) {
      
    } else {
      donnees.message = 'Article ajouté';
      res.redirect('/admin');
    }
  });
});

module.exports = router;
