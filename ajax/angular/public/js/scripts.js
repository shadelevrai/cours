angular.module('moduleBlog',[]);

angular.module('moduleBlog').controller('articlesSuivants',['$http' ,function($http){
  var suivant = this;
  this.showButton = true;
  this.increment = 5;
  this.affiche = function(){
    $http.get('/articleNg/:'+this.increment).then(function(reponse){
      suivant.increment++;
      if (reponse.status == 204) {
        suivant.showButton = false;
      } else {
      suivant.articles.push(reponse.data[0]);
      }
    },function(reponse){
      
    });
  };
  this.articles = [];
}]);