/*********************************
***********Présentation***********
*********************************/
/*
La méthode load() de jQuery permet de récupérer des données depuis le serveur et de les placer dans l'élément du DOM ciblé (https://api.jquery.com/load/).
*/


/*********************************
*************Exercice*************
*********************************/
/*

------ 1 ------
Reprenez le site de l'exercice précédent. Prévoyez au moins trois emplacements dans la seconde page.

------ 2 ------
À chaque click sur un élément de la page un, un élément de la page deux s'affiche dans cette première page à l'aide de la méthode load().
*/

/*

fichier js

$(function () {
    $('body').prepend('<div class="azerty2"></div>');
    $('.azerty2').css({
        height: '50px',
        width: '100px',
        backgroundColor: 'grey',
    }).text('Cliquez ici');

    var incrementation = 1;

    $('.azerty2').click(function () {
        $('.azerty3').remove();
        $('body').prepend('<div class="azerty3"></div>');
        $('.azerty3').css({
            position: 'absolute',
            left: '150px',
            height: '100px',
            width: '300px',
            backgroundColor: 'red',
        });
        if (incrementation < 4)
            $('.azerty3').load('page01 h' + incrementation);
        incrementation++;
        console.log(incrementation);
    });
});