var express = require('express');
var app = express();

app.use(express.static(__dirname + '/fichiers')); //Dire qu'on va utiliser ce que contient le dossier html

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './fichiers'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/',function(req,res){
  res.render('accueil'); // on envoit le fichier index.jade du dossier html3
});

app.get('/page01',function(req,res){
  res.render('page01'); // on envoit le fichier index.jade du dossier html3
});

app.get('/page02',function(req,res){
  res.render('page02'); // on envoit le fichier index.jade du dossier html3
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});