$(function () {
    $('body').prepend('<div class="azerty2"></div>');
    $('.azerty2').css({
        height: '50px',
        width: '100px',
        backgroundColor: 'grey',
    }).text('Cliquez ici');

    var incrementation = 1;

    $('.azerty2').click(function () {
        $('.azerty3').remove();
        $('body').prepend('<div class="azerty3"></div>');
        $('.azerty3').css({
            position: 'absolute',
            left: '50px',
            height: '50px',
            width: '100px',
            backgroundColor: 'red',
        });
        if (incrementation < 4) 
            $('.azerty3').load('page01 h' + incrementation);
        incrementation++;
        console.log(incrementation);
    });
    exports.incrementation = incrementation;
});