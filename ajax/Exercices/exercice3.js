/*********************************
 ***********Présentation***********
 *********************************/
/*
La méthode get() de jQuery permet de gèrer l'envoie d'une requête en get et  de récupérer les données retournées par le serveur (https://api.jquery.com/get/).
*/


/*********************************
 *************Exercice*************
 *********************************/
/*

------ 1 ------
Réalisez un site avec une page d'accueil.

------ 2 ------
Prévoyez au click sur un élément une requête grâce à la méthode get(). Le serveur traitera cette requête en retournant un json (http://expressjs.com/en/api.html#res.json).

------ 3 ------
Utilisez le json récupéré en plaçant les informations dans la page.
*/
var liste = [{
    name: 'Djoher',
    age: '30 ans'
},{
    name: 'Pika',
    age: '15 ans'
}];

var express = require('express');
var app = express();
var bodyParser = require("body-parser");

app.use(express.static(__dirname + '/fichiers2')); //Dire qu'on va utiliser ce que contient le dossier html

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './fichiers2'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/', function (req, res) {
    res.render('accueil'); // on envoit le fichier index.jade du dossier html3
});

app.get('/api/affiche', function (req,res){
    res.json(liste);
})
//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
});