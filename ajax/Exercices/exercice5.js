/*********************************
 *************Exercice*************
 *********************************/
/*

------ 1 ------
À l'aide d'un un serveur fonctionnant avec Express, réalisez une page contenant un simple formulaire(texte et submit) pour des commentaires. Gérez l'inscription en base du formulaire (avec la date).

------ 2 ------
L'ensemble des données saisies avec le formualire s'affichent au-dessus du formulaire en ordre chronologique.

------ 3 ------
Mettez en place une gestion asynchrone du formulaire.

------ 4 ------
Mettez en place côté client un script qui vérifie la présence de nouvelles entrées dans les commentaires.

------ 5 ------
Gérez les sessions sur votre serveur avec une connexion pour chaque utilisateur.
Seuls les utilisateurs connectés peuvent publier un commentaire et chaque commentaire comprend le nom de l'auteur.
*/

var express = require('express');
var bodyParser = require("body-parser");
var app = express();
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/tutoriel';

var myDate = new Date;

app.use(express.static(__dirname + '/fichiers4'));
app.use(bodyParser.urlencoded({
    extended: true
}));

app.set('view engine', 'jade');
app.set('views', './fichiers4');



app.get('/', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', url);
            collection = db.collection('users');
            azerty = db.collection('users').find().toArray();
            db.collection('users').find().toArray(function (err, result) {
                if (err) {
                    throw err;
                }
                for (var i = 0; i < result.length; i++) {
                    console.log(result[i].commentaire + ' ' + result[i].date);
                }
            });
        }
    
    });
    res.render('home');
    
})

app.get('/iki', function (req, res) {
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        } else {
            console.log('Connection established to', url);
            collection = db.collection('users');
            azerty = db.collection('users').find().toArray();
            db.collection('users').find().toArray(function (err, result) {
                if (err) {
                    throw err;
                }
               // for (var i = 0; i < result.length; i++) {
                   // console.log(result[i].commentaire + ' ' + result[i].date);
               
                //}
                 res.json(result);
            });
        }
    });
    
})

app.post('/azerty', function (req, res) {
    var p1 = req.body.p1;
    console.log("p1=" + p1);
    collection.insert({
        commentaire: p1,
        date: myDate
    });
    res.send('Commentaire mis dans le serveur')
})

app.listen(8888);
console.log('Le serveur 8888 est lancé');