$(document).ready(function(){
  $('#gareArrivee, #horaires').hide();
  $('#gareDepart select').change(function(){
    var gare = $(this).val();
    if (gare != '--Votre choix--') {
      $.get('/actionAjax/gareArrivee/' + gare,function(data){
         var select = '<select name="gareArrivee"><option value="--Votre choix--">--Votre choix--</option>';
        for(var i=0;data.elements[i];i++){
          select += '<option value="'+ data.elements[i] + '">' + data.elements[i] +'</option>';
        }
        select += '</select>';
        $('#gareArrivee').show();
        $('#gareArrivee select').html(select);
      });
    }
  });
  $('#gareArrivee select').change(function(){
    var gareDepart = $('#gareDepart select').val();
    var gareArrivee = $(this).val();
    if (gareDepart != '--Votre choix--' && gareArrivee != '--Votre choix--' ) {
      $.get('/actionAjax/horaires/' + gareDepart + '/' + gareArrivee,function(data){
         var select = '<select name="hoaraires"><option value="--Votre choix--">--Votre choix--</option>';
        for(var i=0;data.elements[i];i++){
          select += '<option value="'+ data.elements[i] + '">' + data.elements[i] +'</option>';
        }
        select += '</select>';
        $('#horaires').show();
        $('#horaires select').html(select);
      });
    }
  });
  $('#horaires select').change(function(){
    var gareDepart = $('#gareDepart select').val();
    var gareArrivee = $('#gareArrivee select').val();
    var horaire = $(this).val();
    if (gareDepart != '--Votre choix--' && gareArrivee != '--Votre choix--' && horaire != '--Votre choix--') {
      $.get('/actionAjax/resultat/' + gareDepart + '/' + gareArrivee + '/' + horaire,function(data){
        console.log(data);
        var datas = data.elements[0].fields;
        if (!datas.commentaires) datas.commentaires = 'r.a.s.';
        var texte = '<table><thead><tr><th>Nombre de trains annulés</th><th>Nombre de trains ayant circulés</th><th>Nombre de trains en retard à l\'arrivée</th><th>Nombre de trains programmés</th><th>Taux de régularité</th><th>Commentaires</th</tr></thead><tbody><tr><td>' + datas.nombre_de_trains_annules + '</td><td>' + datas.nombre_de_trains_ayant_circule + '</td><td>' + datas.nombre_de_trains_en_retard_a_l_arrivee + '</td><td>' + datas.nombre_de_trains_programmes + '</td><td>' + datas.taux_regularite + '%</td><td>' + datas.commentaires +'</td></tr></tbody></table>';
        $('#resultat').html(texte);
      });
    }
  });
});
