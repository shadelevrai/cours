//Démarrage
  //express
  var express = require('express');
  var app = express();
  //mongoDB
  var db = require('./db');
  var URL = 'mongodb://localhost:27017/sncf';
  //body-parser
  var bodyParser = require('body-parser');
  app.use(bodyParser.urlencoded({ extended: false }));
  //session
  var session = require('express-session');
  app.use(session({
      secret:'123456789SECRET',
      saveUninitialized : false,
      resave: false
  }));
  //autre
  var tools = require('./tools');
//Réglages
  app.use("/staticFiles", express.static(__dirname + '/staticFiles'));
  app.set('view engine', 'pug');
  app.set('views','pugfiles');

  //Accueil
  app.get('/', function(req,res,next) {
    var collection = db.get().collection('tgv');
    collection.distinct('fields.depart',function(err,data) {
      data.sort();
      console.log(data);
      res.render('index',{ title:'Accueil', gares: data, session:req.session});
    });
  });

  //Ajax - gare d'arrivé
  app.get('/actionAjax/gareArrivee/:gare', function(req,res,next) {
    var collection = db.get().collection('tgv');
    collection.distinct('fields.arrivee',{"fields.depart" : req.params.gare}, function(err,data) {
      data.sort();
      res.json({ elements: data});
    });
  });

  //Ajax - horaires
  app.get('/actionAjax/horaires/:gareDepart/:gareArrivee', function(req,res,next) {
    var collection = db.get().collection('tgv');
    collection.distinct('fields.date',{'fields.depart' : req.params.gareDepart, 'fields.arrivee' : req.params.gareArrivee}, function(err,data) {
      data.sort();
      res.json({ elements: data});
    });
  });

  //Ajax - réponse finale
  app.get('/actionAjax/resultat/:gareDepart/:gareArrivee/:date', function(req,res,next) {
    console.log(req.params);
    var collection = db.get().collection('tgv');
    collection.find({'fields.depart' : req.params.gareDepart, 'fields.arrivee' : req.params.gareArrivee, "fields.date" : req.params.date}).toArray(function(err,data) {
      console.log(data);
      res.json({ elements: data});
    });
  });


	//gestion 403 et 404
  app.use(function(req, res, next) {
    switch(res.statusCode){
      case 403:
        res.render('403',{ title:'Accès interdit !', session: req.session});
      break;
      default:
        res.status(404).render('404',{ title:'Page inconnue', session: req.session});
    }
  });

  //Démarrage du serveur

db.connect(URL, function(err, db) {
  if (err) {
    return;
  }
  app.listen(8080, function() {
    var date = new Date();
    console.log(tools.dateHeureLongue(date) + ' - Le serveur est disponible sur le port 8080');
  });
});
