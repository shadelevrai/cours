const TAXE = {
    CA:1.20
}
function calc(prix,qty,taxe) {
    return prix * qty * (TAXE[taxe] || 1)
}
const argv = process.argv.slice(2);
console.log(calc(...argv));