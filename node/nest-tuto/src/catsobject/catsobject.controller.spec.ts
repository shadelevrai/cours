import { Test, TestingModule } from '@nestjs/testing';
import { CatsobjectController } from './catsobject.controller';

describe('CatsobjectController', () => {
  let controller: CatsobjectController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CatsobjectController],
    }).compile();

    controller = module.get<CatsobjectController>(CatsobjectController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
