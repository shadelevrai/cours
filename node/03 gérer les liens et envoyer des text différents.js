var http = require('http');
var url = require('url'); // Pour récupérer la page demandée par le visiteur, on va faire appel à un nouveau module de Node appelé "url". On demande son inclusion

var server = http.createServer(function(req, res) {
    var page = url.parse(req.url).pathname; // Ensuite, il nous suffit de "parser" la requête du visiteur comme ceci pour obtenir le nom de la page demandée :
    
    console.log(page); // On affiche le lien demandé dans l'invit de commande.
    
    res.writeHead(200, {"Content-Type": "text/plain"});
    if (page == '/') {
        res.write('Vous êtes à l\'accueil, que puis-je pour vous ?');
    }
    else if (page == '/sous-sol') {
        res.write('Vous êtes dans la cave à vins, ces bouteilles sont à moi !');
    }
    else if (page == '/etage/1/chambre') {
        res.write('Hé ho, c\'est privé ici !');
    } else if (page == '/lol'){
        require('./exo2');
        res.write('Cest bon');
    }
    res.end();
});
server.listen(8080);