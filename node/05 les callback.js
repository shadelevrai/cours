/* lss callback sont un concept de 'réponses'. 
$("canvas").on("mouseleave", function() { ... }); est l'équivalent des callback mais en jquery.  
*/


//Je vous propose un exemple concret et complet. On va lancer un serveur et l'arrêter juste après. On écoute l'évènement close qui survient lorsque le serveur est arrêté. On affiche un message dans la console quand le serveur s'apprête à s'arrêter.

server.on('close', function() {
    // Faire quelque chose quand le serveur est arrêté
})

//***************************************************************//

var http = require('http');

var server = http.createServer(function(req, res) {
  res.writeHead(200);
  res.end('Salut tout le monde !');
});

server.on('close', function() { // On écoute l'évènement close
    console.log('Bye bye !');
})

server.listen(8080); // Démarre le serveur

server.close(); // Arrête le serveur. Déclenche l'évènement close