var http = require('http');


var server = http.createServer(function (req, res) {
    res.writeHead(200, {
        "Content-Type": "text/html"
    }); // On ecrit le type de donnée que va envoyer le serveur. On peut envoyer plusieurs valeurs sous forme de tableau.
    res.write('<!DOCTYPE html>' +
        '<html>' +
        '    <head>' +
        '        <meta charset="utf-8" />' +
        '        <title>Ma page Node.js !</title>' +
        '    </head>' +
        '    <body>' +
        '     	<p>Voici un paragraphe <strong>HTML</strong> !</p>' +
        '    </body>' +
        '</html>'); // On ecrit le code HTML s'ecrira ici, dans 'res.write'. C'est mieux que de le mettre dans res.end(). 
    res.end();
});
server.listen(8080);