var http = require('http');
var url = require('url');
var querystring = require('querystring'); // module qui permet de récupérer les paramètres entrés et de les parse (découper).

var server = http.createServer(function(req, res) {
    var params = querystring.parse(url.parse(req.url).query); // Vous disposerez alors d'un tableau de paramètres "params". Pour récupérer le paramètre "prenom" par exemple, il suffira d'écrire : params['prenom'].
    
    res.writeHead(200, {"Content-Type": "text/plain"});
    if ('prenom' in params && 'nom' in params) {
        res.write('Vous vous appelez ' + params['prenom'] + ' ' + params['nom']);
    } // Une précision par rapport à ce code : 'prenom' in params me permet en JavaScript de tester si le tableau contient bien une entrée 'prenom'. S'il manque un paramètre, je peux alors afficher un message d'erreur (sinon mon script aurait affiché undefined à la place).
    
    else {
        res.write('Vous devez bien avoir un prénom et un nom, non ?');
    }
    res.end();
});
server.listen(8888);