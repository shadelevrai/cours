//Si vous voulez émettre des évènements vous aussi, c'est très simple : incluez le module EventEmitter et créez un objet basé sur EventEmitter.
var EventEmitter = require('events').EventEmitter;

var jeu = new EventEmitter();


//Celui qui veut écouter l'évènement :
jeu.on('gameover', function(message){
    console.log(message);
});

/*Ensuite, pour émettre un évènement dans votre code, il suffit de faire appel à emit() depuis votre objet basé sur EventEmitter. Indiquez :

Le nom de l'évènement que vous voulez générer (ex : "gameover"). A vous de le choisir.
Un ou plusieurs éventuels paramètres à passer (facultatif)
Ici, je génère un évènement "gameover" et j'envoie un message à celui qui réceptionnera l'évènement via un paramètre :*/

jeu.emit('gameover', 'Vous avez perdu !');