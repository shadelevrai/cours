var http = require("http"); // la variable 'http' sera un module 'http' qui éxiste déjà dans node.

http.createServer(function (request, response) { // on crée un serveur et on donne des arguments à la fonction.
    console.log("Requête reçue."); // affiche dans l'invit de commande 'requete reçue'.
    response.writeHead(200, {
        "Content-Type": "text/plain"
    });
    response.write("Hello World!!!"); // affiche 'hello world' sur le navigateur
    response.end(); // termine la fonction, et donc ce que le serveur doit renvoyer au navigateur
}).listen(8888); //Le serveur sera dans le port 8888