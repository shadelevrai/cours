var http = require('http'); 

var server = http.createServer(function (req, res) { 
    res.writeHead(200);
    res.end('Salut tout le monde !'); 
});

var monmodule = require('../monModule');

monmodule.direBonjour();
monmodule.direByeBye();

server.listen(8080);