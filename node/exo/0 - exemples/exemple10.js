﻿const fs = require('fs');
const url = require('url');
const http = require('http');
const path = require('path');  
const jade = require('jade');  

var monServeur = http.createServer();

monServeur.on('request', function(requeteHTTP, reponseHTTP){

  var parsedUrl = url.parse(requeteHTTP.url, true);
  
  var queryObject = parsedUrl.query;
  
  var chemin = path.normalize(__dirname + parsedUrl.pathname);
  
  fs.readFile(chemin, function(error, data){
    
    if(error){
      var corps = '<!doctype html><html lang="fr"><head><meta charset="utf-8"><title>Erreur 404 nom de dieu !</title></head><body><h1>Erreur 404 nom de dieu !</h1><p>Le fichier est introuvable par toutatis</p></body></html>';
      var bfCorps = new Buffer(corps);
      
      reponseHTTP.writeHead(404, {
        'Content-type':'text/html;charset=utf8',
        'Content-length': bfCorps.length
      } ); //écrire les en-têtes de réponse
      reponseHTTP.write(bfCorps, 
      function(){
        reponseHTTP.end(); //indique la fin de réponse
      }); //écrire le corps de la réponse
    }else{
      var ext = path.extname(chemin);
      var mimeType = 'text/plain;charset=utf8';
      console.log(ext);
      switch(ext){
        case '.html':
          mimeType = 'text/html;charset=utf8';
        break;
        case '.superSam':
          mimeType = 'text/html;charset=utf8';
          //conversion buffer en string
          var texte = data.toString('utf8');
          //a remplacer
         var date = new Date();
          //a rechercher et remplacer par
          texte = texte.replace('#!#@date', date.toString());
          texte = texte.replace('#!#@timestamp', date.getTime());
          //conversion string en buffer
          data = new Buffer(texte);
        break;
        case '.jade':
          mimeType = 'text/html;charset=utf8';
          //conversion buffer en string
          var texte = data.toString('utf8');
          //a remplacer
          var date = new Date();
          //a rechercher et remplacer par
          texte = jade.render(texte, {
            pageTitle: 'Nouvelle page réalisée avec Jade !',
            zeDate: date.toString(),
            prenom: queryObject.prenom || 'Inconnu'
          });
          //conversion string en buffer
          data = new Buffer(texte);
        break;
        case '.css':
          mimeType = 'text/css';
        break;
        case '.js':
          mimeType = 'application/javascript';
        break;
        case '.png':
          mimeType = 'image/png';
        break;
        case '.jpeg':
          mimeType = 'image/jpeg';
        break;
      }
      
      reponseHTTP.writeHead(200, {
        'Content-type': mimeType,
        'Content-length': data.length
      } ); //écrire les en-têtes de réponse
      
      
      reponseHTTP.write(data, 
      function(){
        reponseHTTP.end(); //indique la fin de réponse
      }); //écrire le corps de la réponse
    }
    
  });
  

});

monServeur.listen(9999);

