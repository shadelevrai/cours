var moduleFileSystem = require('fs');

/*moduleFileSystem.write('fichier.txt', 'Hello World !', function(err){
  if(err){
    console.log('Impossible de créer le fichier');
  }else{
    console.log('Fichier créé');
    moduleFileSystem.read('fichier.txt', 'utf8', function(err, data){
      if(err){
        console.log('Impossible de lire le fichier');
      }else{
        //affiche le contenu du fichier
        console.log(data);
      };
    });
  };
});*/


var io = {
  ecrire: function(nom, contenu, suivante){
    moduleFileSystem.write(nom, contenu, function(err){
      if(err){
        console.log('Impossible de créer le fichier ' + nom);
      }else{
        console.log('Fichier créé');
        if(suivante){
          suivante();
        };
      };
    });
  },
  lire: function(nom, suivante){
    moduleFileSystem.read(nom, 'utf8', function(err, data){
      if(err){
        console.log('Impossible de lire le fichier');
      }else{
        //affiche le contenu du fichier
        console.log(data);
        if(suivante){
          suivante();
        };
      };
    });
  }
};

io.ecrire('fichier.txt', 'Hello World !', function(){
  io.lire(nom, function(){
    data;
  });
});

