﻿const fs = require('fs');
const url = require('url');
const http = require('http');
const path = require('path');  
const jade = require('jade');  

var monServeur = http.createServer();

monServeur.on('request', function(requeteHTTP, reponseHTTP){

  var userAgent = requeteHTTP.headers['user-agent'];

  var isChrome = false;
  if(userAgent.indexOf('Chrome')!= -1){
    isChrome = true;
  }
  var isFirefox = false;
  if(userAgent.indexOf('Firefox')!= -1){
    isFirefox = true;
  }

  if(isChrome){
    reponseHTTP.write('Salut Chrome', function(){
      reponseHTTP.end();
    });
  }
  if(isFirefox){
    reponseHTTP.write('Salut Firefox', function(){
      reponseHTTP.end();
    });
  }
});

monServeur.listen(9999);

