﻿const fs = require('fs');
const url = require('url');
const http = require('http');
const path = require('path');  
const jade = require('jade');  

var monServeur = http.createServer();

monServeur.on('request', function(requeteHTTP, reponseHTTP){

  var getMimeType = function(ext){
      var mimeType = 'text/plain;charset=utf8';
      switch(ext){
        case '.html':
          mimeType = 'text/html;charset=utf8';
        break;
        case '.superSam':
          mimeType = 'text/html;charset=utf8';
          //conversion buffer en string
          var texte = data.toString('utf8');
          //a remplacer
          var date = new Date();
          //a rechercher et remplacer par
          texte = texte.replace('#!#@date', date.toString());
          texte = texte.replace('#!#@timestamp', date.getTime());
          //conversion string en buffer
          data = new Buffer(texte);
        break;
        case '.css':
          mimeType = 'text/css';
        break;
        case '.js':
          mimeType = 'application/javascript';
        break;
        case '.png':
          mimeType = 'image/png';
        break;
        case '.jpeg':
          mimeType = 'image/jpeg';
        break;
      }
      return mimeType;
  };

  var send404 = function(){
    var corps = '<!doctype html><html lang="fr"><head><meta charset="utf-8"><title>Erreur 404 nom de dieu !</title></head><body><h1>Erreur 404 nom de dieu !</h1><p>Le fichier est introuvable par toutatis</p></body></html>';
    var bfCorps = new Buffer(corps);
    send(bfCorps, 404, '.html');
  };

  var send403 = function(){
    var corps = '<!doctype html><html lang="fr"><head><meta charset="utf-8"><title>Erreur 403 : Non autorisé</title></head><body><h1>Erreur 403</h1><p>L\'accès n\'est pas autorisé. </p></body></html>';
    var bfCorps = new Buffer(corps);
    send(bfCorps, 403, '.html');
  };

  var send = function(buffer, code, ext) {
    reponseHTTP.writeHead(code, {
      'Content-type': getMimeType(ext),
      'Content-length': buffer.length
    } ); //écrire les en-têtes de réponse
    reponseHTTP.write(buffer, 
    function(){
      reponseHTTP.end(); //indique la fin de réponse
    }); //écrire le corps de la réponse
  };

  var sendFile = function(filename){
    var chemin = path.normalize(__dirname + filename);
    fs.readFile(chemin, function(error, data){
      if(error){
        send404();
      }else{
        send(data, 200, path.extname(filename));
      };
    });
  };

  sendFile(requeteHTTP.url);

});

monServeur.listen(9999);

