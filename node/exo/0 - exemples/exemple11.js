﻿const fs = require('fs');
const url = require('url');
const http = require('http');
const path = require('path');  
const jade = require('jade');  

var monServeur = http.createServer();

monServeur.on('request', function(requeteHTTP, reponseHTTP){

  switch(requeteHTTP.url){
    case '/public':
      if(requeteHTTP.headers.cookie){
          var corps = 'Vous êtes déjà authentifié !!! Allez dans la partie privée';
          corps = new Buffer(corps);

          reponseHTTP.writeHead(200, {
            'Content-type':'text/html;charset=utf8',
            'Content-length': corps.length
          });

          reponseHTTP.write(corps, function(){
            reponseHTTP.end();
          });
      }else {
        var fichier = 'session_' + Math.ceil(Math.pow(Math.random() * 10, 10));

        var chemin = path.normalize(__dirname + path.sep + 'sessions' + path.sep + fichier);

        fs.writeFile(chemin, 'test', function(error){

          var corps = 'Vous êtes authentifié (votre fichier de session vient d\'être créé)';
          corps = new Buffer(corps);

          reponseHTTP.writeHead(200, {
            'Content-type':'text/html;charset=utf8',
            'Content-length': corps.length,
            'Set-Cookie': fichier
          });

          reponseHTTP.write(corps, function(){
            reponseHTTP.end();
          });

        });
      }
    break;
    case '/private':
      if(requeteHTTP.headers.cookie){
        var fichier = requeteHTTP.headers.cookie;
        var chemin = path.normalize(__dirname + path.sep + 'sessions' + path.sep + fichier);

        fs.readFile(chemin, function(error, data){
          if(error){
            var corps = 'Vous ne vous êtes jamais authentifié.';
            corps = new Buffer(corps);

            reponseHTTP.writeHead(403, {
              'Content-type':'text/html;charset=utf8',
              'Content-length': corps.length
            });

            reponseHTTP.write(corps, function(){
              reponseHTTP.end();
            });

          } else {
            var corps = 'Bienvenue a vous !!! Vous êtes authentifié.';
            corps = new Buffer(corps);

            reponseHTTP.writeHead(200, {
              'Content-type':'text/html;charset=utf8',
              'Content-length': corps.length
            });

            reponseHTTP.write(corps, function(){
              reponseHTTP.end();
            });
          }
        });

      } else {
        var corps = 'Vous ne vous êtes jamais authentifié.';
        corps = new Buffer(corps);

        reponseHTTP.writeHead(403, {
          'Content-type':'text/html;charset=utf8',
          'Content-length': corps.length
        });

        reponseHTTP.write(corps, function(){
          reponseHTTP.end();
        });
      }
    break;
  }

});

monServeur.listen(9999);

