/******* Variables ES6 ******/
//constantes en ES6 :
const fige = 'Hello';
/****************************/

/*** Fonctions ES5 && ES6 ***/

//fonction en ES5 :
var coucou = function {
  console.log('Yo !');
};
//fonction en ES6 :
var coucou = () => {
  console.log('Yo !');
};

coucou(); // affiche "Yo !"
/****************************/

/* ConcatÚnation ES5 && ES6 */
var nombre = 9999;

//ConcatÚnation en ES5 :
var chaine = 'le nombre est : ' + nombre + '.';
//ConcatÚnation en ES6 :
var chaine = `le nombre est : ${nombre}.`;

console.log(chaine); //affiche "le nombre est : 9999."
/****************************/

/* Construire des chemins vers des fichiers */

'/'; //linux
'\\'; //windows
const path = require('path');

var cheminBien = path.normalize(__dirname + path.sep + 'fichier.txt');




