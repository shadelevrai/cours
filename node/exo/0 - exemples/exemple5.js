//la propriété .length donne le nombre de caractères.
console.log('été'.length); //3
console.log('ete'.length); //3
//la méthode .byteLength() de Buffer donne le nombre d'octets
console.log(Buffer.byteLength('été')); //5
console.log(Buffer.byteLength('ete')); //3

/*
((\{\{){1})((\s)*)(mot)((\s)*)((\}\}){1})
*/