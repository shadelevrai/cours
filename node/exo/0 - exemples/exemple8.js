const http = require('http');
const serveStatic = require('serve-static');

var httpServer = http.createServer();

var staticServer = serveStatic(__dirname,{});

httpServer.on('request', function(requeteHTTP, reponseHTTP){
  
  var suivante = function(){
    var corps = '<!doctype html><html lang="fr"><head><meta charset="utf-8"><title>Erreur 404 nom de dieu !</title></head><body><h1>Erreur 404 nom de dieu !</h1><p>Le fichier est introuvable par toutatis</p></body></html>';
      var bfCorps = new Buffer(corps);
      
      reponseHTTP.writeHead(404, {
        'Content-type':'text/html;charset=utf8',
        'Content-length': bfCorps.length
      } ); //écrire les en-têtes de réponse
      reponseHTTP.write(bfCorps, 
      function(){
        reponseHTTP.end(); //indique la fin de réponse
      }); //écrire le corps de la réponse
  }
  
  staticServer(requeteHTTP, reponseHTTP, suivante)
  
});


httpServer.listen(8888);