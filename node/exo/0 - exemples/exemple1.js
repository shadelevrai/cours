var Sami = {
  prenom: 'Sami',
  quiSuisJe: function(callbackIsBackInBlack){
    this.callbackIsBackInBlack = callbackIsBackInBlack;
    this.callbackIsBackInBlack();
  }
};

var Mohamed = {
  prenom: 'Mohamed',
  quiSuisJe: function(callbackIsBack){
    this.callbackIsBack = callbackIsBack;
    this.callbackIsBack();
  }
};

Mohamed.quiSuisJe(function(){
  var that = this;
  console.log(that.prenom);
});

Sami.quiSuisJe(function(){
  var that = this;
  console.log(that.prenom);
});