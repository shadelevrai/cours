﻿const fs = require('fs');
const http = require('http');
const path = require('path');  

var monServeur = http.createServer();

monServeur.on('request', function(requeteHTTP, reponseHTTP){
  
  var url = requeteHTTP.url;
  
  var chemin = path.normalize(__dirname + url);
  
  fs.readFile(chemin, function(error, data){
    
    if(error){
      var corps = '<!doctype html><html lang="fr"><head><meta charset="utf-8"><title>Erreur 404 nom de dieu !</title></head><body><h1>Erreur 404 nom de dieu !</h1><p>Le fichier est introuvable par toutatis</p></body></html>';
      var bfCorps = new Buffer(corps);
      
      reponseHTTP.writeHead(404, {
        'Content-type':'text/html;charset=utf8',
        'Content-length': bfCorps.length
      } ); //écrire les en-têtes de réponse
      reponseHTTP.write(bfCorps, 
      function(){
        reponseHTTP.end(); //indique la fin de réponse
      }); //écrire le corps de la réponse
    }else{
      var ext = path.extname(chemin);
      var mimeType = 'text/plain;charset=utf8';
      console.log(ext);
      switch(ext){
        case '.html':
          mimeType = 'text/html;charset=utf8';
        break;
        case '.css':
          mimeType = 'text/css';
        break;
        case '.js':
          mimeType = 'application/javascript';
        break;
        case '.png':
          mimeType = 'image/png';
        break;
        case '.jpeg':
          mimeType = 'image/jpeg';
        break;
      }
      
      reponseHTTP.writeHead(200, {
        'Content-type': mimeType,
        'Content-length': data.length
      } ); //écrire les en-têtes de réponse
      
      
      reponseHTTP.write(data, 
      function(){
        reponseHTTP.end(); //indique la fin de réponse
      }); //écrire le corps de la réponse
    }
    
  });
  

});

monServeur.listen(9999);

