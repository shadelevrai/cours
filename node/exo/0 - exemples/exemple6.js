var http = require('http');

var server = http.createServer();

server.on('request', function(req, rep){
  
          var requeteHTTP = http.request({
            protocol:'http:',
            host:'marsweather.ingenology.com',
            method:'GET',
            path:'/v1/latest/'
          }, function(reponseHTTP){
           
            reponseHTTP.setEncoding('utf8');
            
            var data = '';

            reponseHTTP.on('data', function(morceauDeData){
              console.log('downloading...');
              data += morceauDeData;
            });

            reponseHTTP.on('end', function(){
              console.log('end !');
              var dataToJSON = JSON.parse(data);
              //console.log(dataToJSON.report.min_temp);
              //console.log(dataToJSON.report.max_temp);
              var maReponse = 'La météo aujourd\'hui sur mars est au minimum ';
              maReponse += dataToJSON.report.min_temp;
              maReponse += ' °C et au maximum ';
              maReponse += dataToJSON.report.max_temp;
              maReponse += ' °C.';
              
              rep.writeHead(200, {
                'Content-length': Buffer.byteLength(maReponse),
                'Content-type': 'text/plain; charset=utf8'
              });
              rep.write(maReponse, function(){
                rep.end();
              });
            });
          });

          requeteHTTP.end();
});

server.listen(8080);
