/**
  Utilisation du module http de Node JS pour créer un serveur http de plus en plus élaboré.

  Votre serveur devra être joignable à l'URL : [protocole]://[adresse IP ou nom de domaine][:port][/ressource]

  Par exemple :
   - Protocole : http
   - Adresse IP : 10.30.20.30
   - Port : 8899
   - Ressource : /index

   Donne l'URL : http://10.30.20.30:8899/index
**/

/**
  Exercices :
    
  1. Vous devez créer un serveur HTTP qui retourne dans sa réponse HTTP un corps de réponse
    en format HTML valide si et seulement si l'URL contenue dans la requête HTTP contient 
    /index.

    Votre objet de type http.IncomingMessage contient une propriété .url vous permettant
    d'obtenir des informations relatives à l'URL employé pour effectuer la requête HTTP.
**/

var http = require('http');
var url = require('url'); // Pour récupérer la page demandée par le visiteur, on va faire appel à un nouveau module de Node appelé "url". On demande son inclusion

var server = http.createServer(function(req, res) {
    var page = url.parse(req.url).pathname; // Ensuite, il nous suffit de "parser" la requête du visiteur comme ceci pour obtenir le nom de la page demandée :
    
    console.log(page); // On affiche le lien demandé dans l'invit de commande.
    
    res.writeHead(200, {"Content-Type": "text/html"});
    if (page == '/index') {
        res.write('<!DOCTYPE html>' +
        '<html>' +
        '    <head>' +
        '        <meta charset="utf-8" />' +
        '        <title>Ma page Node.js !</title>' +
        '    </head>' +
        '    <body>' +
        '     	<p>Vous êtes à bon <strong>PORT</strong> !</p>' +
        '    </body>' +
        '</html>');
    }
    else  {
        res.write('<!DOCTYPE html>' +
        '<html>' +
        '    <head>' +
        '        <meta charset="utf-8" />' +
        '        <title>Ma page Node.js !</title>' +
        '    </head>' +
        '    <body>' +
        '     	<p>Erreur <strong>404</strong> !</p>' +
        '    </body>' +
        '</html>');
    }
    res.end();
});
server.listen(8888);
console.log('le serveur est lancé');

/**
  2. Améliorez votre serveur HTTP pour que, si l'URL employé pour effectuer la requête HTTP
  ne contient pas /index, votre serveur HTTP produise une réponse HTTP avec dans :
   - l'en-tête, un code 404;
   - le corps, un message en format HTML valide du type : L'URL demandé n'existe pas.
**/