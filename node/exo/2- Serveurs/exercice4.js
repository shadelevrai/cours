/**
  Utilisation du module http de Node JS pour créer un serveur http de plus en plus élaboré.

  Votre serveur devra être joignable à l'URL : [protocole]://[adresse IP ou nom de domaine][:port][/ressource]

  Par exemple :
   - Protocole : http
   - Adresse IP : 31.42.53.64
   - Port : 5555
   - Ressource : /accueil

   Donne l'URL : http://31.42.53.64:5555/home
**/

/**
  Exercices :
    
  1. Créez deux fichiers HTML valides : home.html et about.html

  Vous devez créer un serveur HTTP qui retourne dans sa réponse HTTP
  - le contenu du fichier home.html si l'URL utilisé pour effectuer la requête HTTP contient
  la ressource /accueil
  - le contenu du fichier about.html si l'URL utilisé pour effectuer la requête HTTP contient
  la ressource /apropos
**/
var http = require('http');
var url = require('url'); // Pour récupérer la page demandée par le visiteur, on va faire appel à un nouveau module de Node appelé "url". On demande son inclusion.
const fs = require('fs');

var server = http.createServer(function (req, res) {
    var page = url.parse(req.url).pathname; // Ensuite, il nous suffit de "parser" la requête du visiteur comme ceci pour obtenir le nom de la page demandée :

    console.log(page); // On affiche le lien demandé dans l'invit de commande.

    res.writeHead(200, {
        "Content-Type": "text/html" // On dit que les fichiers seront des HTML
    });
    if (page == '/accueil') {;
        fs.readFile('./home.html', 'utf8', function (err, data) { // Il faut mettre les paramètres qu'on va utiliser. Sans 'err', ça fonctionne pas.
            res.write(data); // 'res' est la réponse qui sera affiché. 'data' c'est le fichier qui est dans le 'fs.readFile'.
            res.end(); // on termine.
        });
    }
    if (page == '/apropos') {
        fs.readFile('./about.html', 'utf8', function(err, data){
            res.write(data);
            res.end();
        })
    } else {
        res.writeHead(404);
        fs.readFile('./404.html', 'utf8', function(err,data){
            res.write(data);
            res.end();
        })
    }
});
server.listen(8888);
console.log('le serveur est lancé');
/**
  Exercices :
    
  2. Créez un fichier HTML valide : 404.html

  Votre serveur HTTP doit retourner dans sa réponse HTTP le contenu du fichier 404.html
  si l'URL utilisé pour effectuer la requête HTTP ne contient pas la ressource /accueil ou
  /apropos. N'oubliez pas de préciser le code 404 dans les en-têtes de la réponse HTTP.
**/