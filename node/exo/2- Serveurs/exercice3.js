/**
  Utilisation du module http de Node JS pour créer un serveur http de plus en plus élaboré.

  Votre serveur devra être joignable à l'URL : [protocole]://[adresse IP ou nom de domaine][:port]

  Par exemple :
   - Protocole : http
   - Adresse IP : 1.2.3.4
   - Port : 7777

   Donne l'URL : http://1.2.3.4:7777
**/

/**
  Exercices :
    
  1. Vous devez créer un serveur HTTP qui retourne dans sa réponse HTTP un corps de réponse
    en format HTML obtenu à partir du contenu d'un fichier.

    Vous devez donc créer un fichier HTML valide à coté de votre programme.

    A chaque requête HTTP reçue, vous utiliserez les méthodes asynchrones de l'objet 
    FileSystem de Node JS pour lire et obtenir le contenu de votre fichier HTML. Puis,
    vous produirez une réponse HTTP contenant le contenu du fichier HTML.
**/

var http = require('http');
var url = require('url'); // Pour récupérer la page demandée par le visiteur, on va faire appel à un nouveau module de Node appelé "url". On demande son inclusion.
const fs = require('fs');

var server = http.createServer(function (req, res) {
        var page = url.parse(req.url).pathname; // Ensuite, il nous suffit de "parser" la requête du visiteur comme ceci pour obtenir le nom de la page demandée :

        console.log(page); // On affiche le lien demandé dans l'invit de commande.

        res.writeHead(200, {
            "Content-Type": "text/html" // On dit que les fichiers seront des HTML
        });
        if (page == '/index') {;
                fs.readFile ('./exo3.html', 'utf8', function(err,data){ // Il faut mettre les paramètres qu'on va utiliser. Sans 'err', ça fonctionne pas.
                    res.write(data); // 'res' est la réponse qui sera affiché. 'data' c'est le fichier qui est dans le 'fs.readFile'.
                    res.end(); // on termine.
                });
        } else {
    res.write('<!DOCTYPE html>' +
        '<html>' +
        '    <head>' +
        '        <meta charset="utf-8" />' +
        '        <title>Ma page Node.js !</title>' +
        '    </head>' +
        '    <body>' +
        '     	<p>Erreur <strong>404</strong> !</p>' +
        '    </body>' +
        '</html>');
            res.end();
}
});
server.listen(8888);
console.log('le serveur est lancé');