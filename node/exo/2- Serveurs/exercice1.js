/**
  Utilisation du module http de Node JS pour créer un serveur http de plus en plus élaboré.

  Votre serveur devra être joignable à l'URL : [protocole]://[adresse IP ou nom de domaine][:port]

  Par exemple :
   - Protocole : http
   - Adresse IP : 100.50.25.12
   - Port : 6666

   Donne l'URL : http://100.50.25.12:6666
**/

/**
  Exercices :
  
  1. Vous devez créer un serveur HTTP qui retourne dans sa réponse HTTP un corps de réponse en format HTML valide.
  Attention, vous devez pensez à retourner dans l'en-tête de votre réponse HTTP le Mime Type correct (pour le HTML,
  il s'agit du Mime Type text/html)
**/

var http = require('http');


var server = http.createServer(function (req, res) {
    
    res.writeHead(200, {
        "Content-Type": "text/html"
    }); // On ecrit le type de donnée que va envoyer le serveur. On peut envoyer plusieurs valeurs sous forme de tableau.
    res.write('<!DOCTYPE html>' +
        '<html>' +
        '    <head>' +
        '        <meta charset="utf-8" />' +
        '        <title>Ma page Node.js !</title>' +
        '    </head>' +
        '    <body>' +
        '     	<p>Voici un paragraphe <strong>HTML</strong> !</p>' +
        '    </body>' +
        '</html>'); // On ecrit le code HTML s'ecrira ici, dans 'res.write'. C'est mieux que de le mettre dans res.end(). 
    res.end();
});
server.listen(8888);
console.log('Le serveur est lancé');