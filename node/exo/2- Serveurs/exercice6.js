/**
  Utilisation du module http de Node JS pour créer un serveur http de plus en plus élaboré.

  Votre serveur devra être joignable à l'URL : [protocole]://[adresse IP ou nom de domaine][:port][/ressource]

  Par exemple :
   - Protocole : http
   - Adresse IP : 10.2.1.0
   - Port : 4321
   - Ressource : /ville/paris.html

   Donne l'URL : http://10.2.1.0:4321/ville/paris.html
**/

/**
  Exercices :

  1. Pour cet exercice vous reprendrez le serveur HTTP de l'exercice précédent.
  
  Votre serveur HTTP doit gérer différents Mime Types. Vous devez faire en sorte que
  le Mime Type soit conforme à l'extension obtenue à partir de la ressource dans l'URL.

  Par exemple :
  - Si l'URL est http://10.2.1.0:4321/photo.jpeg (et que le fichier photo.jpeg existe)
  - Alors l'en-tête de la réponse HTTP doit contenir Content-Type : image/jpeg
  
  Vous devez gérer les Mime Types des formats de fichier suivant : css, js, jpeg, png, pdf, gif.

  La liste des Mime Types autorisés est disponible ici : http://www.iana.org/assignments/media-types/media-types.xhtml
**/
/*
var http = require('http');
var url = require('url'); // Pour récupérer la page demandée par le visiteur, on va faire appel à un nouveau module de Node appelé "url". On demande son inclusion.
const fs = require('fs');

var server = http.createServer(function (req, res) {
    var page = url.parse(req.url).pathname; // Ensuite, il nous suffit de "parser" la requête du visiteur comme ceci pour obtenir le nom de la page demandée :

    console.log(page); // On affiche le lien demandé dans l'invit de commande.

    res.writeHead(200, {
        "Content-Type": "text/html" // On dit que les fichiers seront des HTML
    });
    if (page == '/accueil') {
        fs.readFile('./home.html', 'utf8', function (err, data) {
            res.write(data);
            res.end();
        });

    }
     else if (page == '/apropos') {
        fs.readFile('./about.html', 'utf8', function (err, data) {
            res.write(data);
            res.end();
        })

    } else if (page == '/image') {
         res.writeHead(200, {
            "Content-Type": "image/png" // On dit que les fichiers seront des HTML
        });
        fs.readFile('./01.png', function (err, data){
            res.write(data);
            res.end();
        })
    } else {
        res.writeHead(404, {
            "Content-Type": "text/html" // On dit que les fichiers seront des HTML
        });
        fs.readFile('./404.html', 'utf8', function (err, data) {
            res.write(data);
            res.end();
        })

    }

});
server.listen(8888);
console.log('le serveur est lancé');
*/

/**
  2. Utiliser votre serveur HTTP pour "servir" votre projet Front End (sur le réseau local).

  Pensez à utiliser l'onglet réseau des outils de développement de votre navigateur Internet pour
  vérifier que vous arrivez bien à télécharger toutes les ressources exigées par votre projet.

  Ajoutez la gestion des Mime Types manquants si nécessaire...
**/


var http = require('http');
var fs = require('fs');
var path = require('path');
http.createServer(function (req, res) {
    console.log('Serveur lancé');
	
	var filePath = '.' + req.url;
	if (filePath == './')
		filePath = './index.htm';
		
	var extname = path.extname(filePath);
	var contentType = 'text/html';
	switch (extname) {
		case '.js':
			contentType = 'text/javascript';
			break;
		case '.css':
			contentType = 'text/css';
			break;
	}
	
	path.exists(filePath, function(exists) {
	
		if (exists) {
			fs.readFile(filePath, function(error, content) {
				if (error) {
					res.writeHead(500);
					res.end();
				}
				else {
					res.writeHead(200, { 'Content-Type': contentType });
					res.end(content, 'utf-8');
				}
			});
		}
		else {
			res.writeHead(404);
			res.end();
		}
	});
	
}).listen(8888);
console.log('Server running');