/**
D'abord télécharger node JS. L'installer. Puis aller dans l'invite de commande en mode admin. Puis lancer le fichier JS manuellement.
cd .. = revenir au dossier parent
'touche TAB' = taper les premières lettres du dossier puis faire tab pour être plus rapide.
Une fois dans le dossier où est le fichier J, faire 'node.exe nomDuFichier.js' ou 'node nomDuFichier'


  Ce programme en JavaScript affiche 100 fois un message dans la console
  Pour l'utiliser on écrira : node exercice0.js à partir du dossier dans
  lequel se trouve le fichier.
**/
for(var i = 0; i < 100; i++){
  console.log('Bonjour ' + i + ' fois !');
}

/**
  Exercices :

    1. Exécutez ce programme avec Node JS.
**/