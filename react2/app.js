const express = require('express');
const router = express.Router();
const app = express();
var server = require('http').createServer(app);
var port = process.env.PORT || 8080;

app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/'));

app.set('view engine','jsx');
app.set('view engine', 'jade');
app.engine('jsx', require('express-react-views').createEngine());

app.get('/01', (req,res)=>{
    res.render('01HelloWorld.jsx');
})

app.get('/02', (req,res)=>{
  res.render('02MonAttribut.jsx');
})

app.get('/03', (req,res)=>{
  res.render('03 utiliser js dans le html.jsx');
})

app.get('/04', (req,res)=>{res.render('04 if else.jsx')})

app.get('/05', function(req,res){res.render('05 classname.jsx')});

app.get('/06', function (req,res){res.render('06 style et comment.jsx')});

app.get('/07', function(req,res){res.render('07 export default.jsx')});

app.get('/08', function (req,res){res.render('08 import jsx and reactDOM/index.jade')});

app.get('/082', function(req,res){res.render('08 import jsx.jsx')})

app.get('/09', function (req,res){res.render('09 state.jsx')})
app.get('/092', function(req,res){res.render('09 envoyer variable entre composant.jsx',{name:'lala'})})

app.get('/10', function(req,res){res.render('10 props/index.jade')})

app.get('/11', function (req,res){res.render('11 props par le serveur.jsx',{name:'lala'})})

app.get('/12', function(req,res){res.render('12 import variable.jsx')})

app.get('/13', function(req,res){res.render('13 children.jsx')})

app.get('/14', function(req,res){res.render('14 import function.jsx')})

app.get('/15', function(req,res){res.render('15 const element.jsx')})

app.get('/16', function(req,res){res.render('16 function.jsx')})

app.get('/17', function(req,res){res.render('17 fonction.jsx')})

var server = app.listen(port, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
  });