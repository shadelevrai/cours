import React from "react";

class App extends React.Component{
    render(){
        return(
            <div>
            <Header/>
            <Content/>
            </div>
        )
    }
}

class Header extends React.Component{
    render(){
        return(
            <p>c'est le header</p>
        )
    }
}

class Content extends React.Component{
    render(){
        return(
            <p>c'est le content</p>
        )
    }
}

//utilise que si il y a plusieurs class. Si c'est pas le cas, ça plante.
export default App;