import React from 'react'

//Toujours une majuscule quand ça sera un element
function Azerty(props){
    return <h1>hello {props.nom}</h1>;
}

export default class App extends React.Component{
    render(){
        return(
            <div>
                <Azerty nom='Shade'/>
                <Azerty nom='Djiher'/>
            </div>
        )
    }
}