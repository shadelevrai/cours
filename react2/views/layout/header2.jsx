import React from 'react'

export default class HeaderLayout extends React.Component{
    render(){
        return(
            <div>
                <p>Voici la variable que je reçois du composant parent : {this.props.azerty} </p>
                <p>On ne peut pas recevoir de variable serveur dans un composant enfant {this.props.name} </p>
            </div>
        )
    }
}