import React from 'react'

export default class App extends React.Component {
    render(){
        let i=1;
        return(
            <div>
                <h1>
                    {i==1 ? 'cest vrai' : 'cest faux'}
                </h1>
                <h2>

                    {this.props.loggedIn ? 'tu es log' : 'tu es pas log'}

                    {this.props.loggedIn ? 
                        <p>hello toi</p>
                        :
                        <p>tu es pas connecté</p>
                    }
                    
                </h2>
            </div>

        )
    }
}