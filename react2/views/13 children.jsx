import React from 'react'
import Azerty from './layout/title'

export default class App extends React.Component{
    render(){
        return(
            <div>
                <h1>parent</h1>
                <Azerty>Je suis dans le parent</Azerty> 
                Ce qui est dans le azerty n'est affiché que si le children l'affiche
            </div>
        )
    }
}