import React from 'react'
import {log} from './layout/log'//Pour importer une fonction spécifique
import * as obj from './layout/desFonctions'// Toutes les fonctions

log('salut');

export default class App extends React.Component{
    render(){

        log('Encore salut');
        console.log(obj);
        console.log(obj.pikachu);
        //Pour utiliser une fonction qui se trouve dans un objet
        obj.pikachu('hey')


        return(
            <div>
            </div>
        )
    }
}