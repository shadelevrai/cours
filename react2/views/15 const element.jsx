import React from 'react'

export default class App extends React.Component{
    render(){
        const element = (
            <div>
                <p>hello</p>
            </div>
        )

        const azerty = (
            <div>
                <h3>yeah</h3>
            </div>
        )
        return(
            <div>
                {element}
                {azerty}
            </div>
        )
    }
}