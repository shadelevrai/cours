import React from 'react'

export default class App extends React.Component{
  
    constructor(props){
        super(props);
        this.state = {
            count : 0
        }
    }

    render(){
        const {count} = this.state
        return(
            <div>
                <h1> yeah</h1>
                <button type='button' onClick={() => this.setState({count: count + 1})}>
                Click HERE to increment: {count}
                </button>
            </div>
        )
    }
}