import Data from '../data.json'
export const foo = 'RULES_LOADED'

export function loadRules() {
    return {
        type: foo,
        data : Data
    }
}
