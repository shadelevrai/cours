import { combineReducers } from'redux'
import rulesReducer from'../reducers/rules­-reducer'
import { createStore } from 'redux'

const reducer = combineReducers({rules: rulesReducer})
const store = createStore(reducer)

export default store;