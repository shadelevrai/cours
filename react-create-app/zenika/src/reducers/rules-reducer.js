import {
    foo
} from '../actions/rule-actions'

import loadRules from '../actions/rule-actions'

const initialState = {
    loadRules
  };

function ruleReducer(state = initialState, action) {
    if(action.type === foo){
        return action.data
    }
}

export default ruleReducer