import React from 'react'
// import films from '../Helpers/filmsData'
import FilmItem from '../Component/filmItem'
import FilmList from '../Component/FilmList'
import { TextInput, Button, View, Text, StyleSheet, FlatList, ActivityIndicator  } from 'react-native'
import { getFilmsFromApiWithSearchedText } from '../API/TMDBApi'
import {connect} from 'react-redux'

class Search extends React.Component {

    constructor(props) {
        super(props)
        this.searchedText = "";
        this.page = 0;
        this.totalPages = 0;
        this.state = {
            films:[],
            isLoading: false
        }
      }

    _loadFilms=()=>{
        if (this.searchedText.length > 0) { // Seulement si le texte recherché n'est pas vide
        this.setState({ isLoading: true })
          getFilmsFromApiWithSearchedText(this.searchedText, this.page+1).then(data => {
              this.page = data.page;
              this.totalPages = data.total_pages;
              this.setState({ 
                films: [ ...this.state.films, ...data.results ],
                  isLoading: false 
                })
          })
        }
    }
    _searchTextInputChanged=(text)=> {
        this.searchedText = text
    }

    _searchFilms() {
      this.page = 0
      this.totalPages = 0
      this.setState({
        films: [],
      }, () => { 
          // console.log("Page : " + this.page + " / TotalPages : " + this.totalPages + " / Nombre de films : " + this.state.films.length)
          this._loadFilms() 
      })
  }

    _displayLoading() {
      if (this.state.isLoading) {
        return (
          <View style={styles.loading_container}>
            <ActivityIndicator size='large' />
          </View>
        )
      }
      }

    _displayDetailForFilm = (idFilm) => {
        // console.log("Display film with id " + idFilm);
        this.props.navigation.navigate('FilmDetail',{idFilm: idFilm});
      }

    render(){
        // console.log(this.props)
        return(
                <View style={styles.view1}>
                    <View style={styles.view11}></View>
                    <View style={styles.view12}>
                        <TextInput style={styles.textinput} placeholder='Titre du film' onChangeText={(text) => this._searchTextInputChanged(text)} onSubmitEditing={() => this._loadFilms()}/>
                        <Button title='Rechercher' onPress={()=> this._searchFilms()}/>
                        <FilmList
          films={this.state.films} // C'est bien le component Search qui récupère les films depuis l'API et on les transmet ici pour que le component FilmList les affiche
          navigation={this.props.navigation} // Ici on transmet les informations de navigation pour permettre au component FilmList de naviguer vers le détail d'un film
          loadFilms={this._loadFilms} // _loadFilm charge les films suivants, ça concerne l'API, le component FilmList va juste appeler cette méthode quand l'utilisateur aura parcouru tous les films et c'est le component Search qui lui fournira les films suivants
          page={this.page}
          totalPages={this.totalPages} // les infos page et totalPages vont être utile, côté component FilmList, pour ne pas déclencher l'évènement pour charger plus de film si on a atteint la dernière page
        />
                    </View>
                    <View style={styles.view13}>
                        <View style={{flex:1,backgroundColor:'yellow',justifyContent:'center'}}>
                            <View style={{backgroundColor:'purple',width:80,height:80}}></View>
                        </View>
                        <View style={{flex:1,backgroundColor:'blue',alignItems:'center'}}>
                            <View style={{backgroundColor:'grey',width:80,height:80,justifyContent:'center',alignItems:'center'}}>
                                <View style={{backgroundColor:'azure',width:40,height:40}}></View>
                            </View>
                        </View>
                    </View>
                    {this._displayLoading()}
                </View>
        )
    }
}

const styles = StyleSheet.create({
        textinput: {
          marginLeft: 5,
          marginRight: 5,
          height: 50,
          borderColor: '#000000',
          borderWidth: 1,
          paddingLeft: 5
        },
        view1: {
            flex:1,
            backgroundColor:'yellow'
        },
        view11:{
            flex:1,
            backgroundColor:'red'
        },
        view12:{
            flex:5,
            backgroundColor:'antiquewhite'
        },
        view13: {
            flex:1,
            backgroundColor:'grey',
            flexDirection:'row'
        },
        loading_container: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 100,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center'
          }
})

// On connecte le store Redux, ainsi que les films favoris du state de notre application, à notre component Search
const mapStateToProps = state => {
  return {
    favoritesFilm: state.favoritesFilm
  }
}

export default connect(mapStateToProps)(Search)