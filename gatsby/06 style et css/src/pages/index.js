import React from "react"

export default () => <div>
    <p>Dans le dossier src, il faut créer un dossier styles</p>
    <p>Dans le dossier styles, créer un fichier css</p>
    <p>A la racine du projet, créer un fichier gatsby-browser.js</p>
    <p>Dans ce fichier js, il faut importer le fichier css : import "./src/styles/global.css"</p>
</div>
