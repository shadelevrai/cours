import React from "react"
import Header from '../components/header'

export default () => <div>
    <p>Pour utiliser un composan ici, il faut en créer un dans le dossier components et le déclarer ici comme dans l'exemple ci dessus. Attention à bien mettre une majuscule.</p>
    <Header></Header>
    <Header />
</div>
