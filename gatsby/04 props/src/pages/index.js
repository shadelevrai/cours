import React from "react"
import Header from "../components/header"

export default () => <div>
    <p>On peut envoyer des données de parents à enfants. Le parent étant le fichier index.js et l'enfant étant le fichier header.js qui est dans le dossier component</p>
    <Header headerText="Je suis une donnée" anotherText="juste un autre texte"/> 
    <Header headerText="cool non ?"/>
</div>
