import React from "react"

import Container from "../components/container"

export default () => (
  <Container>
    <p>01 : Dans le dossier components, il faut créer un module css. Exemple : container.module.css</p>
    <p>container étant le nom du module</p>
    <p>Dans le fichier container.module.css, il faut créer un style. Exemple : .container *accolade*color:red;*accolade*</p>
    <p>Il faut importer le fichier container.module.css comme ci dessus</p>
    <p>Faire une div qui va avoir la className containerStyles.container comme ci dessous</p>
    <p>Si on veut afficher ce qui y a dans le fichier about-css-module.js, ne pas oublier de mettre children comme ci dessous</p>
  </Container>
)