const express = require('express');
const app = express();
const MongoClient = require('mongodb').MongoClient;
//il faut d'abord lancer le serveur mongod en ligne de commande 'C:\Program Files\MongoDB\Server\3.6\bin\mongod.exe
const url = "mongodb://localhost:27017";
var server = require('http').createServer(app);
var port = process.env.PORT || 8080;

var server = app.listen(port, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
  });