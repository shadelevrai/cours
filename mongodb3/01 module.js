const express = require('express');
const app = express();
//Il faut installer le module mongodb
const MongoClient = require('mongodb').MongoClient;
var server = require('http').createServer(app);
var port = process.env.PORT || 8080;

var server = app.listen(port, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
  });