const express = require('express');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017";
var server = require('http').createServer(app);
var port = process.env.PORT || 8080;

var server = app.listen(port, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
  });

  //Pour changer le dossier où sera présent la base de donnée, il faut :

  //1: Créer un fichier de configuration 'mongod.conf' dans le dossier 'bin' de mongo
  //2: mettre ce code avec le chemin adéquat: 
  /* 
   systemLog:
    destination: file
    path: D:\Drive\fichier shade\dev web\project\gaijin-game-ver0.3\data\log\mongod.log
storage:
    dbPath: D:\Drive\fichier shade\dev web\project\gaijin-game-ver0.3\data\db
 */

 //3: lancer mongod en ligne de commande avec la configuration. Exemple : 'mongod.exe --config mongod.conf
