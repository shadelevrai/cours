const express = require('express');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017";
var server = require('http').createServer(app);
var port = process.env.PORT || 8080;

var server = app.listen(port, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
  });

  //Par défaut, la base de donnée sera dans le dossier C:\data\db 
  //Si le dossier n'éxiste pas, il faut le créer manuellement