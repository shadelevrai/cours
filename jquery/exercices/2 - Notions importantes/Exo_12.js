$(document).ready(function () {

    //1
    $('section div p:first-child').attr({
        class: 'red'
    });

    //2
    $('#message').html('<a href="#">' + $('h1').html() + '</a><p id="jour"></p>');

    //3
    var maDate = new Date();

    var weekday = new Array(7);
    weekday[0] = "Dimanche";
    weekday[1] = "Lundi";
    weekday[2] = "Mardi";
    weekday[3] = "Mercredi";
    weekday[4] = "Jeudi";
    weekday[5] = "Vendredi";
    weekday[6] = "Samedi";

    $('#jour').html(weekday[maDate.getDay()]);

    //4
    var valeurAct = $('input[type=submit]').val();

    $('input[type=submit]').attr('value', valeurAct + ' Le ' + weekday[maDate.getDay()]);

});