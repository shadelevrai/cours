$(function () {
    $('input').blur(function () {
        var message = '';
        $('input').each(function () {
            message += ' - ' + $(this).val();
        });
        $('#message').html(message);
    });
})