$(function () {

    $('body').prepend('<div class="bloc"></div>');
    $('.bloc').css({
        width: '20px',
        height: '20px',
        backgroundColor: 'aqua',
        padding: '10px',
    })

    var augmentePadding = 15;
    setInterval(function () {
        console.log($('.bloc').width());
        if ($('.bloc').width() < 100) {
            var valeurWidth = $('.bloc').width() + 20;
            var valeurHeight = $('.bloc').height() + 20;
            $('.bloc').width(valeurWidth);
            $('.bloc').height(valeurHeight);
            $('.bloc').css({
                padding: augmentePadding + 'px',
            })
            augmentePadding += 10;
            console.log(valeurHeight, valeurWidth, augmentePadding);
        } else if ($('.bloc').width() == 100){
            valeurWidth = $('.bloc').width() - 20;
            valeurHeight = $('.bloc').height() - 20;
            $('.bloc').width(valeurWidth);
            $('.bloc').height(valeurHeight);
            $('.bloc').css({
                padding: augmentePadding + 'px',
            })
            augmentePadding-= 10;
        };
    }, 1000);

})