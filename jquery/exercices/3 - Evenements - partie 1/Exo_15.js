$(document).ready(function () {
    //1
    $(window).resize(function () {
        var largeurEcran = $("body").width();

        if (largeurEcran <= 920) {
            $('p').css({
                'color': 'red'
            });
            if (largeurEcran <= 640) {
                $('body:not(header)').css({
                    'font-size': '11px'
                });
            }
        };

        if (largeurEcran > 920) {
            $('p').css({
                'color': 'black'
            });
            $('body:not(header)').css({
                'font-size': '16px'
            });
        }


    });
    //2
    $(window).scroll(function () {
        $('#carrer').css({
            'visibility': 'visible'
        });
        setTimeout(function () {
            $('#carrer').css({
                'visibility': 'hidden'
            });
        }, 100);
    });



});

console.log($('body').css('width'));