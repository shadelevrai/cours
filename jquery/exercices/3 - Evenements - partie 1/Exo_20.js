$(function () {
    $('body').prepend('<div class="bloc1"></div>');
    $('.bloc1').css({
        width: '50px',
        height: '50px',
        backgroundColor: 'red',
        marginTop: '400px',
        marginLeft: '900px',
    });
    var deplacementADroiteOuGauche = 900;
    var deplacementEnHautOuBas = 400;
    $('body').keydown(function (event) {
        $('.bloc1').css({
            backgroundColor: 'white',
        })
        console.log(event.which);
        switch (event.which) {
        case 39:
            deplacementADroiteOuGauche += 2;
            $('.bloc1').css({
                marginLeft: deplacementADroiteOuGauche + 'px'
            });

            break;

        case 37:
            deplacementADroiteOuGauche -= 2;
            $('.bloc1').css({
                marginLeft: deplacementADroiteOuGauche + 'px'
            })

            break;

        case 40:
            deplacementEnHautOuBas += 2;
            $('.bloc1').css({
                marginTop: deplacementEnHautOuBas + 'px'
            })
            break;

        case 38:
            deplacementEnHautOuBas -= 2;
            $('.bloc1').css({
                marginTop: deplacementEnHautOuBas + 'px'
            })
            break;

        };
    })
    $('body').keyup(function () {
        $('.bloc1').css({
            backgroundColor: 'red',
        })
    });
})