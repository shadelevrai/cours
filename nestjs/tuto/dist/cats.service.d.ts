import { Model } from 'mongoose';
import { CreateCatDto } from './create-cat.dto';
export declare class CatService {
    private readonly catModel;
    constructor(catModel: Model<CreateCatDto>);
    createCat(cat: CreateCatDto): Promise<CreateCatDto>;
}
