import { CatService } from "./cats.service";
import { CreateCatDto } from "./create-cat.dto";
export declare class CatsController {
    private readonly catService;
    constructor(catService: CatService);
    findAll(): string;
    create(catDto: CreateCatDto): Promise<CreateCatDto>;
    findOne(params: any): string;
}
