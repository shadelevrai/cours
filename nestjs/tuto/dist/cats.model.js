"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CatSchema = void 0;
const mongoose_1 = require("mongoose");
exports.CatSchema = new mongoose_1.Schema({
    name: String,
    age: Number,
    breed: String,
});
//# sourceMappingURL=cats.model.js.map