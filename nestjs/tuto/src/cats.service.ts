import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateCatDto } from './create-cat.dto';

@Injectable()
export class CatService {
  constructor(@InjectModel('Cat') private readonly catModel: Model<CreateCatDto>) {}

  async createCat(cat: CreateCatDto): Promise<CreateCatDto> {
    const createdCat = new this.catModel(cat);
    return await createdCat.save();
  }
}