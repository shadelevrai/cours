import { Controller, Get, Post } from "@nestjs/common"
import { Body, Param } from "@nestjs/common/decorators"
import { CatService } from "./cats.service"
import { CreateCatDto } from "./create-cat.dto"

@Controller("cats")
export class CatsController {

    constructor(private readonly catService: CatService) {}

    @Get()
    findAll(): string {
        return "liste des chats"
    }
    @Post()
    // create(@Body() catDo: CreateCatDto): string {
    //     console.log(catDo)
    //     return "chat crée"
    // }
    async create(@Body() catDto: CreateCatDto): Promise<CreateCatDto> {
        const createdCat = await this.catService.createCat(catDto);
        return createdCat;
      }
    @Get(":id")
    findOne(@Param() params: any): string {
        console.log(params.id)
        return `return ${params.id}`
    }
}