import { Schema, Types } from 'mongoose';

export const CatSchema = new Schema({
  name: String,
  age: Number,
  breed: String,
});