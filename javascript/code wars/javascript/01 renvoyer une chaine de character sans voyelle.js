//Your task is to write a function that takes a string and return a new string with all vowels removed. For example, the string "This website is for losers LOL!" would become "Ths wbst s fr lsrs LL!". Note: for this kata y isn't considered a vowel.
function disemvowel(str) {
    let withoutVowels = []
    for (let i = 0; i < str.length; i++) {
        if (str[i] === 'a' || str[i] === 'e' || str[i] === 'u' || str[i] === 'i' || str[i] === 'o' || str[i] === 'A' || str[i] === 'E' || str[i] === 'U' || str[i] === 'I' || str[i] === 'O') {
            console.log(str[i])
        } else {
            withoutVowels.push(str[i])
        }
    }
    console.log(withoutVowels)
    return withoutVowels.join('')
}

//Best practice

function disemvowel(str) {
    return str.replace(/[aeiou]/gi, '');
}
/**************************** */
disemvowel = str => str.replace(/[aeiou]/gi, '');

/**************************** */
function disemvowel(str) {
    var vowels = ['a', 'e', 'i', 'o', 'u'];

    return str.split('').filter(function (el) {
        return vowels.indexOf(el.toLowerCase()) == -1;
    }).join('');
}

/***************************** */
const disemvowel = (str) => {
    const vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'];
    let newStr = '';
    for (let i = 0; i <= str.length; i++) {
        let char = str.charAt(i);
        if (vowels.indexOf(char) == -1) {
            newStr += char;
        }
    }
    return newStr;


};