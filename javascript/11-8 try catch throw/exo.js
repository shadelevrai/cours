//https://javascript.info/try-catch

/***************************************************** */
// let json = '{ "age": 30 }'; // incomplete data

// try {

//   let user = JSON.parse(json); // <-- no errors

//   if (!user.name) {
//     throw new SyntaxError("Incomplete data: no name"); // (*)
//   }

//   //Pas de console.log car il y a une erreur
//   console.log( user.name );

// } catch(e) {
//   console.log( "JSON Error: " + e.message ); // JSON Error: Incomplete data: no name
// }

/****************************************************************** */

let json = '{ "age": 30 }'; // incomplete data

try {
  user = JSON.parse(json); // <-- forgot to put "let" before user

  // ...
} catch(err) {
  alert("JSON Error: " + err); // JSON Error: ReferenceError: user is not defined
  // (no JSON Error actually)
}




//pour le throw : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/throw