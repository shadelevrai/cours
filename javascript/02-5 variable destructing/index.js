// Why use destructuring in JavaScript?

// Destructuring enables us to turn object key-value pairs into variables.

//     Destructuring is a great convenience because we often don’t need to reference an entire object whenever we want to use it.
//     With destructuring, we can just create what looks to some like a reversed version of the object and pluck off what ever values we need, making them independent variables
//     Allows us to reduce repetition by not referencing a single object every time we need a value from it.

// Note that destructuring can be also done with arrays as well as normal objects

const user = {
    name: "Reed",
    username: "ReedBarger",
    email: "reed@gmail.com",
    details: {
      title: "Programmer"
    }
  };
  
  // object property access without destructuring
  console.log(`${user.name}, ${user.email}`); // logs: Reed, reed@gmail.com
  
  // object destructuring for less repetition
  const { name, email } = user;
  
  console.log(`${name}, ${email}`); // logs: Reed, reed@gmail.com
  
  // object destructuring with nested object "details"
  const { username, details: { title } } = user;
  
  console.log(`${username}, ${title}`); // logs: ReedBarger, Programmer


//   How is destructuring used in React?

// Destructuring is used most often for getting individual values from the props object

//     Most often, we don’t need the entire props object, especially if we pass down only one prop to a given component. Instead of referencing ‘props’, we can just destructure it to get the props data as individual variables in our component.

function App() {
    return (
      <div>
        <h1>All Users</h1>
        <UserList users={["Bob", "Jane", "Fred"]} />
      </div>
    );
  }
  
  function UserList({ users }) {
    return (
      <ul>
        {users.map((user, index) => (
          <li key={index}>{user}</li>
        ))}
      </ul>
    );
  }