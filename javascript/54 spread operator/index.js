// The spread operator allows us to “spread” objects (their key-value pairs)
// into new ones.     Spread operator only works when creating a new object or
// array. Spread operator is great for creating new objects by merging their
// properties together.     Whenever an object or array is spread into a new
// object or array, a shallow copy of it is made, which helps prevent errors.
// The spread operator can be used with both objects and arrays. Merge default
// empty data with user data from a sign up form with spread operator
const user = {
    name: "",
    email: "",
    phoneNumber: ""
};

const newUser = {
    name: "ReedBarger",
    email: "reed@gmail.com"
};

/*
    the object that is spread in last overwrites the previous object's values
    if the properties have the same name
  */
const mergedUser = {
    ...user,
    ...newUser
};
mergedUser; // { name: "ReedBarger", email: "reed@gmail.com", phoneNumber: "" };

//
//Ajouter une propriété dans un objet
const mama = {
    property1: '001',
    property2: "002"
}

const lala = {
    ...mama,
    property3: "003"
}

// console.log(lala) { property1: '001', property2: '002', property3: '003' }

const momo = {
    data: {
        property1: '001',
        property2: "002"
    }
}

const lili = {
    ...momo,
    data: {
        ...momo.data,
        property3: "003"
    }
}
console.log("//console.log -> lili", lili) // { data: { property1: '001', property2: '002', property3: '003' } }

//Supprimer une propriété dans un objet
const tata = {
    property1: '001',
    property2: "002",
    property3: "003"
}

const {
    property1,
    ...rest
} = tata

console.log("rest", rest) //{ property2: '002', property3: '003'}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/De
// structuring_assignment#Rest_in_Object_Destructuring 
//Dans un hook ça se fait comme ça :

setArticle((prevState) => ({
    ...prevState,
    data: {
        ...prevState.data,
        [document]: value
    }
}))