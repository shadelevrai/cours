// var monOutil = function(){
//     var resultat = 15 + 84;
//     alert(resultat);
// };
// // Pour lancer la fonction
// monOutil();

// //La fonction sera invoqué par le html
// var maFonction = function(){
// alert('La fonction alert est déclenchée dans la fonction contenue dans la variable maFonction');
// };

/********************************************** */

function function1(p1) {
    console.log(p1)
}

function1(5) //5

/******************************************* */

function function2(p1) {
    return p1
}

console.log(function2(4)) //4

/*********************    ES6 : les fonctions flechées     ***************** */
//Grace en react, plus besoin de bind() avec les fonctions fléchées

hello = () => "Hello World!";
// Parfois, pas besoin de return

/************************************************** */

const myFn = x => x + 1;

console.log(myFn(1)) //2


/********************************************* */

const myFn2 = x => (
    x +
    1 // Ici vous pouvez faire du multi lignes tranquille, on peut imaginer du JSX
  )

  console.log(myFn2(45)) // 46

  /*************************************** */

//   Les exemples suivants sont donc tous les mêmes :

const myFn3 = (x) => {
  return x + 1;
};
// ===
const myFn4 = (x) => x + 1;
// ===
const myFn5 = x => x + 1;
// ===
const myFn6 = x => (x + 1);


/********************************************* */

const aFn7 = obj => ({ key: obj.value }); // It works !
