var bissextile = function () {
    var annee;
    while (isNaN(annee)) {
        annee = parseInt(prompt('Saisissez une année !'));
    };
    if ((annee % 4 == 0 && annee % 100 != 0) || (annee % 400 == 0)) {
        alert('Cette année est bissextile');
    } else {
        alert('Cette année n\'est pas bissextile');
    };
};

bissextile();