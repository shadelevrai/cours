var monTableau = ['lunette', 15, 3.7, 'soleil'];
alert(monTableau[3]);
var secondTableau = ['un ', 2, 'trois ', 4.5, 'un dernier texte.'];
alert(secondTableau[2]);
alert(secondTableau[3]);
var unNombre1 = secondTableau[0] + secondTableau[2] + secondTableau [4];
alert(unNombre1);

var array = [1,2,3];

console.log(array.join('-'))
//1-2-3

//push ajoute un element
// array[0] = 1 met un element

//En javascript, il est interdit de mettre un chiffre après un point

array.length = 0;
// Vider un tableau 

array.unshift(5);
// Ajoute un element au début du tableau 

array.shift(2);
// Retire un element au début du tableau 

array.pop(3);
// supprime à la fin 

const array1 = [1,2,3]
const array2 = [4,5,6]

array1.push(...array2)
//array1 = [1,2,3,4,5,6]

const foo = arr1.concat(arr2);
console.log(foo);
// [1,2,3,4,5,6]
//sans modifier arr1

var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.splice(2, 0, "Lemon", "Kiwi");
// The splice() method adds/removes items to/from an array, and returns the removed item(s).
// Note: This method changes the original array.

var fruits2 = ["Banana", "Orange", "Lemon", "Apple", "Mango"];
var citrus2 = fruits.slice(1, 3);

// The slice() method returns the selected elements in an array, as a new array object.
// The slice() method selects the elements starting at the given start argument, and ends at, but does not include, the given end argument.
// Note: The original array will not be changed.