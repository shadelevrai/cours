// Powerful Array Methods (.map(), .filter(), .reduce(), etc.)

// Why use powerful array methods in JavaScript?

// As compared to using a for-loop to loop over arrays, array methods like map, filter, and reduce enable us to loop over arrays with a certain goal in mind

//     .map() — allows us to transform each element of an array
//     .filter() — allows us to filter out items from arrays that don’t meet a given condition
//     .reduce() — allows us to transform an entire array in whatever way we choose (even into other data types)

// These array methods are shorter and more declarative (more clearly express what they do) than a normal for-loop

// Goal: turn array of users into array of usernames
const users = [
    { name: "Bob", id: 1 },
    { name: "Jane", id: 2 },
    { name: "Fred", id: 3 }
  ];
  const usernames = [];
  
  // for-loop
  for (let i = 0; i < users.length; i++) {
    usernames[i] = users[i];
  }
  
  usernames; // ["Bob", "Jane", "Fred"]
  
  // .map() - concise + readable
  const usernames = users.map(user => user.username);
  
  usernames; // ["Bob", "Jane", "Fred"]



//   How are powerful array methods used in React?

// Methods like .map(), .filter(), .reduce() can be used wherever we need to transform or shape our array data

// Most of the time, you’ll be using these methods to dynamically display components or elements using JSX

//     These methods can be chained together to perform one transformation after another

function UserList() {
    const users = [
      { name: "Bob", id: 1 },
      { name: "Jane", id: 2 },
      { name: "Fred", id: 3 }
    ];
  
    // filter out user with id of 2, then map over the rest to display their names
    return (
      <ul>
        {users
          .filter(user => user.id !== 2)
          .map(user => (
            <li key={id}>{user.name}</li>
          ))}
      </ul>
    );
  }

  