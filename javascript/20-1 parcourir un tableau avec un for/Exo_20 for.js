/****************************************** */

// https://thecodebarbarian.com/for-vs-for-each-vs-for-in-vs-for-of-in-javascript.html


var monTableau = ['paris', 'Toulouse', 'Nancy'];


for (let index = 0; index < monTableau.length; index++) {
  const element = monTableau[index];
  console.log(element)
}

// mieux 

for(let i=0,l=monTableau.length;i<l;i++){
  console.log('lala')
}
// Mettre le tableau dans une variable permet de ne pas regarder la taille de tableau à chaque itération

/********************************************* */
//Introduite en ES5, la methode forEach permet d’itérer sur les propriétés d’un tableau

const list = ['a', 'b', 'c']
list.forEach((item, index) => {
  console.log(item) //value
  console.log(index) //index
})

//index is optional
list.forEach(item => console.log(item))

/********************************************** */

for (const v of list) {
  console.log(v);
}

/******************************************** */

//Dans ce cas, on peut directement déclarer une variable contenant un tableau
for (const value of ['aaa', 'bbbb', 'cccccc']) {
  console.log(value) //value
}

let lala = [1, 2, 3];
for (const value of lala) {
  console.log(value)
}

//Pareil, mais avec deux variable dont un index
for (const [index, value] of ['a', 'b', 'c'].entries()) {
  console.log(index) //index
  console.log(value) //value
}

/********************  IN   *********** */

const arr = ['a', 'b', 'c'];
for (let i in arr) {
  console.log(arr[i]);
}

/*********************************************** */

const arr2 = ['a', 'b', 'c'];
arr2.test = 'abc';

for (const el of arr2) {
  console.log(el);
}
// L'objet dans le tableau est ignoré

/********************************** */
// fonction map
// La fonction map se lance plus rapidement que forEach

let arr3 = ['a', 'b', 'c'];

arr3.map((item, index) => console.log(item))


function multiply (a,b){
  return a*b;
}

function square(n){
  return  multiply(n,n)
}

function printSquare(n){
  var squared = square(n)
  console.log(squared)
}

printSquare(4)