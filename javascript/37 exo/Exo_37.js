
var moyenne = function () {
    //Nous prenons en compte le manque d'argument
    if (arguments[1]) {
        var indice = 0;
        var total = 0;
        while (arguments[indice]) {
            total += arguments[indice];
            indice++;
        }
        alert(total / indice);
    } else {
        alert('Il manque des arguments.')
    }
};