
var test = function (nombre) {
    if (nombre >= 10) {
        console.log("Entrée");
    } else {
        console.log("Sortie");
    }
}

test(10);
test(15);
test(7);
test(10);
test(15);
test(7);
test(10);
test(15);
test(7);
/************************************ */

let momo = 7;

if(momo == 5) {
    console.log('yeaaah')
} else if(momo == 6) {
    console.log('yeah ?')
} else {
    console.log('ben non')
}

/*********************************** */
let lala = true;

if(lala){
    console.log('ok')
}

/****************************** */


// On utilise quand il y aura deux choix. L'équivalent de 
//if(){} else {}
let tata = false;
let titi = tata ? 'oui' : 'non';
console.log(titi);

/************************************ */

//On utilise le & si on donne une valeur ou mimi est true ou équivament une valeur. L'équivalent de 
//if(){}
let mimi = 5;
// let mimi = false;
let tyty = mimi && 'lalala';
console.log(`tyty is ${tyty}`) //mimi is 0

/*************************************** */

function getFee(isMember) {
    return (isMember ? "$2.00" : "$10.00");
  }
  
  console.log(getFee(true));
  // expected output: "$2.00"
  
  console.log(getFee(false));
  // expected output: "$10.00"
  
  console.log(getFee(1));
  // expected output: "$2.00"