var uneVariable;
//Une variable peut contenir un nombre.
var uneAureVariable;
uneAutreVariable = 15;
//ou plus court.
var uneAutreVariable = 17;
//On peut stocker dans une variable des nombres à virgules (nombre flottant).
var encoreUneAutreVariable = 47.23;
//une variable peut contenir une chaine de caractère (en anglais = string).
var etEncoreUneAutreVariable = 'bonjour';
//il faut mieux utiliser les guillements simple pour du javascript et double pour html.
var unePhrase = 'Aujourd\'hui nous sommes le 1er avril;
//Quand on veut utiliser un apostrophe dans une chaine de caractères (string), on utilise un \ avant la '.