class Animal {
    constructor(name, weight) {
        this.name = name;
        this.weight = weight;
    }

    eat() {
        return `${this.name} is eating!`;
    }

    sleep() {
        return `${this.name} is going to sleep!`;
    }

    wakeUp() {
        return `${this.name} is waking up!`;
    }

}

const lala = new Animal('gorille', 300)

console.log(lala)
// Animal { name: 'gorille', weight: 300 }

console.log(lala.eat())
// gorille is eating!