//https://javascript.info/try-catch

/***************************************************** */
let json = "{ bad json }";

try {

  let user = JSON.parse(json); // <-- when an error occurs...
  console.log( user.name ); // doesn't work

} catch (e) {
  // ...the execution jumps here
  console.log( "Our apologies, the data has errors, we'll try to request it one more time." );
  console.log( e.name ); // SyntaxEror
  console.log( e.message ); // Unexpected token b in JSON at position 2
}

/******************************************************* */
