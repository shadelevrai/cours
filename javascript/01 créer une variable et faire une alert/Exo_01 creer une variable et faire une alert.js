// // alert('bonjour');
// // var soleil = 7;
// // alert(soleil);
// // var unePhrase = 'L\'activité de ce début de journée c\'est l\'apprentissage';
// // alert(unePhrase);

// //On peut juste déclarer une variable sans lui donner une valeur, mais elle sera undefined.
// // let soleil2;
// // console.log(`voici la variable soleil2 : ${soleil2}`);

// function test(){
//     if( 1 === 1 ){
//       let maVar = true;
//       console.log(maVar);
//       // Retourne true
//     }
//   console.log(maVar);
//     // Retourne une erreur, let n'existe pas hors du bloc "if"
//   }

//   test()

//   // Si c'est un let, la variable n'existe que dans le if
//   // Si c'est un var, la variable existe dans la function

//   // Une const ne peut pas être changé, sauf si c'est par exemple un tableau et qu'on rajoute une valeure dans le tableau
//   const lala = 'lolo';
//   // Si c'est un const, on doit mettre une valeure lors de sa déclaration
//   // Le const a la même portée qu'un let

// var name = "shade"
// alert(name)
// alert(name)
// alert(name)

var tableau = ["yaya","shade","lou",'david']

for (let index = 0; index < tableau.length; index++) {
  alert(tableau[index])
  
}

function func(){
  alert("ayaya")
}