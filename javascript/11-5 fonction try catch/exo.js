//https://javascript.info/try-catch

// le block try se lance
try {
    const pokemon = 'pikachu';
} catch(err) {
  console.log(`erreur : ${err}`)
};
// Pas d'erreur donc le block catch est ignoré

/************************* */

try {
    lala;
} catch(err) {
  console.log(`erreur : ${err}`)
};
// lala n'existe pas, il y a donc une erreur. Lors d'une erreur, le block catch se lance. Le script ne plantera pas (c'est l'intéret du try catch).


/********************** */


try {
    setTimeout(function() {
      noSuchVariable; // script will die here
    }, 1000);
  } catch (e) {
    alert( "won't work" );
  }
  //Si on faire un settimeout, on ne doit pas le faire de cette manière car dès que le cript voit une erreur, il arrête tout. Mais plutot comme ça :

  setTimeout(function() {
    try {
      noSuchVariable; // try..catch handles the error!
    } catch {
      alert( "error is caught here!" );
    }
  }, 1000);