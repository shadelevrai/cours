// Why use arrow functions in JavaScript?

// Arrow functions allow us to write functions in a shorter syntax, resulting in less boilerplate code for our functions

//     Allows us to replace the return keyword and the function body (curly braces) with a fat arrow syntax: =>
//     It also makes working with objects and classes easier due to how it handles the this keyword.

// Arrow functions come with 3 shorthands, meaning even shorter functions

//     The parentheses around parameters can be removed if there is just one
//     Curly braces for function body can be removed entirely
//     No need for the return keyword; arrow functions have an implicit return (they return by default without curly braces)

// normal function
function capitalize(word) {
    return word.toUpperCase();
  }
  
  // arrow function
  const capitalize = word => {
    return word.toUpperCase();
  };
  
  // arrow function with all 3 shorthands
  const capitalize = word => word.toUpperCase();