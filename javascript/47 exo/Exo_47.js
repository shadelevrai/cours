var digit = function () {
    var nombre = '';
    while (isNaN(nombre) || nombre == '') {
        nombre = prompt('Veuillez saisir un nombre');
    }
    var digit = 1;
    for (i = 10; nombre > i; i = i * 10) {
        digit++;
    }
    var message = 'Le nombre ' + nombre + ' compte ' + digit + ' digit';
    if (digit > 1) {
        message = message + 's';
    }
    alert(message + '.');
}; 