#define ADVANCED_FORMATTING 1

int ft_printf(const char *format, ...) {
    // Code de base pour gérer les formats simples
    // ...

    #ifdef ADVANCED_FORMATTING
    // Code additionnel pour gérer les formats avancés
    // Par exemple, gestion des largeurs de champ, précision, etc.
    #endif
}
