#include <stdio.h>
#include <string.h>
#include <ctype.h>

// Fonction pour convertir un caractère hexadécimal en entier
int hexCharToInt(char c)
{
    if (c >= '0' && c <= '9')
        return c - '0';
    if (c >= 'A' && c <= 'F')
        return 10 + c - 'A';
    if (c >= 'a' && c <= 'f')
        return 10 + c - 'a';
    return 0;
}

// Fonction pour convertir une chaîne hexadécimale en entier décimal
int hexStringToInt(char *hex)
{
    int result = 0;
    int base = 1; // Base représente la puissance de 16 (16^0 initialement)

    // On commence par la fin de la chaîne pour traiter le chiffre le moins significatif en premier
    int length = strlen(hex);
    for (int i = length - 1; i >= 0; i--)
    {
        int digit = hexCharToInt(hex[i]);
        result += digit * base;
        base *= 16; // Augmenter la puissance de la base à chaque étape
    }

    return result;
}

int main()
{
    char hexString[] = "1A3F";
    int decimalValue = hexStringToInt(hexString);
    printf("Le nombre hexadécimal %s est %d en décimal.\n", hexString, decimalValue);
    return 0;
}

/*

Explication du code :

    Fonction hexCharToInt :
        Convertit un seul caractère hexadécimal en sa valeur entière équivalente. Par exemple, 'A' ou 'a' devient 10, 'B' ou 'b' devient 11, et ainsi de suite jusqu'à 15 pour 'F' ou 'f'.

    Fonction hexStringToInt :
        Convertit une chaîne hexadécimale complète en un entier décimal.
        Itère sur chaque caractère de la chaîne de droite à gauche, convertissant chaque caractère en un entier et le multipliant par la puissance appropriée de 16.
        Accumule ces valeurs dans result.

    Dans main :
        La chaîne hexadécimale "1A3F" est convertie en entier, et le résultat est affiché.*/