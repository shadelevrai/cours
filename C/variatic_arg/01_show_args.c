#include <stdarg.h>
#include <stdio.h>

void print_args(int count, ...)
{
    // Déclare une variable pour gérer la liste d'arguments
    va_list args;
    // Initialise args pour qu'elle pointe sur le premier argument variable après count
    va_start(args, count);

    for (int i = 0; i < count; i++)
    {
        // Récupère le prochain argument de la liste, supposé être un entier (int)
        int value = va_arg(args, int);
        printf("Argument %d: %d\n", i + 1, value);
    }
    // Nettoie la liste d'arguments pour éviter des problèmes de mémoire.
    va_end(args);
}

int main()
{
    print_args(5, 10, 20, 30, 40, 50);
    return 0;
}
