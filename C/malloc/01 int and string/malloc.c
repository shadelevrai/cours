#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    //On alloue de la mémoire pour un entier
    int *p = (int *)malloc(sizeof(int));
    char *str = (char *)malloc(20* sizeof(char));
    if (p == NULL || str == NULL) {
        printf("Erreur d'allocation de mémoire\n");
        return 1;
    }

    *p = 42; // Utilisation de la mémoire allouée
    strcpy(str, "Bonjour, C!");

    printf("Valeur de *p : %d\n", *p);
    printf("Valeur de *str : %s\n", str);

    free(p); // Libération de la mémoire
    free(str);
    return 0;
}
