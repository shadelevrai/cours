#include <stdio.h>
#include <stdlib.h>

int main() {
    // Demander à l'utilisateur le nombre d'éléments du tableau
    int n = 5;

    // Allouer de la mémoire pour n entiers
    int *array = (int *)malloc(n * sizeof(int));
    if (array == NULL) {
        printf("Erreur d'allocation de mémoire\n");
        return 1;
    }

    // Initialiser les éléments du tableau
    for (int i = 0; i < n; i++) {
        array[i] = i * 2; // Remplissage du tableau avec des valeurs
    }

    // Afficher les éléments du tableau
    for (int i = 0; i < n; i++) {
        printf("array[%d] = %d\n", i, array[i]);
    }

    // Libérer la mémoire
    free(array);
    return 0;
}
