/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/08/29 09:54:05 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		i++;
	}
	return (i);
}

/*#include <stdio.h>
#include <string.h>
int main(int argc, char *argv[])
{
    if(argc>0){
        printf("%d\n", ft_strlen(argv[1]));
        printf("%lu", strlen(argv[1]));
    }
}*/
