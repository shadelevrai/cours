/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

void	ft_putnbr(int nb)
{
	char	c;

	if (nb > 9)
		ft_putnbr(nb / 10);
	c = (nb % 10) + '0';
	write(1, &c, 1);
}

void	find_first_and_last_number(char *str, int *first, int *last)
{
	int	i;

	i = 0;
	*first = -1;
	*last = -1;
	while (str[i])
	{
		if (str[i] >= '0' && str[i] <= '9')
		{
			if (*first == -1)
				*first = i;
			*last = i + 1;
		}
		i++;
	}
}

int	find_minus_and_plus(char *str, int first_number)
{
	int	i;
	int	position_first_minus_or_plus;
	int	stop;

	i = first_number - 1;
	position_first_minus_or_plus = 0;
	stop = 1;
	while (i != -1)
	{
		if ((str[i] == '-' || str[i] == '+') && stop)
		{
			position_first_minus_or_plus = i;
		}
		else
		{
			stop = 0;
		}
		i--;
	}
	return (position_first_minus_or_plus);
}

char	minus_or_plus(char *str, int position_first_n, int position_first_pn)
{
	int	i;
	int	only_positive;
	int	number_of_minus;

	i = position_first_pn;
	only_positive = 0;
	number_of_minus = 0;
	while (i < position_first_n)
	{
		if (str[i] != '+')
			only_positive = 0;
		if (str[i] == '-')
			number_of_minus++;
		i++;
	}
	if (only_positive == 1)
		return ('+');
	if (number_of_minus % 2 != 0)
		return ('-');
	return ('+');
}

int	ft_atoi(char *str)
{
	int		first;
	int		last;
	char	m_or_p;

	find_first_and_last_number(str, &first, &last);
	m_or_p = minus_or_plus(str, first, find_minus_and_plus(str, first));
	if (m_or_p == '-')
		write(1, &m_or_p, 1);
	while (first < last)
		write(1, &str[first++], 1);
	return (0);
}

/*int	main(int argc, char *argv[])
{
	if (argc > 0)
		ft_atoi(argv[1]);
	return (0);
}*/
