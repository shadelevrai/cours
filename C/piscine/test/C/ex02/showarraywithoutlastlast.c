#include <unistd.h>

void displayarraywithoutlastlast(char *str)
{
    int l = 0;
    int i = 0;
    while (str[l] != '\0')
    {
        l++;
    }

    while (str[i] != '\0')
    {
        if (i != (l - 2))
        {
            write(1, &str[i], 1);
        }
        i++;
    }
}

int main()
{
    char letter[8] = "bonjour";
    displayarraywithoutlastlast(letter);
}