#include <unistd.h>
#include <stdio.h>

void nombreint(int a, int *b, int *c)
{
	int d = *b;

	printf("%d a", a); // affiche 50
	printf("%s", " ");
	printf("%p b", b); // affiche une adresse mémoire (500x7ffce...)
	printf("%s", " ");
	printf("%d d", d); // vaut 50 car on a déférencé b (qui est un pointer depuis sa création)
	printf("%s", " ");
	printf("%p c", c);
}

int main()
{
	//afficher un simple nombre avec write
	char nombre = '5';
	write(1, &nombre, 1);
	write(1, "\n", 1);

	//afficher une simple chaine de character avec write (avec limitation octet)
	char chaine[] = "mamaaaa";
	write(1, &chaine, 4);
	write(1, "\n", 1);

	//afficher 
	char nb1 = nombre;
	char *nb2 = &nombre;
	write(1, &nb1, 1);
	write(1, "\n", 1);
	write(1, nb2, 1);
	write(1, "\n", 1);


	char chaine2[] = "Bonjour";
	write(1, chaine2, 7);
	write(1, &"123456789", 1);

	int a = 50;
	int *b = &a;
	int c =12;
	int d = *b;

	printf("%d d",d);

	nombreint(a, b, &c);

	return 0;
}
