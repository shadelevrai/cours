#include <stdio.h>
#include <unistd.h>

void ft_putchar(char c)
{
    write(1, &c, 1);
}

void ft_putnbr(int nb)
{
    if (nb == -2147483648)
    {
        write(1, "-2147483648", 11);
    }
    if (nb < 0)
    {
        write(1, "-", 1);
        nb = -nb;
    }
    if (nb > 9)
    {
        ft_putnbr(nb / 10);
    }
    ft_putchar((nb % 10) + '0');
}

int leplusgrand(int *tab, unsigned int len)
{
    int i = 0;
    int j = 0;
    int temp;
    while (i < len)
    {

        while (j < len)
        {

            if (tab[i] < tab[j])
            {
                temp = tab[i];
                tab[i] = tab[j];
                tab[j] = temp;
            }
            j++;
        }
        //ft_putnbr(tab[i]);
        j = 0;
        i++;
    }
    return (tab[len-1]);
}

int main(int ac, char *av[])
{
    int tab[7] = {1, 5, 3, 1,10,55,-88};
    int lala = leplusgrand(tab, 7);
    printf("%d\n", lala);
    (void)ac;
    return (0);
}