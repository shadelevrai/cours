#include <stdio.h>

void replace_char(char *str, char find, char replace) {
    while (*str != '\0') {
        if (*str == find) {
            *str = replace;
        }
        str++;
    }
}

int main() {
    char text[] = "Hello World";
    replace_char(text, 'o', 'a');
    printf("Modified Text: %s\n", text);
    return 0;
}
