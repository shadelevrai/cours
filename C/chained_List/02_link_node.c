#include <stdlib.h>
#include <stdio.h>

// Définition de la structure
typedef struct s_list {
    void *content;
    struct s_list *next;
} t_list;

// Fonction pour créer un nouveau nœud
t_list *ft_lstnew(void *content) {
    t_list *new_node = malloc(sizeof(t_list));
    if (!new_node)
        return (NULL);
    new_node->content = content;
    new_node->next = NULL;
    return (new_node);
}

// Fonction pour créer une liste de 3 nœuds
t_list *create_list() {
    // Étape 1 : Créer les valeurs à stocker
    int val1 = 10, val2 = 20, val3 = 30;

    // Étape 2 : Créer les 3 nœuds
    t_list *node1 = ft_lstnew(&val1);
    t_list *node2 = ft_lstnew(&val2);
    t_list *node3 = ft_lstnew(&val3);

    // Étape 3 : Connecter les nœuds entre eux
    node1->next = node2; // Relier node1 à node2
    node2->next = node3; // Relier node2 à node3

    // Étape 4 : Retourner le premier nœud
    return node1;
}

int main() {
    // Créer une liste
    t_list *list = create_list();

    // Parcourir la liste et afficher son contenu
    t_list *current = list;
    while (current) {
        printf("Contenu : %d\n", *(int *)current->content);
        current = current->next; // Passer au nœud suivant
    }

    // Libérer la mémoire (facultatif ici, mais bon à pratiquer)
    return 0;
}
