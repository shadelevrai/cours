#include <stdlib.h>
#include <stdio.h>

// Définition de la structure de la liste chaînée
typedef struct s_list {
    void *content;
    struct s_list *next;
} t_list;

// Fonction pour créer un nouveau nœud
t_list *ft_lstnew(void *data) {
    // Étape 1 : Allouer un espace mémoire pour le nœud
    t_list *new_node = malloc(sizeof(t_list));
    if (!new_node)
        return (NULL); // Si malloc échoue, on retourne NULL

    // Étape 2 : Initialiser les champs
    new_node->content = data; // On assigne le contenu passé en paramètre
    new_node->next = NULL;       // Le champ 'next' pointe vers NULL car c'est un nœud isolé

    return (new_node); // Retourne le pointeur vers le nouveau nœud
}

int main() {
    // Exemple de contenu à stocker dans un nœud
    int data = 42;

    // Étape 1 : Créer un nœud avec ft_lstnew
    t_list *node = ft_lstnew(&data);

    // Étape 2 : Vérifier et afficher le contenu du nœud
    if (node) {
        printf("Contenu du nœud : %d\n", *(int *)node->content); // On récupère la valeur pointée
        free(node); // Libère la mémoire allouée pour éviter une fuite
    } else {
        printf("Échec de la création du nœud.\n");
    }

    int number = 5;
    int *p = &number;
    printf("%d\n",*(int *)p);
    return 0;
}
