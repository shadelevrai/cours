import react, { Component } from 'react'
import AgeContextProvider from '../contexts/AgeContext';

const App = () => {
  return (
    <AgeContextProvider />
  );
}

export default App;