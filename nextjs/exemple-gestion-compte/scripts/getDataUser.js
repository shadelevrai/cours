import fetch from 'isomorphic-unfetch'

const UserData = async function (token) {
    const res = await fetch(`${process.env.API}/usersjennyfers/current`, {
        method: 'get',
        headers: {
            'Authorization': `Token ${token}`,
            'Content-type': 'application/json'
        }
    })
    return await res.json()
}

export default UserData