import cookies from 'next-cookies'

const token = function (ctx) {
    const token = cookies(ctx.ctx).Token
    return token
}
export default token