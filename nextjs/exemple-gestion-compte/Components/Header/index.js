import React, { useContext } from 'react'

import UserContext from '../../useContext/UserContext'

function Header() {

    const dataContext = useContext(UserContext);

    return (
        <div>hello {dataContext.data ? dataContext.data.user.email : 'Guest'}</div>
    )
}

export default Header