import Link from 'next/link'
import { useRouter } from 'next/router'

import Header from '../Components/Header'

function Index(pageProps) {
    const router = useRouter()

    function disconnect() {
        document.cookie = `Token=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT`
        router.push('/')
    }

    return (
        <>
            <Header user={pageProps.data} />
            <Link href="signup">
                <a>S'enregistrer</a>
            </Link>
            <br />
            <Link href='login'>
                <a>Se connecter</a>
            </Link>
            <br />
            <button onClick={disconnect}>Se deconnecter</button>
            <br />
            <br />
            <Link href='profil'>
                <a>Profile</a>
            </Link>
        </>
    )
}


export default Index