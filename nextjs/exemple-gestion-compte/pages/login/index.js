import { useRouter } from 'next/router'
import Link from 'next/link'

function Login() {
    const router = useRouter()
    let emailLoginSuperAdmin
    let passwordLoginSuperAdmin

    function handleMailLoginSuperAdmin(e) {
        emailLoginSuperAdmin = e.target.value
    }

    function handlePasswordLoginSuperAdmin(e) {
        passwordLoginSuperAdmin = e.target.value
    }

    async function postLoginSuperAdmin(e) {
        e.preventDefault();
        const data = {
            user: {
                email: emailLoginSuperAdmin,
                password: passwordLoginSuperAdmin
            }
        }
        const response = await fetch('http://localhost:8080/api/usersjennyfers/login', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        if (response.status === 200) {
            const content = await response.json();
            document.cookie = `Token=${content.user.token}; path=/`
            router.push('/')
        } else {
            console.log('erreur')
        }
    }


    return (
        <div>
            <form onSubmit={postLoginSuperAdmin}>
                email <input type="text" onChange={handleMailLoginSuperAdmin} />
                mdp <input type="password" onChange={handlePasswordLoginSuperAdmin} />
                <button type="submit">valider</button>
            </form>
            <Link href='/'>
                <a>Retour</a>
            </Link>
        </div>
    )
}

export default Login