import UserContext from '../useContext/UserContext'
import '../styles/index.css'

import Token from '../scripts/token'
import UserData from '../scripts/getDataUser'

function MyApp({ Component, pageProps }) {
    return (
        <UserContext.Provider value={pageProps}>
            <Component {...pageProps} />
        </UserContext.Provider>
    )
}

// MyApp.getInitialProps = async ctx => {
//     let pageProps = {
//         connected: false,
//         data: null
//     }

//     Token(ctx) !== undefined &&
//         (pageProps.data = await UserData(Token(ctx)), pageProps.connected = true)

//     return { pageProps }
// }
export default MyApp