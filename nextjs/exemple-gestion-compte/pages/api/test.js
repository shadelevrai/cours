let user = null


export default (req, res) => {
  if (req.method === 'GET') {
    res.status(200).json({
      user: user
    });
  } 

  if(req.method === 'POST') {
    user = req.body
    user !== null ?
      res.status(200).json({
        user: user
      }) 
      :
      res.status(501).json({
        user: 'error'
      })
  }
};