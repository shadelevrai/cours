
import Link from "next/link";


function Profil(pageProps) {
    return (
        <div>
            <p>profile</p>
            <h1>Votre mail : {pageProps.data && pageProps.data.user.email}</h1>
            <Link href='/'>
                <a>Retour</a>
            </Link>
        </div>
    )
}

export default Profil