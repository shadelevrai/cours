import React,{useState, useEffect} from 'react'

export const testContext = React.createContext()

const TestProvider = ({children}) => {

    const [test,setTest] = useState('light')
    
    useEffect(()=>{
        console.log('ça a changé')
    },[test])
 
    
    return(
        <testContext.Provider value={{test,setTest}}>
            {children}
        </testContext.Provider>
    )
}

export default TestProvider