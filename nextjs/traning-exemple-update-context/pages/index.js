import React, { useContext } from 'react'
import { testContext } from '../providers/TestProvider'

export default function Index() {
    const { test, setTest } = useContext(testContext)
    console.log(test);

    return (
        <div>
            <button onClick={e=>setTest('paslight')}>setTest</button>
        </div>
    )
}