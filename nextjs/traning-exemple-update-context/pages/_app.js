import TestProvider from '../providers/TestProvider'

function MyApp({Component}) {
    return (
        <TestProvider>
            <Component/>
        </TestProvider>
    )
}

MyApp.getInitilaProps = () => {
    console.log('hello');
    
    return {}
}

export default MyApp