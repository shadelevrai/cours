import fetch from 'isomorphic-unfetch'

export default function parametreFunctionWithBrace({stars}){
    return(
    <div>next stars {stars}</div>
    )
}

parametreFunctionWithBrace.getInitialProps = async () => {
    const res = await fetch('https://api.github.com/repos/zeit/next.js')
    const json = await res.json()
    console.log(json.stargazers_count)
    return { stars: json.stargazers_count }
  }