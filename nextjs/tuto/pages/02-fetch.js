import { useEffect } from "react"

function FetchTuto({stars}){

    useEffect(()=>{
        fetch('https://api.github.com/repos/zeit/next.js')
        .then(res=>res.json())
        .then(data=>console.log('voici les data du fetch coté client : ', data))
    },[])
    
    return (
        <div>
            <div>{stars}</div>
        </div>
    )
}

FetchTuto.getInitialProps = async () =>{
    const res = await fetch('https://api.github.com/repos/zeit/next.js')
    const json = await res.json()
    console.log('voici les data coté serveur : ', json)
    return { stars: json.stargazers_count }
}

export default FetchTuto