

// getInitialProps est une émthode statique.
// Une méthode statique est une méthode qui se lance qu'une seule fois
// Le getInitial se lance coté serveur

export default function GetinitialPropsTuto({stars}){
    console.log('je suis coté serveur et client')
    return(
        <div>{stars}</div>
    )
}

// Ce code se lance coté serveur
// le getInitialProps peut prendre en parametre un objet de contexte, qui va contenir des infos de routage
//Un object context est un objet qui sera différent par rapport à l'endroit où il est appelé
// getInitialProps doit toujours retourner un objet
GetinitialPropsTuto.getInitialProps = async (req) => {
    // const res = await fetch('https://api.github.com/repos/zeit/next.js')
    // const json = await res.json()
    console.log('je suis dans le getinitial, donc je me lance coté serveur')
    // console.log('voici le req : ', req)
    // return { stars: json.stargazers_count }
    console.log('voici le req',req)
    return {stars: "momo"}
  
  }