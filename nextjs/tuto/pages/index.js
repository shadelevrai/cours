import React from 'react'
import ShopApi from 'commercecloud-ocapi-client'

const config = {
    basePath: 'https://dev05-store-jennyfer.demandware.net/s/Jennyfer-EU/dw/shop/v16_1',
    defaultHeaders: {
        'x-dw-client-id': '54d01e39-c520-41b8-a14e-d2a052e245fd'
    }, // HTTP header for all requests
    timeout: 60000, // Request timeout in milliseconds
    cache: true, // If set to false an additional timestamp parameter is added to all API GET calls to prevent browser caching
    enableCookies: false, //If set to true, the client will save the cookies from each server response, and return them in the next request.
    overrideHttpPut: true // If set to true, any methods specified as using http PUT will be sent using POST along the header value 'x-dw-http-method-override' set to 'PUT'.
  }

ShopApi.ApiClient.instance = new ShopApi.ApiClient(config)

const api = new ShopApi.CategoriesApi()

export default function Index() {

    api.getCategoriesByIDs('Collection')
    .then()
    .catch((fault) => {
        console.error(fault)
    })

    return (
      <div>
        <p>Hello Next.js</p>
      </div>
    );
  }
  