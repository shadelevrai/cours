//To start, create a sample project by running the following commands:

npm init -y
npm install --save react react-dom next
mkdir pages

// Then open the package.json file in the hello-next directory and replace scripts with the following:

"scripts": {
    "dev": "next",
    "build": "next build",
    "start": "next start"
  }
  

//   Now everything is ready. Run the following command to start the dev server:

npm run dev