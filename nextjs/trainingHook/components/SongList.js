import React, { useState, useEffect } from 'react'
import NewSongForm from './NewSongForm'

const SongList = () => {
  // Déclaration de la state facon Hook
  const [songs, setSongs] = useState([
    { title: 'almost home', id: 1 },
    { title: 'memory gospel', id: 2 },
    { title: 'this wild darkness', id: 3 },
  ])
  const [age, setAge ] = useState(20)

  let i = 4

  const addSong = (title) => {
    setSongs([...songs, { title, id: i }])
  }

  // Permet d'utiliser useEFfect sur certains composant seulement et se charge 1 fois que le composant est chargé et une autre quand la state change
  useEffect(() => {
    console.log('useEffect hook ran', songs)
  }, [songs])

  useEffect(() => {
    console.log('useEffect hook ran', age)
  }, [age])

  return (
    <div className="song-list">
      <ul>
        {songs.map(song => {
          return (<li key={i++}>{song.title}</li>)
        })}
      </ul>
      <NewSongForm addSong={addSong} />
      <button onClick={() => setAge(age + 1)}>Add 1 to age: {age}</button>
    </div>
  );
}

export default SongList