angular
    .module("app",[])
    .controller("myController", ($scope)=>{
        $scope.array = [
            {
                name:'Web dev',
                price:300,
                validate:false
            },
            {
                name:'Design',
                price:400,
                validate:false
            },
            {
                name:'Integration',
                price:250,
                validate:false
            },
            {
                name:'Training',
                price:220,
                validate:false
            }
        ]
        $scope.toggleActive = (p1) => {
            p1.validate = !p1.validate;
            console.log(p1);
        }

        $scope.total = ()=>{
            var total = 0;
            angular.forEach($scope.array,(p1) => {
                if(p1.validate){
                    total+=p1.price;
                }
            });
            return total;
        }

    })