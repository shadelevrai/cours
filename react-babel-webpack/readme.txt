1/ Il faut installer nodemon et babel-node en -g

2/ Dans le dossier racine, faire npm init pour créer un fichier package

3/ Toujours dans le fichier racine, créer un fichier .babelrc
C'est le fichier babel de configuration

4/ Dans le fichier .babelrc, il faut mettre {"presets": ["env", "react","stage-0"]}

5/ Installer npm install babel-preset-env --save-dev

6/ Creer un fichier serveur avec ce code :
import express from 'express'; 
const app = express();
app.get('/', (req, res) => {
    res.send('Hello World')
})
app.listen(4000, () => {
  console.log('Listening');
});

7/ Pour voir si tout fonctionne bien, lancer nodemon --exec babel-node server.js

8/ Dans le dossier racine, créer un dossier client

9/ DAns le dossier client, créer un fichier index.html et index.js

10/ Installer react et react-dom
et npm i --save-dev babel-preset-react

11/ Dans le dossier racine, créer un fichier webpack.config.js et y mettre ça :
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import LiveReloadPlugin from 'webpack-livereload-plugin'

export default  {
  mode: 'none', // | 'development' | 'none'
  entry: './client/index.js',
  output: {
    path: '/',
    filename: 'bundle.js'
  },
  module: {
    rules: [{
        use: 'babel-loader',
        test: /\.js$/,
        exclude: /node_modules/
      },
      {
        use: ['style-loader', 'css-loader'],
        test: /\.css$/
      }]
  },
  plugins: [ new HtmlWebpackPlugin({
      template: 'client/index.html'
    }),
    new LiveReloadPlugin()]
};

12/ Installer npm install --save-dev webpack html-webpack-plugin webpack-livereload-plugin 

13/ Installer npm i --save-dev babel-loader style-loader css-loader babel-core

14/ Dans le fichier serveur, mettre :
import express from 'express'; 
import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackConfig from './webpack.config.js';

const app = express();
app.use(webpackMiddleware(webpack(webpackConfig)));

app.get('/api', (req, res) =>  )

app.listen(4000, () => {
  console.log('Listening');
});

15/ Installer npm i --save-dev webpack-dev-middleware babel-preset-stage-0

16/ Dans le fichier package.json, mettre dans l'objet script : "start": "nodemon --exec babel-node server.js  --ignore client"

17/ Faire npm start pour lancer le serveur