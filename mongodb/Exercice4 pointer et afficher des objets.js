//Dans la collection piscines de la base paris, trouver :

//les piscines dont le code postal est supérieur ou égal à 75013 triés par code postal descendant
db.piscines.find({
    zipCode: {
        $gte: 75013
    }
}).sort({
    zipCode: -1
});

// Les piscines situées à l'ouest de Notre Dame de Paris
db.piscines.find({
    lon: {
        $lt: 2.349
    }
}).sort({
    zipCode: -1
});

// Et leur nombre 
db.piscines.find({
    lon: {
        $lt: 2.349
    }
}).count();


// Les piscines dont zipCode=75013 ET id=2929 avec l'opérateur $and et $eq
db.piscines.find({
    $and:[{
        zipCode:{
            $eq:75013
        }
    },{
             id:{
                 $eq:2929
             }
         }]
})
// On peut simplifier - uniquement l'opérateur $and

db.piscines.find({
    $and:[
        {
            zipCode:75013
        },{
            id:2929
        }
    ]
})

// Version la plus courte
db.piscines.find({
    zipCode:75013,id:2929
})