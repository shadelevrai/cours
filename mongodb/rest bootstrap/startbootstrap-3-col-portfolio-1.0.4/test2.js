$(function(){
 $.ajax({
     url:"http://127.0.0.1:28017/newyork/restaurants/?limit=9", // affiche la base de donnée des restau mais se limte à 9.
    dataType:'json',
 }).done(function(data){
     console.log(data);
     data.rows.forEach(function(row){ //'data' est la base de donnée. 'rows' contient les objets qui nous intéresse (voir dans le console.log) et forEach est une fonction qui parcourt les tableau ou les objets. On lui donne 'row' en paramètre.
         var rand = Math.floor(Math.random()*10)+1; // pour générer un chiffre entre 1 et 10.
        var item = '<div class="col-md-4 portfolio-item"><a href="#"><img class="img-responsive" src="http://lorempixel.com/700/400/food/'+rand+'" alt=""></a><h3><a href="#">'+row.name+'</a></h3><p>'+row.cuisine+'</p><p>'+row.borough+'</p><a href="restaurant.html?restaurant_id='+row.name+'">lire la suite</a></div>'; //on créer une variable et on lui met tout dedans avec en prime les données de la base de donnée. On lui rajoute aussi un texte 'lire la suite' qui aura un lien href qui sera dans la base de donnée.
         $('.restaurant').append(item); //dans la div 'restaurant', on met tout ce qu'il y a dans la variable 'item'
     })
 });
    
    
});




/*

$('.maClasse').css({
    backgroundColor : 'grey',
    width : '500px',
    height :'200px',
})

$.ajax({
    url:"http://127.0.0.1:28017/newyork/restaurants/?limit=2",
    dataType:'json',
}).done(function(data){ //une fois les données récupérées
    console.log(data); //on récupère un objet json
    //on va parcourir chacunes de nos lignes
    data.rows.forEach(function(row){ //on ajoute à notre contenu
        $('.maClasse').append('<h2>' +row.name'</h2>');
    })
});

*/