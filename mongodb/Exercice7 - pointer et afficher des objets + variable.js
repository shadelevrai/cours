// Créer une base de données newyork et une collection restaurants
exit
(mettre d'abord le fichier json dans le dossier ou il y a mongo import')


// Importer le fichier restaurant.json
mongoimport --db newyork --collection restaurants --file restaurants.json

// Combien y a-t-il de restaurants ?
25359
// Identique à 


// Trouver les restaurants qui sont dans la rue "Morris Park Ave"
db.restaurants.find({"address.street":"Morris Park Ave"});
// Combien y en-a-t-il ?
9

//pour aussi récupérer ceux avec "avenue"
db.restaurants.find({"address.street":/Morris Park Ave/});
// Afficher uniquement (sans l'id) les champs quartier, type de cuisine et adresse
db.restaurants.find({"address.street":"Morris Park Ave"},{cuisisne:1,borough:1, address:1, _id:0}).pretty()

// Trouver la liste des restaurants situés à Staten Island qui font des hamburgers OU de la boulangerie.
// Avec un $or
db.restaurants.find({"borough":"Staten Island", $or:[{cuisine : "Hamburgers"}, {cuisine : "Bakery"}]}).pretty();
// Avec un $in
db.restaurants.find({"borough":"Staten Island",cuisine:{$in:["Hamburgers","Bakery"]}}).pretty()

// Quel est le type de restaurant le plus présent ?
//var restos = db.restaurants.find();
var restos = db.restaurants.find()

restos // affiche ce qu'il y a dans la variable. Les variables ont une durée de vie de 10 min d'inactivité

restos.count() // voir combien il y a de resto

// On voit que c'est très long ! mais c'est possible...

//pour parcourir un curseur
while(restos.hasNext()){
    printjson(restos.next())
}

//pour parcourir avec un foreach
var restos = db.restaurants.find();
var typeCuisine =[]; // tableau pour stocker les différents types de cuisine

restos.forEach(
function(resto){
    //printjson(resto);
    if(typeCuisine.indexOf(resto.cuisine) > 1){
        //présent dans le tableau
    } else{
        //non présent dans le tableau
        tyoeCuisine.push(resto.cuisine)//on ajoute alors...
    }
})
printjson(typeCuisine)
//je parcours mon tableau des typeCuisine et je fais un count sur ce type de cuisine
var max = 0;
var countType = 0;
var indexCuisine = 0;
for(var i=0; i<typeCuisine.length; i++){
    countType = db.restaurants.find({cuisine:typeCuisine[i]}).count();
    if(countType > max){
        max = countType;
        indexCuisine = i;
    }
}
max // Nous donne le nombre de restaurants de la catégorie la plus représentée
typeCuisine[indexCuisine]; // le nom de la catégorie la plus représentée

//autre méthode



// La méthode aggregate de mongoDB fait la même chose de manière plus puissante
// db.collection.aggregate(query, options)


// Faire la même requête pour le quatier du Bronx

// En limitant le nombre de retours à 5
