// Aggrégation

// Importer dans une base le fichier website.json


// Quel est l'hébergeur qui héberge le plus de sites ?

db.website.aggregate([{
    $match: {}
}, {
    $group: {
        _id: "$hebergeur",
        total: {
            $sum: 1
        }
    }
}, {
    $sort: {
        total: -1
    }
}])

// Pour l'hébergeur gandi, quel site a le plus de traffic
db.website.find({
    hebergeur: "Gandi"
}, {
    _id: 0,
    traffic: 1,
    url: 1
}).sort({
    traffic: -1
})

// Quel est le traffic cumulé des hébergeurs ? Qui est le premier ?
db.website.aggregate([{
    $match: {}
}, {
    $group: {
        _id: "$herbergeur",
        total: {
            $sum: "$traffic"
        }
    }
}, {
    $sort: {
        total: -1
    }
}, {
    $limit: 1
}])


// Quelle est la moyenne des likes par hébergeur ?
db.website.find().forEach(function (site) {
    db.website.update({
        _id: site._id
    }, {
        $set: {
            likes: parseInt(site.likes)
        }
    })
})

db.website.aggregate([{
        $match: {}
    }, {
        $group: {
            _id: "$hebergeur",
            moyenne: {
                $avg: "$likes"
            }
        }
    }, {
        $sort: {
            moyenne: -1
        }
    }, {
        $limit: 3
    }])
    // Augmenter le nombre de 50 likes les sites de Gandi 


// exporter dans un fichier newwebsite.json le contenu de notre collection