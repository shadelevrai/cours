// Vélib


// Récupérer un fichier json des velib chez jcdecaux developer
// Importer dans la base paris, le fichier jcdecaux.json dans une collection velib
mongoimport --db paris --collection velib --file Paris.json --jsonArray
// Problème ! On n'a pas de champ codepostal ... On retrouve le code postal dans l'adresse.

// Mettez à jour tous les enregistrements en leur ajoutant un champ zipCode
var stations = db.velib.find()

stations.forEach(
  function(station){
    cp = station.address.match(/[0-9]{5}/);
    //print(cp); // Juste pour vérifier que l'on est capable de bien récupérer le code postal
    db.velib.update(
      {_id:station._id},
      {$set:{zipCode:parseInt(cp)}}
    );
  }
)

// Quel est l'arrondissement de Paris ou il y a le plus de stations ? (avec un $in)      
db.velib.aggregate([
  {$match:{zipCode:{$in:[75001,75002,75003,75004,75005,75006,75007,75008,75008,75009,75010,75011,75012,75013,75014,75015,75016,75017,75018,75019,75020]}}},
  {$group:{_id:"$zipCode", nbStations: {$sum:1}}},
  {$sort:{nbStations:-1}}
])



// OU plus élégant avec un <
db.velib.aggregate([
  {$match:{zipCode:{$lt:75021}}},
  {$group:{_id:"$zipCode", nbStations: {$sum:1}}},
  {$sort:{nbStations:-1}}
])

// Quelle est la ville (hors Paris) qui a le plus de stations
db.velib.aggregate([
  {$match:{zipCode:{$nin:[75001,75002,75003,75004,75005,75006,75007,75008,75008,75009,75010,75011,75012,75013,75014,75015,75016,75017,75018,75019,75020]}}},
  {$group:{_id:"$zipCode", nbStations: {$sum:1}}},
  {$sort:{nbStations:-1}}
])
// OU plus élégant
db.velib.aggregate([
  {$match:{zipCode:{$gt:75021}}},
  {$group:{_id:"$zipCode", nbStations: {$sum:1}}},
  {$sort:{nbStations:-1}}
])
// Cherchez la piscine Dunois .
var dunois = db.piscines.find({name:/Dunois/})


// Je stocke les coordoonnées de la piscine
var latitude = 48.832973 ;
var longitude = 2.366437 ;

// On déclare une fonction de calcul de distance
//lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees) 
//lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
//unit = the unit you desire for results 
// 'M' is statute miles
// 'K' is kilometers 
// 'N' is nautical miles  

function distance(lat1, lon1, lat2, lon2, unit) {
	var radlat1 = Math.PI * lat1/180
	var radlat2 = Math.PI * lat2/180
	var theta = lon1-lon2
	var radtheta = Math.PI * theta/180
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist)
	dist = dist * 180/Math.PI
	dist = dist * 60 * 1.1515
	if (unit=="K") { dist = dist * 1.609344 }
  if (unit=="meters") { dist = dist * 1.609344 *1000 }
	if (unit=="N") { dist = dist * 0.8684 }
	return dist
}

newlat = 48.83;
newlon = 2.36;

distance(latitude, longitude, newlat, newlon, 'meters')

// Quelles sont les 5 stations les plus proches ?
var lesstations = db.velib.find();

lesstations.forEach(function(unestation){
  db.velib.update(
    {_id:unestation._id},
      {
        $set:{distance: distance(latitude, longitude, unestation.latitude, unestation.longitude, 'meters')}
      }
  )

})

db.velib.find().sort({distance:1}).limit(5)
