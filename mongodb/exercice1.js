// Affiche la liste des bdds
show dbs;

// Sélectionne une base 
use videoclub

// Affiche la liste des collections
show collections;


// Affiche la base de données courante
db 

// Insérer un films
db.films.insert({
  titre: "Matrix",
  duree: 120
})

// Récupérer les enregistrements
db.films.find()

// Récupérer un seul enregistrements
db.films.findOne()

// Nous affiche 'proprement' les documents sélectionnés
db.films.find().pretty()

// Insertion de 
db.films.insert({
  titre : "La vache et le prisonnier",
  duree : 119,
  realisateur:{
    nom:"Verneuil",
    prenom:"Henri"
  },
  acteurs:[
    {
      nom: "Fernandel"
    },
    {
      prenom: "René",
      nom : "Havard"
    }
    ],
  anneesortie : 1959
})


// Dans la base videoclub
// Insérer des enregistrements de films

titre : La vache et le prisonnier
acteurs : Fernandel, René Havard
realisateur : Henri Verneuil
duree : 120

titre: Kung Fury
acteur: David Sandberg
realisateur : David Sandberg
duree:30
synopsis : "Le flic king du KungFu qui doit se débarrasser d'Hitler" 

// Requete d'insertion
db.films.insert({
  titre: "Kung Fury",
  acteur : {
    nom: "SandBerg",
    prenom: "David"
  },
  realisateur : {
    nom: "SandBerg",
    prenom: "David"
  },
  synopsis:"Le flic king du KungFu qui doit se débarrasser d'Hitler"
})

// On peut utiliser les fonctions javascript
db.films.insert({
  nom : "La soupe aux choux",
  created : new Date(),  
})

// En passant par une variable
var maDate = new Date();
db.films.insert({
  titre: "L'aile ou la cuisse",
  created : maDate
})

// Ajouter des clients
avec un nom, un prenom, un sous document 'naissance' qui contient la date de naissance (format date) la ville de naissance et le departement de naissance.
un champ 'date de création = created' qui sera un timestamp

Jean
Bon
naissance : date:15 juillet 1970
            ville: bayonne
created

Liliane
DioSoleil
naissance : date : 10 septembre 1958
            ville : Orange
            departement : Vienne
created

db.clients.insert({
  prenom: "Jean",
  nom : "Bon",
  naissance:{
    date: new Date(1970,6,15),
    ville: "Bayonne"
  },
  created : new Timestamp()
})

db.clients.insert({
  prenom: "Liliane",
  nom : "Diosoleil",
  naissance:{
    date: new Date(1958,8,10),
    ville: "Orange",
    departement: "Vienne"
  },
  created : new Timestamp()
})











