Retrouver les 5 premières piscines par ordre alphabétique ( et dont le champ zipCode existe)

// J'ai créé 2 piscines avec un champ nom au lieu de name
// Si je compte mes piscines, j'en ai donc 33
db.piscines.count()
// Mais seulement 31 avec le champ name
db.piscines.find({name:{$exists:true}}).count()
// équivalent à
db.piscines.count({name:{$exists:true}})
// Renvoie toutes les piscines ayant effectivement le champ nom
db.piscines.find({name:{$exists:true}})
// Limite à 5 résultats
db.piscines.find({name:{$exists:true}}).limit(5)

// En les triant par ordre alphabétique (case sensitive)
db.piscines.find({name:{$exists:true}}).limit(5).sort({name:1})

// En plus en limitant les champs retournés au nom
db.piscines.find({name:{$exists:true}},{name:1}).limit(5).sort({name:1})