// Revenez sur la base "videoclub"

// Trouver un film dont le nom contient 'vache' (en utilisant une expression régulière)

db.films.find({titre:{$regex:/vache/}})
// équivalent 
db.films.find({titre:/vache/})

// Afficher uniquement le prenom des acteurs de ce film
db.films.find({titre:/vache/},{"acteurs.prenom":1})

// Trouver les films dont un acteur s'appelle René
db.films.find({"acteurs.prenom":"René"})

