//Dans la collection piscines de la base paris, trouver :

//les piscines qui sont dans le 13ème arrondissement


// est équivalent à 


//les piscines qui ne sont pas le 13ème arrondissement
db.piscines.find({zipCode:{$ne:75013}});

// En affichant uniquement le nom
db.piscines.find({zipCode:{$ne:75013}},{name:1});

//les piscines qui sont dans le 13ème et le 14ème arrondissement
db.piscines.find({zipCode:{$in:[75013,75014]}});

//les piscines qui ne sont pas dans le 15, 16, 17 et 18ème arrondissement
db.piscines.find({zipCode:{$nin:[75015,75016,75017,75018]}}).pretty();
