// Mise à jour

// Dans la liste des restaurants
use newyork
// Modifier les restaurants dont la cuisine est Hamburgers pour leur ajouter un champ healthy_food égal à 2
db.restaurants.update(
  // Requete
  { cuisine: "Hamburgers"},
  // Mise à jour
  {$set:{ healthy_food: 2 }},
  // Options
  { multi:true}
)

// Pour les végétariens, leur mettre le champ healthy food à 9.
db.restaurants.update(
  // Requete
  { cuisine: "Vegetarian"},
  // Mise à jour
  {$set:{ healthy_food: 9 }},
  // Options
  { multi:true}
)

// Pour vérifier ques tous les restaurants ont un tableau grades 
db.restaurants.find({grades:{$exists:false}}).count()

// Supprimer le champ building des restaurants situés dans le Bronx et ajouter un booléen
db.restaurants.update(
  { borough: "Bronx"},
  { 
    $unset :{"address.building":""},
    $set : {maBoule: true}
  },
  { multi: true}
)
//Pour vérifier
db.restaurants.findOne({ borough: "Bronx"})

// Ajouter un champ rating à 5 à tous les restaurants
db.restaurants.update(
  {},
  { 
    $set : {rating: 5}
  },
  { multi: true}
)
// Multiplier le champ rating par 2 pour les restaurants situés dans le Queens
db.restaurants.update(
  { borough:"Queens" },
  { 
    $mul: { rating: 2 }
  },
  { multi: true}
)
// Trouver les restaurants de Brooklyn
db.restaurants.find({borough:"Brooklyn"})

// Limiter les résultats à 100
db.restaurants.find({borough:"Brooklyn"}).limit(100)
// Appliquer d'abord un count()
db.restaurants.find({borough:"Brooklyn"}).limit(100).count()
// Puis à la place appliquer un size()
db.restaurants.find({borough:"Brooklyn"}).limit(100).size()
// Quelle est la différence ?
db.restaurants.find({borough:"Brooklyn"}).count()
db.restaurants.find({borough:"Brooklyn"}).size()

count() ne tient pas compte de la limit le précédant 
size() tient compte de la limite précédante 

// Ajouter une entrée au tableau grades pour le restaurant "Tu-Lu'S Gluten-Free Bakery"
db.restaurants.find({name:"Tu-Lu'S Gluten-Free Bakery"})
db.restaurants.update(
  {name:"Tu-Lu'S Gluten-Free Bakery"},
  { 
    // On fait un push dans le tableau
    $push:{
      grades:{
        date: new Date(),
        grade: "B",
        score: 10
      }
    }
  }
)

// Modifier le champ rating pour tous les documents pour qu'il soit égal à la moyenne réelle des grades

// On crée un curseur
var restos = db.restaurants.find().limit(5);
// On parcourt chaque restaurant
restos.forEach(
  function(resto){
    // Faire la moyenne du restaurant
    var moyenne = 0;
    for(var i=0; i<resto.grades.length; i++){
      moyenne += resto.grades[i].score;
    }
    moyenne = moyenne / resto.grades.length;
    //print(moyenne);
    //resto;
    // Faire l'update du restaurant
    db.restaurants.update(
      { _id: resto._id },
      { $set:{rating : moyenne }}
    );
  }
)


