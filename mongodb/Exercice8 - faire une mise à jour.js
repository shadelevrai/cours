﻿// Exercices de mises à jour

// db.collection.update(query, update, options)

// Reprendre la base paris
use paris
// On ajoute un champ 'acces_handicapé' à true aux piscines du 13ème

db.piscines.update(
  {zipCode:75013},
  {
    $set:{"acces_handicape": true}
  }
)

// Par défaut update() ne modifie que le premier élément qui matche
db.piscines.find({zipCode: 75013}).pretty()

//Il faut ajouter l'option multi:true pour que la mise à jour se fasse pour tous les enregistrements


db.piscines.update(
  {zipCode:75013}, // Requete
  {
    $set:{"acces_handicape": true} // Ajout nouveau champ
  },
  { multi: true} // Options
)
// L'option upsert : true ajoute un document si aucun document ne correspond ou modifie si un document correspond 
db.piscines.update(
  {zipCode:13000}, // Requete
  {
    $set:{"acces_handicape": true} // Ajout nouveau champ
  },
  { multi: true, upsert: true} // Options
)
// On peut définir des champs et en supprimer dans la meme requete
// Ajouter un champ verif true et supprimer l'accès handicapé
db.piscines.update(
  {}, // Requete
  {
    $set:{"verif": true}, // Ajout nouveau champ
    $unset:{"acces_handicape":""}
  },
  { multi: true, upsert: true} // Options
)




