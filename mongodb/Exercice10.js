// Aggrégation

// Importer dans une base le fichier website.json
mongoimport --db websites --collection sites --file website.json

// Quel est l'hébergeur qui héberge le plus de sites ?
db.sites.aggregate([
  { $match: {}},
  { $group: { _id: "$hebergeur", total: { $sum: 1 } } },
  { $sort: { total: -1 } }
])

// Pour l'hébergeur gandi, quel site a le plus de traffic
db.sites.find(
  {hebergeur:"Gandi"}, // Requête
  {_id:0,traffic:1,url:1} // Projection 
).sort({traffic:-1})

// Quel est le traffic cumulé des hébergeurs ? Qui est le premier ?
// Problème ! Le traffic est une chaîne de caractères... On ne peut donc pas les additionner
// 2 solutions :
// - on fait un calcul manuel... C'est chiant
// - on change la nature du champ traffic dans la base avec un update ... C'est moins chiant

db.sites.find().forEach(function(site){
  db.sites.update(
    {_id:site._id},
    {$set: {traffic : parseInt(site.traffic)}}
  )
})

// Ou version plus longue et plus lisible
var messites = db.sites.find();

messites.forEach(
  function(site){
    site.traffic = parseInt(site.traffic);
    db.sites.update(
      {_id:site._id},
      {$set: {traffic : site.traffic}}
    )
  }
)
// Traffic cumulé de tous les hébergeurs
// Traffic cumulé de chaque hébergeur
db.sites.aggregate([
  { $match:{}},
  { $group: { _id: "$hebergeur", total: { $sum: "$traffic" } } },
  { $sort:{total:-1}},
  {$limit:1}
])

// Quelle est la moyenne des likes par hébergeur ?
// les likes sont aussi en string, on les passe en int

db.sites.find().forEach(function(site){
  db.sites.update(
    {_id:site._id},
    {$set: {likes : parseInt(site.likes)}}
  )
})
// On peut maintenant calculer la moyenne
db.sites.aggregate([{ $match:{}},{ $group: { _id: "$hebergeur", moyenne: { $avg: "$likes" } } },{ $sort:{moyenne:-1}},{$limit:3}]);

// Augmenter le nombre de 50 likes les sites de Gandi 
db.sites.update(
  { hebergeur : "Gandi" },
  { $inc : { likes: 50 } },
  { multi : true}
)

// exporter dans un fichier newwebsite.json le contenu de notre collection
mongoexport --db websites --collection sites --out newwebsite.json





