mongoimport --db paris --collection piscines --file piscines_paris.json

db.collection.find(query, projection)

// Pour compter les éléments
db.piscines.find().count();
// ou 
db.piscines.count();

// Pour les piscines du 11ème
db.piscines.find({zipCode:75011})

// Pour les piscines du 11ème, n'affichez que les champs nom et code postal
db.piscines.find({zipCode:75011},{name:1, zipCode:1})

// Par défaut, le champ _id est présent. Il faut l'exclure explicitement
db.piscines.find({},{name:1, zipCode:1, _id:0})

// Pour limiter le nobre de résultats, on utilise 
db.piscines.find({zipCode:75011},{name:1, zipCode:1, _id:0}).limit(7)

// Pour trier par nom
db.piscines.find({zipCode:75011},{name:1, zipCode:1, _id:0}).sort({name:1})


// Quelle est la différence ?
db.piscines.find({zipCode:75011},{name:1, zipCode:1, _id:0}).sort({name:1}).limit(5)

db.piscines.find({zipCode:75011},{name:1, zipCode:1, _id:0}).limit(5).sort({name:1})






