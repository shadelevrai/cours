#!/bin/bash
#  Variables may also have default values. We can define them as such using
#   the following syntax:

     # Assign FOO value 'default' if it's empty
     FOO=${FOO:-'default'}

#   Default values may be useful when you should process the positional
#   parameters, which could be omitted.