#!/bin/bash
#Brace expansions allow us to generate arbitrary strings. It's similar to filename expansion. For example:

echo beg{i,a,u}n # begin began begun

# Also brace expansions may be used for creating ranges, which are iterated over in loops.

echo {0..5} # 0 1 2 3 4 5
echo {00..8..2} # 00 02 04 06 08