#!/bin/bash
# affiche_param.sh

# Les variables parametre peuvent être invoqué en pramatre lors du lancement du script 
#./index.bash un 2 blablabla 4 5 6 7 8 9 10 11 12 13 14 15 16 17

echo "Le 1er paramètre est : $1"
echo "Le 3ème paramètre est : $3"
echo "Le 10ème paramètre est : ${10}"
echo "Le 15ème paramètre est : ${15}"

# $1 fait référencce à la première place dans la ligne pour lancer le script
# $2 fait référencce à la deuxieme place dans la ligne pour lancer le script

echo $0 #correspond au nom du script

#  $0           Script's name.
#  $1 … $9      The parameter list elements from 1 to 9.
#  ${10} … ${N} The parameter list elements from 10 to N.
#  $* or $@     All positional parameters except $0.
#  $#           The number of parameters, not counting $0.
#  $FUNCNAME    The function name (has a value only inside a function).