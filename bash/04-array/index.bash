#!/bin/bash

#Pour déclarer un tableau
fruits=(raisin melon)

#Pour rajouter des éléments au tableau
fruits[2]=Apple
fruits[3]=Pear
fruits[4]=Plum

echo ${fruits[*]} # echo ${fruits[@]} may be used as well

echo ${fruits[*]:1:3} # Affiche les elements entre 1 et 3

echo ${@:1:2} # slice of positional parameters

# Adding elements into an array is quite simple too. Compound assignments are specially useful in this case. We can use them like this:

     fruits=(Orange ${fruits[*]} Banana Cherry)
     echo ${fruits[*]} # Orange Apple Pear Plum Banana Cherry

# In the example above, fruits[*] represents the entire contents of the array and substitutes it into the compound assignment, then assigns the new value into the fruits array, mutating its original value.

#  To delete an element from an array, use the unset command:

     unset fruits[0]
     echo ${fruits[*]} # Apple Pear Plum Banana Cherry