# Il peut exister 3 variable différentes : local, environnement et argument

################### Variable locale 

# Elle n'est accessible que dans le script

#Déclarer une variable
foo="value"

#afficher une variable
echo $foo
echo "my variable is $foo"

#supprimer une variable 
unset foo


####################Variable environnement

#Elle est exportable à l'exterieur

#Déclarer une variable
export myVar='exportValue'

# Il existe des types de variable 

#  $HOME    The current user's home directory.
#  $USER    The current user.
#  $PATH    A colon-separated list of directories in which the shell looks for commands.
#  $PWD     The current working directory.
#  $RANDOM  Random integer between 0 and 32767.
#  $UID     The numeric, real user ID of the current user.
#  $PS1     The primary prompt string.
#  $PS2     The secondary prompt string.

#Toute la liste ici :
#  <http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_03_02.html#sect_03_02_04>

echo $PATH 
echo $USER