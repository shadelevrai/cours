/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	to_hex(unsigned long num, char *buffer, char instruction)
{
	const char	*hex_digits;
	size_t		start;

	if (instruction == 'p' || instruction == 'x')
	{
		hex_digits = "0123456789abcdef";
	}
	if (instruction == 'X')
	{
		hex_digits = "0123456789ABCDEF";
	}
	start = 0;
	while (num != 0)
	{
		buffer[start++] = hex_digits[num % 16];
		num /= 16;
	}
	reverse_string(buffer);
	buffer[start] = '\0';
}
