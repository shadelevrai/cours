/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	print_address(void *ptr, char instruction, int *nbofchara, int *i)
{
	char	address[17];

	if (ptr == NULL)
	{
		write(1, "(nil)", 5);
		*nbofchara += 5;
		*i += 2;
		return (0);
	}
	ft_memset(address, 0, sizeof(address));
	if (instruction == 'x')
		to_hexint((unsigned long)ptr, address, instruction);
	else
		to_hex((unsigned long)ptr, address, instruction);
	if (instruction == 'p')
	{
		*nbofchara = *nbofchara + 2;
		write(1, "0x", 2);
	}
	*nbofchara = *nbofchara + ft_strlen(address);
	write(1, address, ft_strlen(address));
	*i = *i + 2;
	return (*nbofchara);
}

void	print_address_int(int ptr, char instruction, int *nbofchara)
{
	char	address[17];

	ft_memset(address, 0, sizeof(address));
	if (instruction == 'x' || instruction == 'X')
	{
		to_hexint((unsigned long)ptr, address, instruction);
	}
	else
	{
		to_hex((unsigned long)ptr, address, instruction);
	}
	if (instruction == 'p')
	{
		*nbofchara = *nbofchara + 2;
		write(1, "0x", 2);
	}
	*nbofchara = *nbofchara + ft_strlen(address);
	write(1, address, ft_strlen(address));
}
