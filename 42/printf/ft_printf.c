/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	argumentpercent(char argument, int *i, int *nbofchara)
{
	if (argument == '%')
	{
		ft_putchar('%', nbofchara);
		*i = *i + 2;
	}
}

void	argumentx(char argument, va_list args, int *i, int *nbofchara)
{
	int	character;

	if (argument == 'x' || argument == 'X')
	{
		character = va_arg(args, int);
		if (character == 0)
		{
			write(1, "0", 1);
			(*nbofchara)++;
			*i += 2;
			return ;
		}
		print_address_int(character, argument, nbofchara);
		*i = *i + 2;
	}
}

int	print(char argument, va_list args, int *i, int *nbofchara)
{
	if (argument == 'c')
	{
		ft_putchar((char)va_arg(args, int), nbofchara);
		*i = *i + 2;
	}
	if (argument == 'd' || argument == 'i')
	{
		ft_putnbr(va_arg(args, int), nbofchara);
		*i = *i + 2;
	}
	if (argument == 's')
		return (ft_putstr(va_arg(args, char *), nbofchara, i));
	if (argument == 'p')
		return (print_address(va_arg(args, void *), argument, nbofchara, i));
	if (argument == 'u')
	{
		ft_putnbrunsigned(va_arg(args, unsigned int), nbofchara);
		*i = *i + 2;
	}
	argumentpercent(argument, i, nbofchara);
	argumentx(argument, args, i, nbofchara);
	return (0);
}

int	ft_printf(const char *arguments, ...)
{
	int		i;
	int		nbofchara;
	va_list	args;

	i = 0;
	va_start(args, arguments);
	nbofchara = 0;
	if (!arguments)
		return (0);
	while (arguments[i] != '\0')
	{
		if (arguments[i] != '%')
		{
			nbofchara++;
			write(1, &arguments[i], 1);
			i++;
		}
		if (arguments[i] == '%')
		{
			print(arguments[i + 1], args, &i, &nbofchara);
		}
	}
	va_end(args);
	return (nbofchara);
}
