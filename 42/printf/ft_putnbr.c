/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_putnbr(int n, int *nbofchara)
{
	if (n == -2147483648)
	{
		write(1, "-2147483648", 11);
		(*nbofchara) += 11;
		return ;
	}
	if (n < 0)
	{
		ft_putchar('-', nbofchara);
		n = -n;
	}
	if (n > 9)
	{
		ft_putnbr(n / 10, nbofchara);
	}
	ft_putchar((n % 10) + '0', nbofchara);
}
