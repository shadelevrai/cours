/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_putstr(char *str, int *nbofchara, int *i)
{
	int	y;

	if (str == NULL)
	{
		write(1, "(null)", 6);
		*nbofchara += 6;
		*i += 2;
		return (6);
	}
	y = 0;
	while (str[y])
	{
		write(1, &str[y], 1);
		(*nbofchara)++;
		y++;
	}
	*i = *i + 2;
	return (y);
}
