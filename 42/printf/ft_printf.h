/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stddef.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdarg.h>

int		ft_printf(const char *arguments, ...);
void	ft_putchar(char c, int *nbofchara);
size_t	ft_strlen(const char *str);
void	ft_putnbr(int n, int *nbofchara);
void	reverse_string(char *str);
void	*ft_memset(void *pointer, int value, size_t num);
void	to_hex(unsigned long num, char *buffer, char instruction);
void	to_hexint(unsigned int num, char *buffer, char instruction);
void	ft_putnbrunsigned(unsigned int n, int *nbofchara);
int		print_address(void *ptr, char instruction, int *nbofchara, int *i);
void	print_address_int(int ptr, char instruction, int *nbofchara);
int		ft_putstr(char *str, int *nbofchara, int *i);

#endif
