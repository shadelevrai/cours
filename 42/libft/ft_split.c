/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*prepare_split_data(char *s, char c, int *segment_count)
{
	size_t	i;
	int		token;
	char	*str;

	if (!s)
		return (NULL);
	*segment_count = 0;
	i = 0;
	token = 0;
	str = malloc((ft_strlen(s) + 1) * sizeof(char));
	if (!str)
		return (NULL);
	while (s[i])
	{
		if (!(s[i] == c && (s[i + 1] == c || s[i + 1] == '\0')))
		{
			str[token++] = s[i];
			if ((i == 0 || s[i - 1] == c) && s[i] != c)
				(*segment_count)++;
		}
		i++;
	}
	str[token] = '\0';
	free(s);
	return (str);
}

static void	thewhileinsidethewhile(char *strnodouble, char **array2, int *p)
{
	int	j;

	array2[p[2]] = malloc((p[1] - p[0] + 1) * sizeof(char));
	if (!array2[p[2]])
	{
		while (p[2] > 0)
		{
			p[2]--;
			free(array2[p[2]]);
		}
		free(array2);
		return ;
	}
	j = p[0];
	while (j < p[1])
	{
		array2[p[2]][j - p[0]] = strnodouble[j];
		j++;
	}
	array2[p[2]][j - p[0]] = '\0';
	array2[p[2] + 1] = NULL;
}

static void	thewhile(char *strnodouble, char c, char **array2)
{
	size_t	i;
	int		p[3];

	i = 0;
	p[0] = 0;
	p[2] = 0;
	while (strnodouble[i])
	{
		if (strnodouble[i] == c || i == ft_strlen(strnodouble) - 1)
		{
			if (i == ft_strlen(strnodouble) - 1)
				p[1] = i + 1;
			else
				p[1] = i;
			thewhileinsidethewhile(strnodouble, array2, p);
			p[2]++;
			p[0] = i + 1;
		}
		i++;
	}
}

static char	**emptyarray(int instruction, char *p1)
{
	char	**array2;

	array2 = NULL;
	if (instruction == 1)
	{
		array2 = malloc(sizeof(char *));
		if (!array2)
			return (NULL);
		array2[0] = NULL;
		return (array2);
	}
	if (instruction == 2)
	{
		free(p1);
		array2 = malloc(sizeof(char *));
		if (!array2)
			return (NULL);
		array2[0] = NULL;
		return (array2);
	}
	return (array2);
}

char	**ft_split(char const *s, char c)
{
	char	set[2];
	char	*strwithoutdouble;
	char	**array2;
	int		segment_count;
	char	*stringtrim;

	if (s[0] == '\0' || !s)
		return (emptyarray(1, ""));
	set[0] = c;
	set[1] = '\0';
	stringtrim = ft_strtrim(s, set);
	if (!stringtrim[0])
		return (emptyarray(2, stringtrim));
	strwithoutdouble = prepare_split_data(stringtrim, c, &segment_count);
	if (strwithoutdouble[0] == '\0' || !strwithoutdouble)
	{
		free(stringtrim);
		return (emptyarray(2, strwithoutdouble));
	}
	array2 = malloc((segment_count + 1) * sizeof(char *));
	if (!array2)
		return (NULL);
	thewhile(strwithoutdouble, c, array2);
	free(strwithoutdouble);
	return (array2);
}
