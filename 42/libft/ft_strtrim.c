/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/12/17 18:34:11 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	trim(char chara, const char *set)
{
	int	i;

	i = 0;
	while (set[i])
	{
		if (set[i] == chara)
		{
			return ('y');
		}
		i++;
	}
	return ('n');
}

char	*ft_strtrimnext(size_t start, size_t end, const char *s1)
{
	char	*str;
	size_t	i;

	str = malloc(end - (start - 1) + 1 * sizeof(char));
	if (!str)
		return (0);
	i = 0;
	while (i < end - (start - 1))
	{
		str[i] = s1[start + i];
		i++;
	}
	str[i] = '\0';
	return (str);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	size_t	start;
	size_t	end;

	if (ft_strlen(s1) == 0)
		return (ft_strdup(""));
	if (!set)
		return (ft_strdup(s1));
	start = 0;
	end = ft_strlen(s1) - 1;
	while (trim(s1[start], set) == 'y')
		start++;
	while (trim(s1[end], set) == 'y')
		end--;
	if (end - (start - 1) <= 0)
		return (ft_strdup(""));
	if (start >= ft_strlen(s1))
		return (ft_strdup(""));
	return (ft_strtrimnext(start, end, s1));
}
