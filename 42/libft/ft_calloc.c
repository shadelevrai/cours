/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t num, size_t size)
{
	int		sizes;
	char	*p;
	int		i;
	void	*ptr;

	sizes = num * size;
	ptr = malloc(sizes);
	i = 0;
	if (!ptr)
	{
		return (0);
	}
	p = ptr;
	while (i < sizes)
	{
		p[i] = 0;
		i++;
	}
	return (ptr);
}
