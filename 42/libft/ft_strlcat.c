/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t size)
{
	unsigned int	i;
	unsigned int	j;
	unsigned int	dest_len;
	unsigned int	src_len;

	i = 0;
	j = 0;
	while (dest[i] && i < size)
	{
		i++;
	}
	dest_len = i;
	src_len = 0;
	while (src[src_len])
		src_len++;
	if (size == 0 || dest_len >= size)
		return (size + src_len);
	while (src[j] && (dest_len + j) < size - 1)
	{
		dest[dest_len + j] = src[j];
		j++;
	}
	if (j < size)
		dest[dest_len + j] = '\0';
	return (dest_len + src_len);
}
