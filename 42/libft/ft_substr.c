/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	func1(char const *s, size_t *i, unsigned int *length)
{
	while (s[*i])
	{
		(*length)++;
		(*i)++;
	}
}

static void	func2(char *str, char const *s, size_t *i, int *j)
{
	str[*i] = s[*j];
	(*i)++;
	(*j)++;
}

static char	*func3(void)
{
	char	*str;

	str = (char *)malloc(1);
	if (!str)
		return (NULL);
	str[0] = '\0';
	return (str);
}

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	unsigned int	length;
	size_t			i;
	int				j;
	char			*str;

	if (!s)
		return (NULL);
	length = 0;
	i = 0;
	func1(s, &i, &length);
	if (start >= i)
		return (func3());
	if (len > i - start)
		len = i - start;
	str = (char *)malloc(len + 1);
	if (!str)
		return (NULL);
	i = 0;
	j = start;
	while (i < len && s[j])
		func2(str, s, &i, &j);
	str[i] = '\0';
	return (str);
}
