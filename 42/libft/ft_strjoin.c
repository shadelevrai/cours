/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*create(char const *s1, char const *s2, int i)
{
	char	*str2;
	int		lenghts;

	lenghts = 0;
	while (s1[i])
	{
		lenghts++;
		i++;
	}
	i = 0;
	while (s2[i])
	{
		lenghts++;
		i++;
	}
	i = 0;
	str2 = (char *)malloc(lenghts + 1);
	if (!str2)
		return (NULL);
	return (str2);
}

static void	thewhile(char *str, char const *s2, int *i, int *j)
{
	str[*i] = s2[*j];
	(*i)++;
	(*j)++;
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*str;
	int		i;
	int		j;

	if (!s1)
		s1 = "";
	if (!s2)
		s2 = "";
	i = 0;
	str = create(s1, s2, i);
	if (!str)
		return (NULL);
	while (s1[i])
	{
		str[i] = s1[i];
		i++;
	}
	j = 0;
	while (s2[j])
	{
		thewhile(str, s2, &i, &j);
	}
	str[i] = '\0';
	return (str);
}
