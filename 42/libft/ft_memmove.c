/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *destination, const void *source, size_t size)
{
	unsigned char		*a;
	unsigned char		*b;
	long unsigned int	i;

	a = (unsigned char *)destination;
	b = (unsigned char *)source;
	i = 0;
	if (a < b)
	{
		while (i < size)
		{
			a[i] = b[i];
			i++;
		}
	}
	else if (a > b)
	{
		i = size;
		while (i > 0)
		{
			a[i - 1] = b[i - 1];
			i--;
		}
	}
	return (destination);
}
