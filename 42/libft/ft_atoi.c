/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*insertblank(const char *str)
{
	char	set[7];
	char	*stringtrim;

	set[0] = ' ';
	set[1] = '\t';
	set[2] = '\n';
	set[3] = '\v';
	set[4] = '\f';
	set[5] = '\r';
	set[6] = '\0';
	stringtrim = ft_strtrim(str, set);
	return (stringtrim);
}

int	ft_atoi(const char *str)
{
	char	*s;
	int		number;
	int		i;

	s = insertblank(str);
	if (s[0] != '-' && s[0] != '+' && !(s[0] >= '0' && s[0] <= '9'))
		return (0);
	if ((s[0] == '-' || s[0] == '+') && (!(s[1] >= '0' && s[1] <= '9')))
		return (0);
	i = 0;
	if (s[0] == '-' || s[0] == '+')
		i = 1;
	number = 0;
	while (s[i])
	{
		if (!(s[i] >= '0' && s[i] <= '9'))
			break ;
		number = number * 10 + (s[i] - '0');
		i++;
	}
	if (s[0] == '-')
		number = -number;
	free(s);
	return (number);
}
