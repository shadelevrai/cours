/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rdjoher <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/08/21 19:25:43 by rdjoher           #+#    #+#             */
/*   Updated: 2024/09/03 08:45:34 by rdjoher          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_rev_int_tab(char *str, int i)
{
	int	begin;
	int	final;
	int	temp;

	begin = 0;
	if (str[0] == '-')
	{
		i--;
		str++;
	}
	final = i - 1;
	while (begin < i / 2)
	{
		temp = str[begin];
		str[begin] = str[final];
		str[final] = temp;
		begin++;
		final--;
	}
}

static void	ft_inttochar(int n, char *str, int *j)
{
	if (n < 0)
	{
		str[*j] = '-';
		n = -n;
		(*j)++;
		ft_inttochar(n, str, j);
		return ;
	}
	if (n > 9)
	{
		str[*j] = (n % 10) + '0';
		(*j)++;
		ft_inttochar(n / 10, str, j);
		return ;
	}
	else
	{
		str[*j] = n + '0';
		return ;
	}
}

static void	ft_intlen(int n, int *i)
{
	if (n == -2147483648)
	{
		*i = 11;
		return ;
	}
	if (n < 0)
	{
		(*i)++;
		n = -n;
		ft_intlen(n, i);
		return ;
	}
	if (n > 9)
	{
		(*i)++;
		ft_intlen(n / 10, i);
		return ;
	}
	else
	{
		(*i)++;
		return ;
	}
}

static char	*bignegativenumber(void)
{
	char	*str;
	char	*s;
	int		i;

	i = 0;
	s = "-2147483648";
	str = malloc((11 + 1) * sizeof(char));
	if (!str)
		return (NULL);
	while (s[i])
	{
		str[i] = s[i];
		i++;
	}
	str[11] = '\0';
	return (str);
}

char	*ft_itoa(int n)
{
	char	*str;
	int		i;
	int		j;

	if (n == -2147483648)
	{
		return (bignegativenumber());
	}
	else
	{
		i = 0;
		j = 0;
		ft_intlen(n, &i);
		str = malloc((i + 1) * sizeof(char));
		if (!str)
			return (NULL);
		ft_inttochar(n, str, &j);
		ft_rev_int_tab(str, i);
		str[i] = '\0';
		return (str);
	}
}
