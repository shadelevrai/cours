const express = require('express');
const router = express.Router();

router.get('/home', function (req, res) {
  res.render('index');
});

router.get('/add-game', function (req, res) {
  
  res.render('addGame');
});

router.get('/add-blindtest', function (req, res) {
  res.render('addBlindTest',{messages:req.flash('info')});//et on l'envoie au chargement de la page
});

router.post('/upload', function (req, res, next) {
  req.flash('info', 'Flash is back!');//On initialise le message ici
  res.redirect('/add-blindtest');

});

module.exports = router;