const express = require('express');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017";
const ObjectId = require('mongodb').ObjectID;
const flash = require('connect-flash-plus');
var session = require('express-session');

let routes = require('./routes/routes');

app.set('port', process.env.PORT || 8080);

app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false
}));

app.use(flash());

app.use('/', routes);

//view engine setup
app.set('views', __dirname + '/views');
app.set('view engine','jsx');
app.engine('jsx', require('express-react-views').createEngine());

var server = app.listen(app.get('port'), function () {
  console.log('app started');
});