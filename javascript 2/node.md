Node est un runtime environnement en javascript, il permet de lancer des script

1/ HTTP

LEs methodes http sont :
GET (pour récupérer des données)
POST (pour envoyer des données)
PUT (La méthode PUT remplace toutes les représentations actuelles de la ressource visée par le contenu de la requête.)
PATCH (La méthode PATCH est utilisée pour appliquer des modifications partielles à une ressource.)
DELETE (La méthode DELETE supprime la ressource indiquée.)
ALL (prend toutes les methodes)

2/ Un middleware est un script/module qui se place entre les HTTP request et les HTTP responses, comme Express.