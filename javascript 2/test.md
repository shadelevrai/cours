1/ fonction 

import {maFonction} from "./index"

describe("libs",()=>{
    it("la fonction retourne bien une addition,()=>{
        expect(maFonction(2,3)).toStrictEqual(5);
    })
    test("character position start", () => {
        const setGameOver = () => { }
        const { getByTestId } = render(<Character characterLocationLeft={1} characterLocationTop={1} beginGame={true} index={20} characterDirection={["A", "A", "D", "A", "D", "A", "A", "D", "A", "A", "D", "A", "A", "G", "A", "G", "G", "A", "A", "D"]} lineMontains={[[1, 0], [2, 1]]} setGameOver={setGameOver} />)
        expect(getByTestId('character')).toHaveStyle({ marginTop: "100px", marginLeft: "100px" });
    })
})