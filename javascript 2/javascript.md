1/ Variables
Une variable est un contenant qui contient des valeurs.

"let" est une variable qui peut changer
"const" est une variable qui n'est pas mutable


2/ Les types primitifs
Number, String, Boolean,


3/ OBject
Les objets sont ecrit en JSON (Javascript Object Notation) et ont des séries de paires clés/valeurs
Pour accéder à des valeurs dans un objet, on utilise des "dot notation". Par exemple : myBook.title ou  myBook["title"]


4/ Classe
Une classe est un modèle pour un objet dans le code. Elle permet de créer des instances.
> class Book {

}

4.1/ Constructor
Le  constructor d'une classe est la fonction qui est appelée quand on crée une nouvelle instance de cette classe avec le mot clé "new" 
> class Book {
    constructor(title,author,pages){
        this.title = title;
        this.author = author;
        this.pages = pages;
    }
}

Maintenant que la classe est terminée, vous pouvez créer des instances par le mot clé   new  :
> let myBook = new Book("l'histoire de Tao","will",250)
Cela créer une instance qui sera un objet

4.2/ Methodes
Les méthodes sont des fonctions présent dans les classes

class BankAccount {
    constructor(owner, balance) {
        this.owner = owner;
        this.balance = balance;
    }
    
    showBalance() {
        console.log("Solde: " + this.balance + " EUR");
    }
    
    deposit(amount) {
        console.log("Dépôt de " + amount + " EUR");
        this.balance += amount;
        this.showBalance();
    }
    
    withdraw(amount) {
        if (amount > this.balance) {
                 console.log("Retrait refusé !");
        } else {
            console.log("Retrait de " + amount + " EUR");
            this.balance -= amount;
            this.showBalance();
        }
    }
}

4.3/ Methodes utilitaires (helpers)

Dans une classe, on peut créer une fonction qui peut être directement appelé, sans instancié la classe dans un nouvel objet. Pour ça, on créer une fonction static. Si la class n'a pas besoin d'être instancier, pas besoin de constructor.

class BePolite {
    
    static sayHello() {
        console.log("Hello!");
    }
    
    static sayHelloTo(name) {
        console.log("Hello " + name + "!");
    }
    
    static add(firstNumber, secondNumber) {
        return firstNumber + secondNumber;
    }
}

BePolite.sayHello(); // imprime "Hello!""

BePolite.sayHelloTo("Will"); // imprime "Hello Will!""

const sum = BePolite.add(2, 3); // sum = 5




5/ Tableau


6/ Egalité simple et stricte
l'égalité simple vérifie la valeur, mais pas le type. Exemple : 5 == "5" est true. L'égalité complexe (===) lui, retourne false car les type sont pas les mêmes.


7/ Fonctions

Les fonctions servent à ne pas se répéter , DRY – Don't Repeat Yourself 