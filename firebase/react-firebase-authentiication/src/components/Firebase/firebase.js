import app from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyDk4gViFaIWIjoEOHN2TCEze8J9N91FF4M" ,
  authDomain: "test-4dc24.firebaseapp.com",
  databaseURL: "https://test-4dc24.firebaseio.com",
  projectId: "test-4dc24",
  storageBucket: "test-4dc24.appspot.com",
  messagingSenderId: "414089529372",
  appId: "1:414089529372:web:63d9f186150a6aa48ab7d9"
};

class Firebase {
  constructor() {
    app.initializeApp(config);
    this.auth = app.auth();
  }

  doCreateUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);
 
    doPasswordUpdate = password =>
      this.auth.currentUser.updatePassword(password);
}
 
export default Firebase;



/*
import app from 'firebase/app';
 
const prodConfig = {
  apiKey: process.env.REACT_APP_PROD_API_KEY,
  authDomain: process.env.REACT_APP_PROD_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_PROD_DATABASE_URL,
  projectId: process.env.REACT_APP_PROD_PROJECT_ID,
  storageBucket: process.env.REACT_APP_PROD_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_PROD_MESSAGING_SENDER_ID,
};
 
const devConfig = {
  apiKey: process.env.REACT_APP_DEV_API_KEY,
  authDomain: process.env.REACT_APP_DEV_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DEV_DATABASE_URL,
  projectId: process.env.REACT_APP_DEV_PROJECT_ID,
  storageBucket: process.env.REACT_APP_DEV_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_DEV_MESSAGING_SENDER_ID,
};
 
const config =
  process.env.NODE_ENV === 'production' ? prodConfig : devConfig;
 
class Firebase {
  constructor() {
    app.initializeApp(config);
  }
}
 
export default Firebase;
*/