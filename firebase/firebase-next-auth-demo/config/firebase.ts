// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth  } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
//   apiKey: process.env.FIREBASE_CONFIG_APIKEY,
//   authDomain: process.env.FIREBASE_CONFIG_AUTHDOMAIN,
//   projectId: process.env.FIREBASE_CONFIG_PROJECT_ID,
//   storageBucket: process.env.FIREBASE_CONFIG_STORAGE_BUCKET,
//   messagingSenderId: process.env.FIREBASE_CONFIG_MESSENGING_SENDER_ID,
//   appId: process.env.FIREBASE_CONFIG_APP_ID,
//   measurementId: process.env.FIREBASE_CONFIG_MEASUREMENT_ID
apiKey: "AIzaSyCSCvkLbWUlE3JYGqooH-f5umRNXVr-hUs",
authDomain: "myanh-tatoo.firebaseapp.com",
projectId: "myanh-tatoo",
storageBucket: "myanh-tatoo.appspot.com",
messagingSenderId: "1074155814724",
appId: "1:1074155814724:web:da52bd4640cd804257a111",
measurementId: "G-93CG9BFL3Z"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth()