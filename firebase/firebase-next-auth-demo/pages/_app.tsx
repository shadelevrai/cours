//https://articles.wesionary.team/email-password-authentication-with-firebase-9-and-next-js-12-8dc836336b39

import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Navbar from "../components/navbar";
import { AuthContextProvider } from "../context/AuthContext";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <AuthContextProvider>

      <Navbar>
        <Component {...pageProps} />
      </Navbar>
    </AuthContextProvider>
  )
}

export default MyApp
