import React from 'react';
import Lala from './Components/Le01_rendu'
import Lolo from './Components/Le02_chargement_de_données'
// import Lili from './Components/Le03.simuler_des_modules'
import Lele from './Components/Le04_evenements'

function App() {
  return (
    <div>
      <p>Learn test</p>
      <Lala name="jenny"/>
      <Lolo id='lili'/>
      {/* <Lili name='shade' email='lala@lala.com' site='sqdqdqd' center='mm'/> */}
      <Lele/>
    </div>
  );
}

export default App;
