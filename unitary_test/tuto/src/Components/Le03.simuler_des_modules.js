

// contact.js

import React from "react";
import Map from "./map";

export default function Contact(props) {
  return (
    <div>
      <address>
        Contacter {props.name} par{" "}
        <a data-testid="email" href={"mailto:" + props.email}>
          e-mail
        </a>
        ou sur son <a data-testid="site" href={props.site}>
          site web
        </a>.
      </address>
      <Map center={props.center} />
    </div>
  );
}