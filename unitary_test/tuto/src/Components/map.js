import React from "react";

import { LoadScript, GoogleMap } from "react-google-maps";
export default function Map(props) {
  return (
    <LoadScript id="script-loader" googleMapsApiKey="AIzaSyCaxl13-3pglWGIZ6PuzGzfPBBi2Ni3a-8">
      <GoogleMap id="example-map" center={props.center} />
    </LoadScript>
  );
}