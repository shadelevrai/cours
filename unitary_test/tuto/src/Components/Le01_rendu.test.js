import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import Hello from "./Le01_rendu";

let container = null;
beforeEach(() => {
  // met en place un élément DOM comme cible de rendu
  container = document.createElement("div");
  document.body.appendChild(container);
});

it("test si le parametre passé est affiché", () => {
  act(() => {
    render( < Hello /> , container);
  });
  expect(container.textContent).toBe("Salut, étranger");

  act(() => {
    render( < Hello name = "Jenny" /> , container);
  });
  expect(container.textContent).toBe("Bonjour, Jenny !");

  act(() => {
    render( < Hello name = "Margaret" /> , container);
  });
  expect(container.textContent).toBe("Bonjour, Margaret !");
});

afterEach(() => {
    // nettoie en sortie de test
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});