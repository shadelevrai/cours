var assert = require('assert');

describe.only('test01', () => {
    it('should return 2', () => {
        assert.equal(2, 1 + 1)
    })
})
// Seul ce test va se lancer

describe('test02', () => {
    it.only('sould return 4', () => {
        assert.equal(4, 2 + 2)
    })
})

// describe.only('03',()=>{
describe.skip('03', () => {
    it('should return 6', () => {
        assert.equal(6, 3 + 3)
    })
    // Seul ce test (it) va être lancé
    it('should return 8', () => {
        assert.equal(8, 4 + 4)
    })
})
