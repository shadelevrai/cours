// https://codeburst.io/how-to-test-javascript-with-mocha-the-basics-80132324752e
// Require the built in 'assertion' library
var assert = require('assert');
// Create a group of tests about Arrays
describe('Array', function() {
  // Within our Array group, Create a group of tests for indexOf
  describe('#indexOf()', function() {
    // A string explanation of what we're testing
    it('should return -1 when the value is not present', function(){
      // Our actual test: -1 should equal indexOf(...)
      assert.equal(-1, [1,2,3].indexOf(4));
    });
  });
});

// https://codeburst.io/javascript-unit-testing-using-mocha-and-chai-1d97d9f18e71
describe('Basic Mocha String Test', function () {
  it('should return number of charachters in a string', function () {
         assert.equal("Hello".length, 5);
     });
  it('should return first charachter of the string', function () {
         assert.equal("Hello".charAt(0), 'H');
     });
 });