import { useState } from 'react';

const STATUS = {
    HOVERED: 'hovered',
    NORMAL: 'normal',
};

export default function Exo1() {
    const [status, setStatus] = useState(STATUS.NORMAL);

    const onMouseEnter = () => {
        setStatus(STATUS.HOVERED);
    };

    const onMouseLeave = () => {
        setStatus(STATUS.NORMAL);
    };

    function sum(a, b) {
        return a + b;
      }

    return (
        <div>
            <a
                className={status}
                href={'#'}
                onMouseEnter={onMouseEnter}
                onMouseLeave={onMouseLeave}
            ></a>
        </div>
    )
}

