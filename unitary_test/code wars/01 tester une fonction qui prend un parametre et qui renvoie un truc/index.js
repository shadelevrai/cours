//Cette fonction prend une chaine de charactère et renvoi cette chaine de charactère sans voyelle (y n'est pas considéré comme une voyelle)
function disemvowel(str) {
    let withoutVowels = []
    for (let i = 0; i < str.length; i++) {
        if (str[i] === 'a' || str[i] === 'e' || str[i] === 'u' || str[i] === 'i' || str[i] === 'o' || str[i] === 'A' || str[i] === 'E' || str[i] === 'U' || str[i] === 'I' || str[i] === 'O') {
            console.log(str[i])
        } else {
            withoutVowels.push(str[i])
        }
    }
    console.log(withoutVowels)
    return withoutVowels.join('')
}

// Test pouir savoir si on donne une chaine de charact_re à la fonction disevowel, est-ce qu'elle nous renvoi la même chaine sans voyelle ?
Test.assertEquals(disemvowel("This website is for losers LOL!"),
  "Ths wbst s fr lsrs LL!")
  
Test.assertEquals(disemvowel(
    "No offense but,\nYour writing is among the worst I've ever read"
  ), "N ffns bt,\nYr wrtng s mng th wrst 'v vr rd")

Test.assertEquals(disemvowel(
    "What are you, a communist?"
  ), "Wht r y,  cmmnst?")