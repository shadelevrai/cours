//https://dev.to/dinhhuyams/introduction-to-react-memo-usememo-and-usecallback-5ei3
import { useMemo } from 'react'
import Counter from './Counter'

export default function Home() {
  const [count1, setCount1] = React.useState(0)
  const [count2, setCount2] = React.useState(0)

  const tab = ['0','1','2','3']

  const increaseCounter1 = () => {
    setCount1(count1 => count1 + 1)
}

const test = useMemo(()=>tab.map(tabs =>{
  return (<p>{tabs}</p>)
  })
,[tab])

  return (
    <>
      <button onClick={increaseCounter1}>Increase counter 1</button>
      <Counter value={count1}>Counter 1</Counter>
      <Counter value={count2}>Coutner 2</Counter>
      <div>{test}</div>
    </>
  )
}
