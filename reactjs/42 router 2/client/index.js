import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class VacancySign extends Component{
  render(){

    return(
      <div>
        <Router>
          <div>
          <Link to='/elem1'><p>Element1</p></Link>
          <Link to='/elem2'><p>Element2</p></Link>
          <Link to='/elem3'><p>Element3</p></Link>
          <Route path='/elem1' component={elem1}/>
          <Route path='/elem2' component={elem2}/>
          <Route path='/elem3' component={elem3}/>
          </div>
        </Router>
      </div>

    )
  }
}

const elem1 = () => (
  <div>
    <p>Salut elem1</p>
  </div>
)

const elem2 = () => (
  <div>
    <p>Salut elem2</p>
  </div>
)

const elem3 = () => (
  <div>
    <p>Salut elem3</p>
  </div>
)



ReactDOM.render(
  <VacancySign />,
  document.querySelector('#root')
);
