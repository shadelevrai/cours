import React from 'react'

import Title from './Title'
import Converter from './Converter'

export default class App extends React.Component {
  render(){
    return(
      <div>
        <Title />
        <Converter />
      </div>
    )
  }
}