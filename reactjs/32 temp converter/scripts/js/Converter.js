import React from 'react'

export default class Converter extends React.Component {

    state = {
      type: 'number',
      defaultValue: 'Entrez une valeur',
      valueFar: ''
    }

    test = () => {
      let calc = this.input.value * 9 / 5 + 32;
      this.setState({
        valueFar: calc
      })
    }

    render() {
      return (
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-offset-3 col-xs-6">
              <input type={this.state.type} placeholder={this.state.defaultValue} onChange={e=>this.test()} ref={(input) => this.input = input}  />
              <p>En fahrenheit, ça fait</p>
              <p>{this.state.valueFar}</p>
            </div>
          </div>
        </div>
      )
    }
}