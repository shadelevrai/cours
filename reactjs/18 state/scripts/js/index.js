import React from 'react'
import ReactDOM from 'react-dom'
import HelloWorld from './HelloWorld'

class App extends React.Component {

  // componentWillUpdate(nextProps, nextState){
  //   console.log('pika');
  // }

state = {
  var1 : 'lala',
  var2 : 'lolo'
}

//pour que le ReactDOM.render() réagisse au changement, il faut mettre setState
change = () =>{
  this.setState({var1:'lili'})
}
  render() {
    return (
      <div>
        <HelloWorld name='Dan'/>
        <h2>{this.state.var1}</h2>
        <h3 onClick={e =>this.change()}>click me</h3>
      </div>
    )
  }
}

ReactDOM.render(<App /> ,
  document.getElementById('root')
);
