import React from 'react'
import ReactDOM from 'react-dom'

function disBonjour(){
  console.log('value')
}

class App extends React.Component {

  state = {
    lala:'lolo',
    isToggleOn : true,
  }

  handleClick = () => {
    console.log('this is : ', this);
  }

  disBonjour2 = (value) => {
    console.log(value);
  }

  render() {
 return (
  <div>
    <h1 onClick={e => console.log('lala')}> yeah</h1>{/*il faut mettre e (event) quand on envoi un parametre sinon la fonction se lancer au chargement*/}
    <h2 onClick={e => console.log(this.state.lala)}>yo !</h2>
    <h3 onClick={e => disBonjour()}>salut</h3>
    <h1 onClick={this.handleClick}>click me</h1>{/*au click, on montre ce qu'il y a dans le composant*. Pas besoin de e (event) car il n'y a pas de parametre/}
    <h2 onClick={e => this.disBonjour2('hoho')}>dis bonjour 2</h2> {/*Si je mets un parametre à ma fonction, je dois mettre e => sinon la fonction va se lancer au chargement*/}
    <p onClick={e => console.log("poke")}>ohoh</p>
  </div>
    )
  }
}
ReactDOM.render(
  <App />,
  document.getElementById('root')
);
