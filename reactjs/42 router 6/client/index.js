import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class VacancySign extends Component{
  render(){

    return(
      <div>
<Router>
  <div>
    <div>
      <Link to='/hello/abc'>Hello</Link>
    </div>
    <div>
      <Route path='/hello/:user' component={UserCompo} />
    </div>
  </div>
</Router>
</div>

    )
  }
}

const UserCompo = ({match}) => (
  <div>
    <p>Voici les parametres : {match.params.user}</p>
    <p>Voici le lien : {match.url}</p>
  </div>
)

ReactDOM.render(
  <VacancySign />,
  document.querySelector('#root')
);
