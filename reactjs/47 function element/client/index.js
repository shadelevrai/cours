import React, { Component } from 'react';
import ReactDOM from 'react-dom';



class VacancySign extends Component{


  render(){

    function Elem(){
      return <p>hello</p>
    }

    function Elem2({lala = 'shade'}){
      return <p>salut {lala}</p>
    }

    const GuessCount = ({ guesses = 'lala' }) => <div className="guesses">{guesses}</div>

    return(
      <div>
        <Elem />
        <Elem2 />
        <Elem2 lala='shade2'/>
        <GuessCount />
      </div>
    )
  }
}

ReactDOM.render(
  <VacancySign />,
  document.querySelector('#root')
);
