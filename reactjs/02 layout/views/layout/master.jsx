import React from 'react'

export default class MasterLayout extends React.Component {
  render(){
    return(
          <html>
          <head>
            <meta content='text/html' />
            <title>{this.props.name}</title>
          </head>
          <body>
          {this.props.children}{/*cette methode prend ce qui il a dans le index.jsx et le mets dans le body*/}
          </body>
          </html>
    )
  }
}