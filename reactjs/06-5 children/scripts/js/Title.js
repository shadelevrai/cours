import React, { Component } from "react";

//Dans le console.log, on aura tout ce qui y'a dans la balise 'Title' qui est dans le fichier App.js
//On peut y avoir accès avec this.props.children
export default class Title extends Component{
  render(){
    console.log(this.props);
    const {children} = this.props;//Il faut obligatoirement mettre des accolades entre la variable. Et la variable doit se nommer children
    return(
      <div>
      <p>je suis dans l'enfant</p>
      <h2>{children}</h2>
      </div>
    )
  }
}