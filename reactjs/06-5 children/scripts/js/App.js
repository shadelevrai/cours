import React, { Component } from 'react'
import Title from './Title'

export default class App extends Component {
  render(){
    return(
      <div>
      <h2>Je suis dans le parent</h2>
      <Title>Je suis dans le parent mais dans le Title</Title>
      </div>
    )
  }
} 
