const express = require('express');
const app = express();

var routes = require('./routes/route');

app.use('/', routes);

app.set('port', process.env.PORT || 8888);

//view engine setup
app.set('view engine', 'ejs');
app.set('views', './');
app.use(express.static(__dirname + '/'));
app.engine('jsx', require('express-react-views').createEngine());

var server = app.listen(app.get('port'), function () {
  console.log('app starteddd');
});