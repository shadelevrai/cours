// https://putaindecode.io/articles/react-hooks-proposal/
// https://www.fullstackreact.com/articles/an-introduction-to-hooks-in-react/#fetch-data-and-update-state
// https://reactjs.org/docs/hooks-effect.html#tip-optimizing-performance-by-skipping-effects

import React, { useState, useEffect } from "react";

export default function Form(props) {

  //la variable doit être un tableau avec deux éléments : le nom de la variable et la fonction qui permet de la mettre à jour
  const [name, setName] = useState("putaindecode");

  function handleNameChange(e) {

    //La fonction qui permet de mettre à jour la variable, comme un state
    setName(e.target.value);
  }

  useEffect(() => {
    console.log("je suis l'équivalent des life cycle, je suis lancé au chargement du composant, à la mise à jour du state et à la destruction du composant")
  })
  // Si on met un tableau vide en second argument, le useEffect se lancera juste au montage. Exemple :

  // useEffect(()=>{
  //   console.log("hello")
  // },[])

  // si on met une variable du state dans le tableau, alors ce useEffect se re-lancera quand cette variable changera. Exemple : 

  // useEffect(()=>{
  //   console.log("hello")
  // },[name])


  return (
    <div>
      <label htmlFor="inputName">Name</label>
      <input
        id="inputName"
        type="text"
        value={name}
        name="firstname"
        onChange={handleNameChange}
      />
    </div>
  );
}