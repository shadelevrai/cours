import React from 'react'
import ReactDOM from 'react-dom'
import HelloWorld from './HelloWorld'

class App extends React.Component {

  //componentWillUpdate() is invoked just before rendering when new props or state are being received. Use this as an opportunity to perform preparation before an update occurs. This method is not called for the initial render.
  componentWillUpdate(nextProps, nextState){
    console.log('pika');
  }

state = {
  var1 : 'lala',
  var2 : 'lolo'
}

change = () =>{
  this.setState({var1:'lili'})
}
  render() {
    return (
      <div>
        <HelloWorld name='Dan'/>
        <h2>{this.state.var1}</h2>
        <h3 onClick={e =>this.change()}>click me</h3>
      </div>
    )
  }
}

ReactDOM.render(<App /> ,
  document.getElementById('root')
);
