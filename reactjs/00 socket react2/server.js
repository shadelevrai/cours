import express from 'express';
import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackConfig from './webpack.config.js';
import socketIO from 'socket.io'

const app = express();
const http = require('http')
const server = http.createServer(app)
const io = socketIO(server)

app.use(webpackMiddleware(webpack(webpackConfig)));

app.use(express.static(__dirname + '/'));

io.on('connection', (client) => {
  client.on('message', (message) => {
    console.log('On veut te dire un truc : ', message);
    client.emit('message2', 'tu veux ?')
  });
});

const port = 8000;
io.listen(port);
console.log('listening on port ', port);

console.log('hello');

app.listen(8080, () => {
  console.log('Listening');
});