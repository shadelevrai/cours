
// Le module standard  prop-types  fournit une série de validateurs basés essentiellement sur le type de valeur de base (nombre, texte, booléen, fonction de rappel…) et des agencements plus complexes (tableaux, objets, énumérations de valeurs ou types possibles…)
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types'



class App extends Component{

  
  render(){

    const Person = (props) => <div> 
    <h1>{props.firstName} {props.lastName} {props.ville} {props.codePostal} {props.ville} </h1>
    {props.country ? <p>Country: {props.country}</p> : null}
    </div>;

   

    Person.propTypes = {
      monTableau: PropTypes.array,
      mesCharactere: PropTypes.string,
      firstName:PropTypes.string,
      lastName:PropTypes.string,
      country:PropTypes.string,
      ville:PropTypes.string.isRequired,
      codePostal : PropTypes.number.isRequired
    }
    return(
      <div>
        <Person firstName='shade' lastName='Djoher' country='France' ville='paris' codePostal={55555}/>
      </div>
    )
  }
}


ReactDOM.render(
  <App />,
  document.querySelector('#root')
);
