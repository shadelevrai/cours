import React from 'react'
import ReactDOM from 'react-dom'


class App extends React.Component {

  state = {date: new Date()};

//02 : Quand le Dom a été généré, react lance la fonction DidMount. Cette fonction contient le fonction timerID qui va lanccer et mettre à jour la fonction tick toute les secondes
  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  //Si le timer s'arrête, la fonction 'timerID' sera retiré du DOM
  componentWillUnmount() {
    clearInterval(this.timerID);    
  }

  //cette fonction est appellée par le DidMount. Elle met à jour le 'date' du 'state' avec le 'setState'. Le 'setState' permet à react de savoir qu'il va y avoir du changement dans le state et le fait immediatement dans le 'render()'
  tick() {
    console.log('hello');//'hello' sera lancé toutes le secondes, mais elle sera annulé lors du changement de composant
    
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    )
  }
}
//01 : React envoi le render() du App au dom
ReactDOM.render(<App /> ,
  document.getElementById('root')
);

setTimeout(()=>{
  ReactDOM.render(
    <h1>hello</h1>,
    document.getElementById('root')
  )
},5000)
/*

En bref : 
ComponentDidMount() : Se lance après que le DOM a été généré 

*/