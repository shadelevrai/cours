import React from 'react'
import ReactDOM from 'react-dom'
import HelloWorld from './HelloWorld'

class App extends React.Component {

  //appelé quand le composant a été mis à jour
  componentDidUpdate(prevProps, prevState) {
console.log('didUpdate');

  }

//appelé quand le composant va être mis à jour
  componentWillUpdate(nextProps, nextState){
    console.log('willUpdate');
  }

state = {
  var1 : 'lala',
  var2 : 'lolo'
}

change = () =>{
  this.setState({var1:'lili'})
}
  render() {
    return (
      <div>
        <HelloWorld name='Dan'/>
        <h2>{this.state.var1}</h2>
        <h3 onClick={e =>this.change()}>click me</h3>
      </div>
    )
  }
}

ReactDOM.render(<App /> ,
  document.getElementById('root')
);
