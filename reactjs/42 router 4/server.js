import express from 'express'; 
import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackConfig from './webpack.config.js';

const app = express();
app.use(webpackMiddleware(webpack(webpackConfig)));

app.get('/create-game', (req, res) => {
    res.render('hello')
})
app.use(express.static(__dirname + '/'));

app.listen(8888, () => {
  console.log('Listening');
});