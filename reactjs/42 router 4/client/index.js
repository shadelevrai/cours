import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class VacancySign extends Component{
  render(){

    return(
      <div>
        <Router>
          <div>
          {/* when the pathname is '/elem1' or '/elem1/someStuff', the path matches */}
          <Link to='/elem1/someStuff'><p>Element1</p></Link>
          <Route path='/elem1' component={elem1}/>
          {/* If you only want to match '/roster', then you need to use the "exact" prop. The following will match '/roster', but not '/roster/2'. */}
          {/* <Route exact path='/elem1' component={elem1}/> */}
          </div>
        </Router>
      </div>

    )
  }
}

const elem1 = () => (
  <div>
    <p>Salut elem1</p>
  </div>
)

ReactDOM.render(
  <VacancySign />,
  document.querySelector('#root')
);
