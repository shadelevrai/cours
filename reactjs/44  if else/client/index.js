
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types'



class App extends Component{

    
    constructor(props){
      super(props)
      this.state = {
          etape : 1,
      }
  }

  change = () => {
      this.setState({etape : 2})
  }

  renderelement(){
      if(this.state.etape == 1){
          return <div><div>hello1</div><div onClick={e =>this.change()}>Hello2</div></div>
      }
      if(this.state.etape == 2){
          return <div>hello3</div>
      }
  }

  render(){

      return(
          <div>
              {this.renderelement()}
          </div>
      )
  }
}


ReactDOM.render(
  <App />,
  document.querySelector('#root')
);
