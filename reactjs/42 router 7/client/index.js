import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class VacancySign extends Component{
  render(){

    return(
      <div>
<Router>
  <div>
    <div>
      <Link to='/hello/lala'>Hello</Link>
    </div>
    <div>
      <Route path='/hello/:leParametre' component={UserCompo} />
    </div>
  </div>
</Router>
</div>

    )
  }
}

const UserCompo = ({match}) => {
  // On met du code javascript ici
  let mama = 'dddd';
  let lala = 456454645;
  let lolo = true;
  let gringo = () =>(
    <div>olala</div>
  )
  let azerty = () => {
    let azerty2 = 'ok';
    return(
      <div>
        {azerty2}
      </div>
    )
  }

  // On oublie pas de return le code

  if(lolo){
    return(
      <div>
        <div>voici le lala : {lala}</div>
        <div>{gringo()}</div>
        <div>{azerty()}</div>
      </div>
    )
  }
  return(
    <div>
      <p>Voici les parametres : {match.params.leParametre} </p>
      <p>voici la variable : {mama}</p>
    </div>
  )
}

ReactDOM.render(
  <VacancySign />,
  document.querySelector('#root')
);
