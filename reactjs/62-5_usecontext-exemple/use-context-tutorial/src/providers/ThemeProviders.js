import React, { useState } from 'react';

export const themeContext = React.createContext()

const ThemeProvider = (props) => {
    const [theme, setTheme] = useState('light')
    return (
        <themeContext.Provider value={{ theme, setTheme }}>
            {props.children}
        </themeContext.Provider>
    );
};

export default ThemeProvider;

