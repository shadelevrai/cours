import React, { useContext } from 'react';
import { themeContext } from './providers/ThemeProviders';
import './App.css';

function App() {
  const { theme, setTheme } = useContext(themeContext)

  return (
    <div className="App" style={theme === 'dark' ? { backgroundColor: "#000000" } : null}>
      <header>
        <button onClick={() => setTheme('dark')}>change theme</button>
      </header>
    </div>
  );
}

export default App;
