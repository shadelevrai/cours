import React from 'react'
import ReactDOM from 'react-dom'
// import log from './log'//on importe tout ce qu'il y a dans le fichier. Ne pas oublier le default dans le fichier log.js
// import { log2 } from './aaa'//on importe seulement la fonction log2 du fichier aaa.js
// import * as obj from './bbb' // on stocke toutes les fonction du fichier bbb.js dans un objet

// log('salut');
// log2('cest le log2');
// console.log(obj);

// console.log('hellooooo');

// const title = 'My Minimal React Webpack Babel Setup';

// class App extends React.Component{
//   render(){
//     return(
//       <div>
//       <h1>yeah</h1>
//       </div>
//     )
//   }
// }
//*********************************Une méthode**************************************** */
// function tick(){
//   const element = (
//     <div>
//       <h1>Hello, world!</h1>
//       <h2>It is {new Date().toLocaleTimeString()}.</h2>
//     </div>
//   )
//   ReactDOM.render(
//     element,
//     document.getElementById('root')
//   );
// }

// setInterval(tick, 1000);

//*****************************Une méthode******************************************** */

// function Clock(props) {
//   return (
//     <div>
//       <h1>Hello, world!</h1>
//       <h2>It is {props.date.toLocaleTimeString()}.</h2>
//     </div>
//   );
// }

// function tick() {
//   ReactDOM.render(
//     <Clock date={new Date()} />,
//     document.getElementById('root')
//   );
// }

// setInterval(tick, 1000);

//*****************************une methode*************************************** */


/*function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}

export default class indexComponent extends React.Component{

  render(){
    return (
      <html>
      <head>
      </head>
      <body>
        <div>
          <div>
            <Welcome name='Sara'/>
            <Welcome name='Louis'/>
            <Welcome name='Jack'/>
          </div>
        </div>
      </body>
      </html>
    )
  }
} */

class Clock extends React.Component {
  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.props.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }
}
ReactDOM.render(
  <Clock date={new Date()}/>,
  document.getElementById('root')
);