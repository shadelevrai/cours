import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Child from './public/script/js/Child'

class App extends React.Component {

  lolo(data){
    console.log(data);
    
  }
  render(){
    return(
      <div>
        <Child lala={this.lolo}/>
      </div>
    )
  }
}

ReactDOM.render(
  <App />,
  document.querySelector('#root')
);
