import React, { useEffect } from "react"
import { useState } from '@hookstate/core';
import globalState from "../globalState/index"

export default function Compo3(){

  const state = useState(globalState);

  useEffect(()=>{
    console.log("le console de compo3 doit s'afficher")
  })

  return(
    <div>
      Compo3
      <br></br>
      {console.log('3')}
      {state.get()}
    </div>
  )
}