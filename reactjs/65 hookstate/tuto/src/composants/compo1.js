import React, { useEffect } from 'react';
import { useState } from '@hookstate/core';
import globalState from "../globalState/index"

setInterval(() => globalState.set(p => p + 1), 3000)

export default function Compo1(){
  useEffect(()=>{
    console.log('compo1')
  },[])
  const state = useState(globalState);
  return <>
      <b>Counter value: {state.get()}</b> (watch +1 every 3 seconds) {' '}
      <button onClick={() => state.set(p => p + 1)}>Increment</button>
  </>
}