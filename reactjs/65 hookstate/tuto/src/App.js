import React from 'react';

import Compo1 from "./composants/compo1"
import Compo2 from "./composants/compo2"
import Compo3 from "./composants/compo3"

function App() {
  return (
    <div>
      <Compo1/>
      <Compo2/>
      <Compo3/>
    </div>
  );
}

export default App;
