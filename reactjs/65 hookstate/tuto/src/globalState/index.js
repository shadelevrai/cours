import React from 'react';
import { createState } from '@hookstate/core';

const globalState = createState(0);

export default globalState