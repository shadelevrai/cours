import React from "react";
import Toolbar from './Components/Toolbar'
import Nothing from './Components/Nothing'
import Change from './Components/Change'
import JediContext from './useContext/JediContext'

function App() {
  return (
    <JediContext.Provider value={"Luke"}>
      <Toolbar />
      <Nothing/>
      <Change/>
    </JediContext.Provider>
  );
}

export default App;
