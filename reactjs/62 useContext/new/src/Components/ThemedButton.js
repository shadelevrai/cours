import React, {useContext} from 'react'

import JediContext from '../useContext/JediContext'

export default function ThemeButton(){

    const value = useContext(JediContext);
    
    return <div>{value}, I am your Father.</div>;
}