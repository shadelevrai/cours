import React from 'react'
import ReactDOM from 'react-dom'
import HelloWorld from './HelloWorld'

class App extends React.Component {

  //http://cheng.logdown.com/posts/2016/03/26/683329

  //Le constructeur sert à mettre du pur code JS
  constructor(props){
    super(props)
    console.log('rer');
    console.log(this.props);
    function lala(){
      console.log('hiii');
    }
    lala();
  }
  

  componentWillUpdate(nextProps, nextState){
    console.log('pika');
  }

state = {
  var1 : 'lala',
  var2 : 'lolo'
}

change = () =>{
  this.setState({var1:'lili'})
}
  render() {
    return (
      <div>
        <HelloWorld name='Dan'/>
        <h2>{this.state.var1}</h2>
        <h3 onClick={e =>this.change()}>click me</h3>
      </div>
    )
  }
}

ReactDOM.render(<App /> ,
  document.getElementById('root')
);
