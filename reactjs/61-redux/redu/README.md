https://www.valentinog.com/blog/redux/
Un autre tuto à voir : https://riptutorial.com/fr/react-redux

## Etape 1

Il faut installer redux
npm i --save redux

## Etape 2 

Dans le dossier src, on peut créer un dossier js qui contiendra un dossier store

## Etape 3

Dans le dossier store, if faut créer un fichier index.js et mettre ce code :

import { createStore } from "redux";
import rootReducer from "../reducers/index";
const store = createStore(rootReducer);
export default store;

## Etape 4 

Il faut créer un dossier reducers dans le dossier js du src

## Etape 5

Dans le dossier reducers, créer un index.js et mettre ce code

// src/js/reducers/index.js
const initialState = {articles: []};
function rootReducer(state = initialState, action) {return state};
export default rootReducer;

## Etape 6

Il faut creer un dossier actions dans le dossier js du src

## Etape 7 

Dans le dossier actions, il faut créer un fichier index.js et mettre ce code

export function addArticle(payload) {
    return { type: "ADD_ARTICLE", payload }
  };

## Etape 8

Créer un dossier constants dans le dossier js du src

## Etape 9

Dans le dossier constants, créer un fichier action-types.js et mettre ce code

export const ADD_ARTICLE = "ADD_ARTICLE";

## Etape 10

Dans le fichier index du dossier actions, faire un remplacer le code présent avec ce code :

import { ADD_ARTICLE } from "../constants/action-types";
export function addArticle(payload) {
  return { type: ADD_ARTICLE, payload };
}

## Etape 11

Ouvrir le reducers/index.js et remplacer le code par celui-ci :

import { ADD_ARTICLE } from "../constants/action-types";
const initialState = {
  articles: []
};
function rootReducer(state = initialState, action) {
  if (action.type === ADD_ARTICLE) {
    state.articles.push(action.payload);
  }
  return state;
}
export default rootReducer;

## TIPS 

Si le fichier reducers/index.js devient trop gros, on peut le diviser : https://redux.js.org/api/combinereducers


## Les différents event

getState for accessing the current state of the application
dispatch for dispatching an action
subscribe for listening on state changes

## Etape 12

Créer un fichier index.js dans le dossier js

## Etape 13 

Dans le fichier index.js du dossier src, rajouter ce code :

import index from "./js/index"

## Etape 14 

lancer l'appli (npm start) et dans la console, faire store.getState()
On voir le state
Dans le state, on voit que 'article' est un tableau vide, c'est ce qui est dans le initialState.

## Etape 15 

Dans le js/index.js, rajouter ce code :

store.dispatch( addArticle({ title: 'React Redux Tutorial for Beginners', id: 1 }) )

'store' fait référence au state global
.dispatch est la fonction qui permet de changer le store. Cette fonction doit prendre en parametre le nom du type qu'on donne. C'est dans le fichier reducers/index.js qu'on déclare ce que chaque type va faire. En bref, ADD_ARTICLE va ajouter un element dans le state grâce à sa fonction qui est dans le rootReducer. L'objet présent dans le parametre de addArticle est ce qui va être ajouté au store

Pour info, si l'ajout dans le store ne sera pas un objet mais un tableau : 
Using concat(), slice(), and …spread for arrays
Using Object.assign() and …spread for objects



## REDUX EST PRET, MAINTENANT IL FAUT LE CONNECTER A REACT


## Etape 16

Ouvrir le fichier src/index.js et ajouter ces lignes dans les endroits adéquats : 

import { Provider } from "react-redux";
import store from "./js/store/index";

## Etape 17

Toujours dans le fichier src/index.js, il faut mettre les balises Provider autour du app : 


ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
, document.getElementById('root')
);

Le store sera maintenant accéssible dans tout l'application react

## Etape 18

Maintenant, on peut créer des composants. Dans le dossier src, créer un dossier Component. Et dans ce dossier, créer un fichier Test.js (ne pas oublier de l'appeler dans le App).
Créer un componsant List qui sera appelé par le componsant Test
Le composant List doit contenir ce code :

import React from "react";
import { connect } from "react-redux";
const mapStateToProps = state => {
  return { articles: state.articles };
};
const ConnectedList = ({ articles }) => (
  <ul className="list-group list-group-flush">
    {articles.map(el => (
      <li className="list-group-item" key={el.id}>
        {el.title}
      </li>
    ))}
  </ul>
);
const List = connect(mapStateToProps)(ConnectedList);
export default List;

Maintenant on sait comment initier et lire le global state dans un composant

## Etape 19

Dans le dossier Component, faire un fichier Form.js et copier le code suivant :

// src/js/components/Form.jsx
import React, { Component } from "react";
import { connect } from "react-redux";
import uuidv1 from "uuid";
import { addArticle } from "../actions/index";
function mapDispatchToProps(dispatch) {
  return {
    addArticle: article => dispatch(addArticle(article))
  };
}
class ConnectedForm extends Component {
  constructor() {
    super();
    this.state = {
      title: ""
    };
  }
  handleChange=event=> {
    this.setState({ [event.target.id]: event.target.value });
  }
  handleSubmit=event=> {
    event.preventDefault();
    const { title } = this.state;
    const id = uuidv1();
    this.props.addArticle({ title, id });
    this.setState({ title: "" });
  }
  render() {
    const { title } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            className="form-control"
            id="title"
            value={title}
            onChange={this.handleChange}
          />
        </div>
        <button type="submit" className="btn btn-success btn-lg">
          SAVE
        </button>
      </form>
    );
  }
}
const Form = connect(null, mapDispatchToProps)(ConnectedForm);
export default Form;

On peut maintenant modifier le store

## Etape 20



