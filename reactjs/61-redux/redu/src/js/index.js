import store from "../js/store/index";
import { addArticle } from "../js/actions/index";
window.store = store;
window.addArticle = addArticle;

//On ajout des données dans le store
// store.dispatch( addArticle({ title: 'React Redux Tutorial for Beginners', id: 1 }) )

//on console.log le store
console.log(store.getState())

//On écoute les changements du store
store.subscribe(()=>{
    console.log('changement')
})

//on ajoute encore des données au store
// store.dispatch(addArticle({title:'lalalal',id:4}))

