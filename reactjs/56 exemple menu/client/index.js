import React, { Component } from 'react';
import ReactDOM from 'react-dom';



class VacancySign extends Component{


  render(){
    const Greeter = ({ whom = 'lala'}) => (
      <button onClick={() => console.log(`Bonjour ${whom} !`)}>
        Vas-y, clique !
      </button>
    )

    return(
      <div>
        <Greeter />
      </div>
    )
  }
}

ReactDOM.render(
  <VacancySign />,
  document.querySelector('#root')
);
