import React from 'react'
import ReactDOM from 'react-dom'


class App extends React.Component {

  //la fonction est lancé au début

  /* Whenever this method is called, React has already rendered our component and put it into the DOM. Therefore, if there is any initialization you want to perform that relies on the DOM, do it here and now.

  State
  You can set the state with this.setState(). Whenever you do this, it will also trigger a re-render of the component.

  Use Cases
  You can use componentDidMount to fetch data from a server with AJAX calls. Also if you need to initialize anything that relies on the DOM, you can do this here (e.g. initializing third party libraries like D3). And last but not least, you can add event listeners inside componentDidMount. */
  componentDidMount() {
    console.log('japparais avant que le dom appraisse, donc avant qu\'un render ait été mis dans le EJS. Cette fonction est appelé qu\'une seule fois au début');
  }

  //la fonction est lancé quand un changement/destruction de composant est fait dans le reactDOM.render(). n peut donc annuler des fonctions

  /* Right before React unmounts and destroys our component, it invokes componentWillUnmount.

  State
  You can’t set state before unmounting the component.

  Use Cases
  Use this hook to perform clean up actions. This could be

  removing event listeners you added in componentDidMount (or elsewhere)
  cancelling active network requests
  invalidating timers
  cleaning up DOM elements that you created in componentDidMount */
  componentWillUnmount(){
    console.log('japparais quand le composant a été changé, je peux donc annuler des fonctions qui ont été lancé au début');
    
  }

  render() {
    return (
      <div>
        <h1>Yo</h1>
      </div>
    )
  }
}

ReactDOM.render(<App /> ,
  document.getElementById('root')
);

setTimeout(()=>{
  ReactDOM.render(
    <h2>je suis nouveau</h2>,
    document.getElementById('root')
  )
},4000)