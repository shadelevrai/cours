import React from 'react';
import ReactDOM from 'react-dom';

const users = [

    { id: 1, name: 'Alice' },
  
    { id: 2, name: 'Bob' },
  
    { id: 3, name: 'Claire' },
  
    { id: 4, name: 'David' },
  
  ]

class App extends React.Component{

  
    state = {
        txt: [
            {id: 'Lancer un Jeu'},
            {id: 'Voir la liste des jeux'},
            {id: 'Créer un jeu'}
        ]

    }
    
 
    
    render(){
        return(
            <div>
    
                    {this.state.txt.map( item =>
                        <p>{item.id}</p>
                    )}

                    <div className="userList">
                        {this.props.users.map((user) => (
                            <a href={`/users/${user.id}`}>{user.name}</a>
                        ))}
                    </div>
         
                
            </div>
        )
    }
    
}

ReactDOM.render(
  <App />,
  document.querySelector('#root')
);
