const express = require('express');
const app = express();

// var routes = require('./routes/index');

// app.use('/', routes);

app.get('/', function (req,res){
  res.render('layout/master')
})

app.set('port', process.env.PORT || 8080);

//view engine setup
app.set('views', __dirname + '/views');
app.set('view engine','jsx');
app.engine('jsx', require('express-react-views').createEngine());

var server = app.listen(app.get('port'), function () {
  console.log('app started');
});