import React from 'react'
import ReactDOM from 'react-dom'
import HelloWorld from './HelloWorld'

class App extends React.Component {

  //après que le DOM ait été généré
  componentDidMount(){
    console.log('lala');
    
  }

  render() {
    return (
      <div>
        <HelloWorld name='Dan'/>
      </div>
    )
  }
}

ReactDOM.render(<App /> ,
  document.getElementById('root')
);
