import React from 'react'

import Title from './Title'
import GenerateName from './GenerateName'

export default class App extends React.Component {
  render(){
    return(
      <div>
        <Title />
        <GenerateName />
      </div>
    )
  }
}