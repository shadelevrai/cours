import React, { Component } from 'react';
import ReactDOM from 'react-dom';

let lolo = true;
let nombre = 23;
let azerty = ['popo','papa']

class VacancySign extends Component{

  constructor(props){
    super(props)
    this.state = {
      lala : true
    }
  }

  render(){
  

    return(
      <div>
        {/* l'équivalent de if(this.state.lala == true){} */}
        <div>{this.state.lala && <p>Salut tu es VIP</p>}</div>
        <div>{lolo && <p>Salut tu es encore VIP</p>}</div>
        {/* l'équivalent de if(nombre > 22){} */}
        <div>{nombre > 22 && <p>Salut tu es encore encore VIP</p>}</div>
        {/* si le tableau possède au moins 1 élément, ça s'affiche */}
        <div>{azerty.length && <p>Le tableau s'affiche</p>}</div>
      </div>
    )
  }
}

ReactDOM.render(
  <VacancySign />,
  document.querySelector('#root')
);
