import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class VacancySign extends Component{
  render(){

    return(
      <div>
        <Router>
          <div>
          <Link to='/elem1'><p>Element1</p></Link>
          <Link to='/elem2'><p>Element2</p></Link>
          <Link to='/elem3'><p>Element3</p></Link>
          <Route path='/elem1' component={elem1}/>
          <Route path='/elem2' component={elem2}/>
          <Route path='/elem3' component={elem3}/>
          </div>
        </Router>
      </div>

    )
  }
}

const elem1 = () => (
  <div>
    <p>Salut elem1</p>
  </div>
)

const elem2 = () => (
  <div>
    <p>Salut elem2</p>
  </div>
)

const elem3 = ({match}) => (
  <div>
    <p>Salut elem3</p>
    <div>
      <Link to={`${match.url}/lala1`}>lala1</Link>
    </div>
    <div>
      <Link to={`${match.url}/lala2`}>lala2</Link>
    </div>
    <div>
      <Link to={`${match.url}/lala3`}>lala2</Link>
    </div>
    <Route path={`${match.url}/:topicId`} component={Topic} />
    <Route exact path={match.url} render={() => <h3>Please select a topic.</h3>}/>
  </div>
)

const Topic = ({ match }) => (
  <div>
    <h3>{match.params.topicId}</h3>
  </div>
);



ReactDOM.render(
  <VacancySign />,
  document.querySelector('#root')
);
