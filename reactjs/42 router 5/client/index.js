import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class VacancySign extends Component{
  render(){

    return(
      <div>
        <Router>
          <div>
          <div>
          <Link to='/'>hello</Link>
          </div>
          <div>
          <Link to='/hello'>dsfdsf</Link>
          </div>
          {/* il faut mettre exact sur un lien '/' sinon elem1 sera toujours là quand on clique sur autre chose */}
          <Route exact path='/' component={elem1}/>
          <Route path='/hello' component={elem2}/>
          </div>
        </Router>
      </div>

    )
  }
}

const elem1 = () => (
  <div>
    <p>Salut elem1</p>
  </div>
)

const elem2 = () => (
  <div>
    <p>Salut elem2</p>
  </div>
)

ReactDOM.render(
  <VacancySign />,
  document.querySelector('#root')
);
