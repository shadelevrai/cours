import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap'

export default class Menu1 extends Component{

  

    render(){
        return(
            <div>
                <Container>
                    <Row>
                        <Col sm={{size:4,offset:4}}>
                            <div className="cadre">
                                <p className='txt'>Lancer un jeu</p>
                            </div>                            
                        </Col>
                    </Row>
                </Container>
                <Container>
                    <Row>
                        <Col sm={{size:4,offset:4}}>
                            <div className="cadre">
                                <p className='txt'>Voir les jeux (désactivé)</p>
                            </div>                            
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}