import { getByTestId, render, screen } from "@testing-library/react";
import Compo1 from "./index";

describe("test du compo1", () => {
  render(<Compo1 />);
  const generalDiv = screen.getByTestId("generalDiv");
  const title = screen.getByTestId("title");
  const inputElem = screen.getByTestId("element-to-focus");

  test("Présence d'une chaine de charactère précise", () => {
    const textElementTitle = screen.getByText(/J'ai un titre/i);
    expect(textElementTitle).toBeInTheDocument();
  });

  test("Non présence d'une chaine de charactère précise", () => {
    expect(() => getByText(/Je suis pas là/i)).toThrow();
  });

  test("Présence d'un element dans la div générale", () => {
    expect(generalDiv).not.toBeEmptyDOMElement();
  });

  test("Présence d'une classe précise dans un element", () => {
    expect(title).toHaveClass("title");
  });

  test("Présence d'un id spécifique dans un element avec un id spécifique", () => {
    expect(generalDiv).toContainElement(title);
  });

  // test("Présence d'un element",()=>{
  //   expect(generalDiv).toContainHTML("<h1 data-testid='title' id='title' className='title'>J'ai un titre</h1>")
  // })

  test("Présence d'un element focusable", () => {
    inputElem.focus();
    expect(inputElem).toHaveFocus();
  });
});
