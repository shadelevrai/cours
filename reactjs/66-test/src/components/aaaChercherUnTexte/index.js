import React from "react";

const Compo1 = () => {
  return (
    <div data-testid="generalDiv">
      <h1 data-testid="title" id="title" className="title">
        J'ai un titre
      </h1>
      <input type="text" data-testid="element-to-focus" />

    </div>
  );
};

export default Compo1;
