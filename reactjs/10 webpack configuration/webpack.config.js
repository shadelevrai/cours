let path = require('path');

module.exports = {
  watch:true,
  entry:'./scripts/js/index.js',
  output:{
    path:path.resolve('./dist'),
    filename:'bundle.js'
  }
}