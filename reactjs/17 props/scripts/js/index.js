import React from 'react'
import ReactDOM from 'react-dom'
import HelloWorld from './HelloWorld'

class App extends React.Component {

  //Les props, ce sont les properties que l'ont peut passer à un composant (parents ?).
  render() {
    return (
      <div>
        <HelloWorld name='Dan'/>
        <h2></h2>
      </div>
    )
  }
}
//Ici, on passe donc l'attribut "name" au composant HelloWorld, avec la valeur "Dan". On récupère ainsi cette valeur dans le composant, via un accès par "this.props.name". Simple non ?

//Si vous avez bien suivi la logique, vous aurez remarqué que le passage d'information ne se fait que dans un sens, du composant parent vers le composant enfant. JAMAIS dans l'autre sens. Les props sont ce que l'on appelle "immutable", en gros, en lecture seule.

ReactDOM.render(<App /> ,
  document.getElementById('root')
);
