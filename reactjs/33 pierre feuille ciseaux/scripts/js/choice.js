import React from 'react'

// const style = {
//   title:{
//     cursor:'pointer',
//     left:''
//   }
// }

export default class Choice extends React.Component {

  state = {
    img : {
      cursor :'pointer',
    }
  }

  launch1 = () => {
    console.log('lala');  
    this.setState({img:{marginLeft:'200px',cursor:'pointer'}})
  }

    render() {
      return (
      <div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-offset-3 col-xs-3">
              <img src="../public/img/paper.jpg" alt="" style={this.state.img} onClick={e=>this.launch1()}/>
            </div>
            <div className="col-xs-offset-3 col-xs-3">
              <img src="../public/img/paper.jpg" alt=""/>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-offset-3 col-xs-3">
              <img src="../public/img/scissors.jpg" alt=""/>
            </div>
            <div className="col-xs-offset-3 col-xs-3">
              <img src="../public/img/scissors.jpg" alt=""/>
            </div>
          </div>
        </div>
        <div className="container-fluid">
          <div className="row">
            <div className="col-xs-offset-3 col-xs-3">
              <img src="../public/img/stone.jpg" alt=""/>
            </div>
            <div className="col-xs-offset-3 col-xs-3">
              <img src="../public/img/stone.jpg" alt=""/>
            </div>
          </div>
        </div>
      </div>
      )
    }
}