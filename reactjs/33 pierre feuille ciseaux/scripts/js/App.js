import React from 'react'

import Title from './Title'
import Choice from './Choice'

export default class App extends React.Component {
  render(){
    return(
      <div>
        <Title />
        <Choice />
      </div>
    )
  }
}