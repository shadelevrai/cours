import React from 'react';

const style = {
        title : {
            textAlign:'center',
            color:'red'
        }
}

export default class Title extends React.Component {

    render() {
        return(
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-offset-3 col-xs-6">
                        <h1 style={style.title}>Papier caillou ciseaux</h1>
                    </div>
                </div>
            </div>
        )
    }
}