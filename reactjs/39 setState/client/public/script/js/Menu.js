import React from 'react'
import { Container, Row, Col } from 'reactstrap'

export default class Menu extends React.Component{

  

    render(){
        return(
            <div>
                <Container>
                    <Row>
                        <Col sm={{size:4,offset:4}}>
                            <div className="cadre">
                                <p className='txt'>{this.props.txt}</p>
                            </div>                            
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}