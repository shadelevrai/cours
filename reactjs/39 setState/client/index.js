import React from 'react';
import ReactDOM from 'react-dom';

class HelloUser extends React.Component {

    //   Il faut avant tout créer la state.
    constructor(props) {
      super(props)
      this.state = {
        username: ''
      }
  
      this.handleChange = this.handleChange.bind(this)
    }

    //La methode handleChange est appelé à chaque fois qu'il y a un changement dans le input
    handleChange (e) {
      this.setState({
        username: e.target.value
      })
    }
    render() {
      return (
        <div>
          Hello {this.state.username} <br />
          Change Name:
          <input
            type="text"
            value={this.state.username}
            onChange={this.handleChange}
            // onChange capture le changement dans le input
          />
        </div>
      )
    }
  }

ReactDOM.render(
  <HelloUser  />,
  document.querySelector('#root')
);
