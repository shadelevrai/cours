import React from 'react'
import { sampleText } from './sampleText'
// import './index.css'

export default class indexComponent extends React.Component{
  
  constructor(props) {
    super(props);
    //affiche les infos comme les parametre du serveur ou les variables envoyé par le serveur
    console.log(props);
    console.log('----------------------');
    console.log(this.props);
    console.log('----------------------');
    console.log(this.props.name);
    console.log('----------------------');
    console.log(this.props.settings);
    console.log('----------------------');
    console.log(this.props.settings.port);
    
    this.state = {
      text: sampleText,
      text2:'lalala'
    };
  }

  render(){
    return (
      <html>
      <head>
      </head>
      <body>
        <div>
          <div>
          <div>
            <p>{this.state.text}</p>
            <textarea name="" id="" cols="30" rows="10" defaultValue={this.state.text2}></textarea>
            <p>{this.props.name}</p>{/*Pn peut aussi faire comme ça pour afficher les variables envoyés pas le serveur*/}
            </div>
          </div>
        </div>
      </body>
      </html>
    )
  }
}