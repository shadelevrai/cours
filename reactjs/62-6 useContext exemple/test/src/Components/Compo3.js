import React, {useContext} from 'react'
import videoGame from "../useContext/videoGame"

export default function Compo3(){

  const { gameTitle, setGameTitle } = useContext(videoGame)

  return(
  <div>
    {gameTitle.name}
    <button onClick={e=>setGameTitle({name:"Mario"})}>Changer le nom du jeu</button>
  </div>
  )
}