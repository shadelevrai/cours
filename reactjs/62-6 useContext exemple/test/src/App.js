import React, { useState } from 'react';
import VideoGame from './useContext/videoGame'
import Compo1 from "./Components/Compo1"


function App() {

  const [gameTitle, setGameTitle] = useState({name:"Persona 5"})

  return (
    <VideoGame.Provider value={{gameTitle, setGameTitle}}>
      <Compo1/>
    </VideoGame.Provider>
  );
}

export default App;
