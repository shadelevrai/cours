import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MyComponent from './MyComponent'
import OtherComponent from './OtherComponent'



class VacancySign extends Component{

  constructor(props){
    super(props)
    this.state = {
      user:'shade'
    }
  }
  render(){

    let showMyComponent = false;

    return(
      <div>
        {showMyComponent ? <MyComponent/> : <OtherComponent/>}

          {this.state.user ? (<p>salut</p>) : (<p>t'es qui ?</p>) }
      </div>
    )
  }
}

ReactDOM.render(
  <VacancySign hasvacancy={true}  />,
  document.querySelector('#root')
);
