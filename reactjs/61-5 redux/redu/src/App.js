import React, { useEffect, useState } from 'react';
import { createStore } from 'redux'


function App() {

  let store = createStore(counter)
  let [numberForHtml, setNumberForHtml] = useState(0)

  function counter(state = 2, action) {
  
    switch (action.type) {
      case 'INCREMENT':
        return state + 1
      case 'DECREMENT':
        return state - 1
      default:
        return state
    }
  }
  
  function incr(){
    store.dispatch({ type: 'INCREMENT'})
  }
  
  function decr(){
    store.dispatch({ type: 'DECREMENT' })
  }
  
  
  store.subscribe(() => {
    console.log('je suis dans le subscribe')
    console.log(store.getState())
    setNumberForHtml(store.getState())
  })

  return (
    <div className="App">
      {console.log('je suis le console log du HTML de App')}
      <button onClick={incr}>Incrémenter</button>
      <button onClick={decr}>Decrémenter</button>
      <br/>
      {numberForHtml}
    </div>
  );
}

export default App;
