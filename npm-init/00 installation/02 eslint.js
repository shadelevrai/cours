// ESlint permet de repérer les erreurs ou les mauvaises méthode de codage dans visual code

//https://eslint.org/docs/user-guide/getting-started

// Exemple de fichier config : 
// https://gist.github.com/nkbt/9efd4facb391edbf8048

// On peut aussi checker la sécurité : https://www.npmjs.com/package/eslint-plugin-security
