// One Paragraph Explainer
// Rate limiting should be implemented in your application to protect a Node.js application from being overwhelmed by too many requests at the same time. Rate limiting is a task best performed with a service designed for this task, such as nginx, however it is also possible with application middleware such as express-rate-limiter.

// Code example: Express rate limiting middleware for certain routes
// Using express-rate-limiter npm package

var RateLimit = require('express-rate-limit');
// important if behind a proxy to ensure client IP is passed to req.ip
app.enable('trust proxy'); 
 
var apiLimiter = new RateLimit({
  windowMs: 15*60*1000, // 15 minutes
  max: 100,
  delayMs: 0 // disabled
});
 
// only apply to requests that begin with /user/
app.use('/user/', apiLimiter);