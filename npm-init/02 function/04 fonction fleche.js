const myFn = (x) => {
    return x + 1;
};
// ===
const myFn = (x) => x + 1;
// ===
const myFn = x => x + 1;
// ===
const myFn = x => (x + 1);