
var varTest;

//une façon de faire
premier = (p1) => {
    return new Promise(()=>{
        console.log(`le ${p1} est exécuté et voici la variable varTest : ${varTest}`);
    })
}
deuxieme = (p2) => {
    console.log(`le ${p2} est exécuté et on va mettre quelque chose dans la variable varTest`);
    varTest = 42;
}
premier("preums").then(deuxieme("deuz")).then(console.log(`troiz et on lit la vartest : ${varTest}`));    

//Une autre façon de faire, mais il ne faut pas oublier de require async et await
var foo = async (function() {
    var resultA = await (firstAsyncCall());
    var resultB = await (secondAsyncCallUsing(resultA));
    var resultC = await (thirdAsyncCallUsing(resultB));
    return doSomethingWith(resultC);
});
foo()
//Plusieurs façons de faire, voir le dossier async

// On peut aussi capturer l'erreur de la promise : https://github.com/i0natan/nodebestpractices/blob/master/sections/errorhandling/catchunhandledpromiserejection.md