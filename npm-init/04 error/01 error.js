try {
    //Throw déclenche l'érreur
    throw new Error("Ouups !");
} catch (e) {
    //catch s'affiche quand une erreur est détecté.
    //e.name est le nom de l'erreur : Error
    //e.message est le message en paramètre : "Oups !"
    console.log(e.name + ": " + e.message);
}
/********************************************************************* */

// let machin = {
//     truc : function(){
//         return undefined
//     }
// }

// try {
//     machin.truc();
// } catch (e) {
//     if (e instanceof EvalError) {
//         console.log(e.name + ": " + e.message);
//     } else if (e instanceof RangeError) {
//         console.log(e.name + ": " + e.message);
//     }
//     // ... etc
// }

/******************************************************************** */

//Si on lance cette fonction alors que la variable qu'elle doit affciher n'existe pas, alors il ne se passera rien.
lala=()=>{
    console.log(variableInexistante);
}

//Mais là, on aura un message d'erreur comme quoi la variable existe pas.
try {
    console.log(variableInexistante);
}
catch(err) {
    console.log(err.message);
}

/******************************************************************* */

myFunction=(p1)=> {
    try { 
        //throw sera appelé avec le 'err' de la fonction 'catch' et retournera sa chaine de caractère
        if(p1 == "")  throw "empty";
        if(isNaN(p1)) throw "not a number";
        if(p1 < 5)    throw "too low";
        if(p1 > 10)   throw "too high";
    }
    catch(err) {
        //Si p1 est vide, il affichera 'input is empty'
        //si p1 n'est pas un nombre, il affichera 'input is not a number'
        //si p1 est moins que 5, il affichera 'input is too low'
        //si p1 est plus que 10, il affichera 'input is too high'
        console.log(`input is ${err}`);
    }
}

myFunction();