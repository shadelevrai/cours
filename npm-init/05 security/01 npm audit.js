// Avec la sortie de la version 6 du CLI et le rachat de la Node Security Platform de Lift Security (voir actualité précédente), l'équipe npm met l'accent sur la sécurité.

// Ces efforts s'orientent dans deux directions, l'audit des failles de sécurité au travers de la NSP via la nouvelle commande npm audit et la signature électronique des paquets du registre via PGP. Si ce dernier point nécessite encore beaucoup de travail, la commande npm audit, elle, devient fonctionnelle.

// https://www.developpez.com/actu/203081/npm-la-version-6-0-1-du-gestionnaire-de-paquets-officiel-de-Node-js-passe-en-latest-la-commande-npm-audit-est-desormais-fonctionnelle/