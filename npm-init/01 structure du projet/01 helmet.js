// Utilisez Helmet
// Helmet vous aide à protéger votre application de certaines des vulnérabilités bien connues du Web en configurant de manière appropriée des en-têtes HTTP.

// Helmet n’est actuellement qu’une collection de neuf fonctions middleware plus petites qui définissent des en-têtes HTTP liés à la sécurité :

// csp définit l’en-tête Content-Security-Policy pour la protection contre les attaques de type cross-site scripting et autres injections intersites.
// hidePoweredBy supprime l’en-tête X-Powered-By.
// hpkp ajoute des en-têtes Public Key Pinning (épinglage de clé publique) pour la protection contre les attaques d’intercepteur avec de faux certificats.
// hsts définit l’en-tête Strict-Transport-Security qui imposer des connexions (HTTP sur SSL/TLS) sécurisées au serveur.
// ieNoOpen définit X-Download-Options pour IE8+.
// noCache définit des en-têtes Cache-Control et Pragma pour désactiver la mise en cache côté client.
// noSniff définit X-Content-Type-Options pour protéger les navigateurs du reniflage du code MIME d’une réponse à partir du type de contenu déclaré.
// frameguard définit l’en-tête X-Frame-Options pour fournir une protection clickjacking.
// xssFilter définit X-XSS-Protection afin d’activer le filtre de script intersites (XSS) dans les navigateurs Web les plus récents.

// Installez Helmet comme n’importe quel autre module :


// $ npm install --save helmet

// Puis, pour l’utiliser dans votre code :


// ...
var helmet = require('helmet');
app.use(helmet());