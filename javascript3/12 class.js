class Test {
    constructor(name, year){
        this.name = name
        this.year = year
    }
    age(){
        let date = new Date();
        return date.getFullYear() - this.year;
    }
}

const lala = new Test("jean",2015)
console.log("🚀 ~ file: 12 class.js:9 ~ lala", lala)

console.log("🚀 ~ file: 12 class.js:7 ~ Test ~ age ~ age", lala.age("jeanne",2000))