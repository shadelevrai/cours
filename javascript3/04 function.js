/**
 * Comme les var, la déclaration des fonctions sont aussi en priorité dans l'exectution du code, mais pas les expressions de fonction.
 * Déclaration de fonction = une simple fonction
 * Expression de fonction = une fonction stocké dans une variable
 * 
 */

const obj = { "lala": 1, "lolo": 2 }

function mama(titi) {
    titi.lala = 3
}

mama(obj)

console.log(obj)