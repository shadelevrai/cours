/**
 * 
 * Polymorphisme
 * Classe
 * S-O-L-I-D
 * 
 */

 const obj = {
    log: ['a', 'b', 'c'],
    get latest() {
      return this.log[this.log.length - 1];
    }
  };
  
  console.log(obj.latest);
  // expected output: "c"
  
  console.log(obj.constructor === Object)

  const mama = []
  console.log("🚀 ~ file: 00 a voir.js:22 ~ mama", mama.constructor)

  