var x = { name: "John" };
var y = x;
y.name =  "Codeur";
console.log(x.name); // Codeur
console.log(y.name); // Codeur

/**
 * 
 * Dans la mémoire, ce n’est pas l’objet lui-même qui est stocké mais un pointeur, c’est-à-dire une adresse qui pointe vers un autre endroit dans la mémoire.
 * Ça va nous afficher 2 fois  « Codeur »  car si on change une propriété d’un objet, toutes les variables pointant vers cet objet vont être aussi modifiées. C’est relativement simple et c’est la base de JavaScript !
 * 
 * Pour copier les données d'un objet sans sa place dans la mémoire, on utlise Ojject.assign() 
 */

 const target = { a: 1, b: 2 };
 const source = { b: 4, c: 5 };

 
 const test = Object.assign({},target)
 console.log("🚀 ~ file: 07 object.js:27 ~ test", test)
 test.mama="lala"
 console.log("🚀 ~ file: 07 object.js:29 ~ test", test)
 console.log("🚀 ~ file: 07 object.js:17 ~ target", target)


 /**
  * 
  * Object.assign sert aussi à faire fusionner deux objets en un objet
  */

   const returnedTarget = Object.assign(target, source);
 
  console.log(target);
  // expected output: Object { a: 1, b: 4, c: 5 }
 
  console.log(returnedTarget === target);
  // expected output: true


  const lili = {a:1,b:2}
  const mimi = {lili,c:3}
  const gigi = Object.assign({},mimi)
  console.log("🚀 ~ file: 07 object.js:43 ~ gigi", gigi)
  const popo = Object.values(lili)
  console.log("🚀 ~ file: 07 object.js:45 ~ popo", popo)

  const tyty = {
    name: "John",
    age: function () {return 30;}
  }
  
  console.log(JSON.stringify(tyty))
  
  Object.defineProperty(tyty,"lala",{value:"lolo"})