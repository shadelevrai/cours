/**
 * 
 * L'instruction var (pour variable) permet de déclarer une variable et éventuellement d'initialiser sa valeur.
 */

var x = 1;

if (x === 1) {
    var x = 2;

    console.log(x);
    // expected output: 2
}

console.log(x);
// expected output: 2


/* La valeur initiale à affecter à la variable, cela peut être n'importe quelle expression valide. S'il n'y a aucune valeur fournie, la variable vaudra undefined.
*  
* La portée d'une variable déclarée avec var est le contexte d'exécution courant, c'est-à-dire : la fonction qui contient la déclaration ou le contexte global si la variable est déclarée en dehors de toute fonction.
* 
* Les déclarations de variables sont traitées avant que le code soit exécuté, quel que soit leur emplacement dans le code.
* 
* La remontée de variables (hoisting) : Les déclarations de variables (et les déclarations en général) sont traitées avant que n'importe quel autre code soit exécuté. Ainsi, déclarer une variable n'importe où dans le code équivaut à la déclarer au début de son contexte d'exécution. Cela signifie qu'une variable peut également apparaître dans le code avant d'avoir été déclarée. Ce comportement est appelé « remontée » (hoisting en anglais) car la déclaration de la variable est « remontée » au début de la fonction courante ou du contexte global.

Il est important de noter que la remontée des variables affecte uniquement la déclaration et pas l'initialisation de la valeur. La valeur sera affectée lorsque le moteur accèdera à l'instruction d'affectation. Par exemple :
*/

function faireQuelqueChose() {
    console.log(truc); // undefined
    var truc = 111;
    console.log(truc); // 111
}

// Correspond en fait à :
function faireQuelqueChose() {
    var truc;
    console.log(truc); // undefined
    truc = 111;
    console.log(truc); // 111
}


/**
 * 
 * Les variables global sont des propriété de l'objet "global" qui sont accessible sur l'objet "window" : window.maVariable
 * 
 */



// console.log("🚀 ~ file: 02 variables.js:54 ~ mama", mama)
// let mama = 4


let reaction = 'yikes';
reaction[0] = 'l';
console.log(reaction);
