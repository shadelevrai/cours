/**
 * 
 * Le const est comme le var et let, sauf qu'on ne peux pas la ré-assigner.
 * Mais si c'est un objet ou un tableau, on peut modifier son contenant
 * 
 */