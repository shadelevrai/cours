/**
 * 
 * variable = function scope mais pas block scope
 * let = block scope et functional scope

 */

var age = 100;
if (age > 12) {
    var dogYears = age * 7;
    let dogYears2 = age * 7
}


console.log("🚀 ~ file: 03 let.js:6 ~ dogYears", dogYears)
console.log("🚀 ~ file: 03 let.js:5 ~ dogYears2", dogYears2) // error