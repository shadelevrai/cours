var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 48.8656, lng: 2.3789893},
    zoom: 14
  });
}