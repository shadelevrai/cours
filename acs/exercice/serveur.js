var express = require('express');
var app = express();

app.use(express.static(__dirname + '/html')); //Dire qu'on va utiliser ce que contient le dossier html

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './html'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/',function(req,res){
  res.render('about'); // on envoit le fichier index.jade du dossier html
});
app.get('/services',function(req,res){
    res.render('services');
});
app.get('/contact',function(req,res){
    res.render('contact');
});

//ERROR

app.get('/404', function(req, res, next){
  next();
});

app.use(function(req,res,next){
    res.status(404); 
    if (req.accepts('html')) {
    res.render('404',{ titrePage: 'Erreur', titreH1: 'Des erreurs'});
    return;
  }
});

//Mise en place du serveur qui écoute sur le port indiqué

  app.listen(8888, function() {
    console.log('Le serveur est disponible sur le port 8888');
  });
