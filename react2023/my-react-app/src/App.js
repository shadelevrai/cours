import './App.css';
import useFetch from './hooks/useFetch';
import UseCallBackTest from './Components/useCallback';
import Useeffect from "./Components/useEffect"
import Uselayouteffect from './Components/useLayoutEffect';
import Useref from './Components/useRef';

function App() {

  
    // const mama = useFetch('mama')
    // console.log("🚀 ~ file: App.js:9 ~ useEffect ~ lala", mama)

 
  return (
    <div className="App">
      {/* <Useeffect/> */}
     {/* <UseCallBackTest/> */}
     {/* <Uselayouteffect/> */}
     <Useref/>
    </div>
  );
}

export default App;
