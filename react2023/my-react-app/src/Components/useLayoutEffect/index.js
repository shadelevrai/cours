import { useEffect, useState, useLayoutEffect } from "react"

const Uselayouteffect = () => {
    useEffect(() => {
        console.log("UseEffect")
    }, [])
    useLayoutEffect(()=>{
        console.log("useLayout")
    },[])

    return (
        <button>up</button>
    )
}

export default Uselayouteffect