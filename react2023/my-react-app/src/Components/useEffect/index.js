import { useEffect, useState } from "react"

const Useeffect = () => {
    const [kiki, setKiki] = useState(null)
    const [kaka, setKaka] = useState(null)
    useEffect(() => {
        console.log("lalal")
        return () => console.log("mama")
    }, [kiki,kaka])

    return (<>
        <button onClick={e=>setKiki("papa")}>up</button>
        <button onClick={e=>setKaka("papa")}>up</button>
        </>
    )
}

export default Useeffect