import { useState, useEffect, useRef } from "react";

const Useref=()=> {
  const [inputValue, setInputValue] = useState("");
  const count = useRef(0);
//   const inputRef = useRef();

  useEffect(() => {
    count.current = count.current + 1;
  });


//   // Lors du clic sur le bouton, la fonction 'focus' de l'input sera appelée
//   const onButtonClick = () => {
//     // On utilise la propriété 'current' de la référence pour y accéder
//     inputRef.current.focus();
//   };

  return (
    <>
      <input
        type="text"
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
      />
      <h1>Render Count: {count.current}</h1>
      {/* <input ref={inputRef} type="text" />
      <button onClick={onButtonClick}>Focus</button> */}
    </>
  );
}

export default Useref