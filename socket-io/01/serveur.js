var express = require('express');
var app = express();


app.use(express.static(__dirname + '/fichiers')); //Dire qu'on va utiliser ce que contient le dossier html

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './fichiers'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/', function (req, res) {
    res.render('accueil'); // on envoit le fichier index.jade du dossier html3
});



//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8080, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
});

var io = require('socket.io').listen(server);

// Quand un client se connecte, on le note dans la console
io.sockets.on('connection', function (socket) {
    console.log('Un client est connecté !');
});


io.sockets.on('connection', function (socket) {
    socket.emit('message', 'Vous êtes bien connecté ! Ce message est juste pur la page qui a fait l\'appel');
    socket.broadcast.emit('message', 'ce message est pour toutes les pages, sauf celui qui l\envoi')
    socket.emit('message2', 'C\'est cool non ?'); //on envoit la 'variable' message et message 2 au client
        socket.on('message', function (message) { // on écoute la 'variable' message et on active la fonction quand on la reçois
            console.log('Un client me parle ! Il me dit : ' + message);
        })
});