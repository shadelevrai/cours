var socket = io.connect('http://localhost:8080');

//On écoute le serveur et on récupère les 'variable' message et message2

socket.on('message', function (message) {
    alert('Le serveur a un message pour vous : ' + message);
})
socket.on('message2', function (message2) {
    alert('Le serveur a encore un message pour vous: ' + message2);
})


$(function () {
    $('body').prepend('<div class="azerty2"></div>');
    $('.azerty2').css({
        height: '50px',
        width: '100px',
        backgroundColor: 'grey',
    }).text('Cliquez ici');

    var incrementation = 1;

    $('.azerty2').click(function () {
        $('.azerty3').remove();
        $('body').prepend('<div class="azerty3"></div>');
        $('.azerty3').css({
            position: 'absolute',
            left: '150px',
            height: '50px',
            width: '250px',
            backgroundColor: 'red',
        }).text(incrementation);
        incrementation++;
    });

    $('#poke').click(function () {
        socket.emit('message', 'Salut serveur, ça va ?');
    })
});