const express = require('express');
const app = express();
const PORT = 8889;

const server = require('http').createServer(app);
const io = require('socket.io')(server);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

io.on('connect', (socket) => {
    socket.broadcast.emit("message2", "ce message est pour toutes les pages, sauf celui qui l'envoi")
    socket.on('message3', function (message) { // on écoute la 'variable' message et on active la fonction quand on la reçois
        console.log('Un client me parle ! Il me dit : ' + message);
    })
    socket.on('join_room', room => {
        console.log("room", room)
        socket.join(room);
        setTimeout(() => {
            socket.leave(room)
        }, 2000);
    });
    client.on('send', function(room) {
        console.log(`Ce message sera envoyé : ${room}`);
        io.sockets.in(room).emit('message', room);
    });
});


server.listen(3000);

// const io = require('socket.io')();
// io.on('connection', client => { console.log('lala') });
// io.listen(3000);

app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}/`));