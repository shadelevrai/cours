const express = require('express');
const hostname = 'localhost';
const port = 8080;
const mongoose = require('mongoose');
const app = express();
const bodyParser = require("body-parser");

const url = 'mongodb://cernnunos:Zmfgvknrzs11@ds141209.mlab.com:41209/jeux';

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

var db = mongoose.connection;

mongoose.connect(url, {
    useNewUrlParser: true
});

db.on('error', console.error.bind(console, 'Erreur lors de la connexion'));
db.once('open', function () {
    console.log("Connexion à la base : OK");
});

// CODE
// Pour modéliser les données, le framework mongoose utilise des "schémas" ; nous créons donc un modèle de données :
var piscineSchema = mongoose.Schema({
    nom: String,
    adresse: String,
    tel: String,
    description: String
});

var Piscine = mongoose.model('Piscine', piscineSchema);
//SUITE DU CODE

// Je vous rappelle notre route (/piscines).  
app.get('/piscines', function (req, res) {
    Piscine.find(function (err, piscines) {
        if (err) {
            res.send(err);
        };
        res.json(piscines);
    });
});

app.get('/piscines-post', function (req, res) {
    // Nous utilisons le schéma Piscine
    var piscine = new Piscine();
    // Nous récupérons les données reçues pour les ajouter à l'objet Piscine
    piscine.nom = 'nom';
    piscine.adresse = 'adresse';
    piscine.tel = 'tel';
    piscine.description = 'description';
    //piscine.wrong n'est pas de le schema, il sera pas envoyé mais le reste, oui.
    piscine.wrong = 'wrong';
    //Nous stockons l'objet en base
    piscine.save(function (err) {
        if (err) {
            res.send(err);
        }
        res.send({
            message: 'Bravo, la piscine est maintenant stockée en base de données'
        });
    })
})

app.listen(port, () =>
    console.log(`Serveur lancé sur le port ${port}`)
)