import { additional, sameObject, sameString, returnTrue, awaitString, throwError, throwError2, getAPI } from "./index"

describe("index libs", () => {
    it("additional", () => {
        expect(additional(4, 5)).toEqual(9)
        expect(additional(14, 5)).toEqual(19)
        expect(additional(14, 5)).not.toEqual(5)
    })
    it("sameObject", () => {
        expect(sameObject({ mama: "mama" })).toMatchObject({ mama: "mama" })
    })
    it("sameString", () => {
        expect(sameString("lala")).toMatch("lala")
    })
    it("returnTrue", () => {
        expect(returnTrue(15)).toBeTruthy()
        expect(returnTrue(4)).toBeFalsy()
    })
    it("awaitString", async () => {
        const data = await awaitString("papa")
        expect(data).toMatch("papa")
        awaitString("papa").then(data => {
            expect(data).toBe('papa');
        });
    })
    it("throwError", () => {
        expect(() => throwError("lol")).toThrow("naaan")
        expect(() => throwError("lol")).toThrow(Error)
    })
    it("throwError2", () => {
        // expect.assertions(1);
        expect(throwError2("azerty")).toBe("naaan");
    })

    it("fetch error", async() => {
        const data = await getAPI("lien-qui-existe-pas")
        expect(data).toMatchObject({erreur:{message:"Mauvais lien ou serveur non atteignable"}})
        // expect.assertions(1)
        // /*return */mama("http://dsijfsf.sdo").catch(e=>expect(e).toMatch("error"))
        // console.log("mamaaaaaa ",await mama("http://dsijfsf.sdo"))
    })

})