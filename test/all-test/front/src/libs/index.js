function additional(a, b) {
    return a + b
}

function sameObject(obj) {
    return obj
}

function sameString(string) {
    return string
}

function returnTrue(a) {
    if (a > 10) {
        return true
    } else {
        return false
    }
}

function throwError(param1) {
    if (param1 === "lol") throw new Error("naaan")
}

function throwError2(param1) {
    try {
        if (param1 === "azerty") throw new Error("naaan")
    } catch (error) {
        return error.message
    }
}

async function awaitString(string) {
    return new Promise(resolve => {
        resolve(string)
    })
}

async function mama(link){
    try {
        fetch(link).then(res=>console.log(res))
    } catch (error) {
        return error
        
    }
}

async function getAPI(link) {
    try {
        const res = await fetch(`http://localhost:8080/api/astro/${link}`, {
            method: "GET",
            headers: { "Content-Type": "application/json" },
        })
        const responseData = await res.json()
        return responseData
    } catch (error) {
        const responseData = {
            erreur: {
                message: "Mauvais lien ou serveur non atteignable",
            }
        };
        return responseData;
    }
}

export { additional, sameObject, sameString, returnTrue, awaitString, throwError,throwError2, mama,getAPI }   