import { useState } from "react"
import { useEffect } from "react"
import { getAPI } from "../libs"

export default function Compo1({content,onClickFunc}){

    const [user, setUser]=useState(null)

    useEffect(()=>{
        getUser()
        getDataAstro()
    },[])

    async function getUser(){
        const userData = await fetch("https://jsonplaceholder.typicode.com/todos/1").then(res=>res.json()) 
        setUser(userData)
    }

    async function getDataAstro(){
        const res = await getAPI("read-all")
        // console.log("🚀 ~ file: compo1.jsx:21 ~ getDataAstro ~ res", res)
    }
   
    function justConsoleLog(){

    }

    return (
    <div>
        <input value="JavaScript" onChange={e=>console.log(e.target.value)} />
        <p onClick={onClickFunc}>lala</p>
        <p>hello</p>
        {user ? <p>Signed in as {user.userId}</p> : null}
        <button onClick={onClickFunc}> {content} </button>
        <button onClick={()=>justConsoleLog()}> yes </button>
        <hr />

    </div>
    )
}