import Compo1 from "./compo1"
import { render, screen, fireEvent } from '@testing-library/react'
import userEvent from "@testing-library/user-event"

describe("compo1", () => {
    
    it("lala text is present", () => {
        render(<Compo1 />)
        expect(screen.getByText("lala")).toBeInTheDocument()
        expect(screen.getByDisplayValue('JavaScript')).toBeInTheDocument();
    })
    it("async user data", async () => {
        render(<Compo1 />)
        expect(screen.queryByText(/Signed in as/)).toBeNull();
        // screen.debug();

        expect(await screen.findByText(/Signed in as/)).toBeInTheDocument();
    })
    // it("simualte click",()=>{
    //     render(<Compo1 />)
    //     screen.debug();

    //     fireEvent.change(screen.getByRole('textbox'), {
    //       target: { value: 'JavaScript' },
    //     });
    
    //     screen.debug();
    // })
    it("test props",()=>{
        render(<Compo1 content="lala" />)
        expect(screen.getByRole("button",{name:"lala"}))
    })
    it("test a function in the props",()=>{
        const mockOnClicFunction = jest.fn()
        render(<Compo1 onClickFunc={mockOnClicFunction} content="lala"/>)
        userEvent.click(screen.getByRole("button",{name:"lala"}))
        expect(mockOnClicFunction).toBeCalled()
    })
    // it("test a simple function insode component",()=>{
    //     const mockOnClicFunction = jest.fn()
    //     render(<Compo1/>)
    //     userEvent.click(screen.getByRole("button",{name:"yes"}))
    //     expect(mockOnClicFunction).toBeCalled()
    // })
})
