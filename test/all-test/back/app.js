require('dotenv').config()

const express = require('express');
const app = express();

const cors = require("cors")

const mongoose = require('mongoose');
const bodyParser = require('body-parser');

mongoose.set('strictQuery', false)

app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cors())

mongoose.connect(process.env.MONGO_CONNECT, {});

require('./models/Astro');
app.use(require('./routes'))

app.listen(process.env.PORT, () => console.log(`Server running on http://localhost:${process.env.PORT}/`));