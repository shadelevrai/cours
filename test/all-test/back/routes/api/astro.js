const mongoose = require("mongoose");

const router = require("express").Router();
const Astro = mongoose.model("Astro")

router.post("/add", (req, res) => {

    try {
        console.log("🚀 ~ file: astro.js:24 ~ router.post ~ req.body", req.body.astro)
        if (!req.body.astro) throw "Object astro required"
        if (!req.body.astro.yearOld || !req.body.astro.name) throw "name or year old astro required"

        const { body: { astro } } = req;

        const finalAstro = new Astro(astro)
        finalAstro.save(err => {
            if (err) return res.status(422).json({ error: "save failed" });
            return res.status(200).json({ astro: finalAstro.toAuthJSON() });
        })

    } catch (err) {
        return res.status(422).json({ erreur: { message: err } })
    }
})

router.get("/read-all", (req, res) => {
    return Astro.find().then((astro) => {
        return res.status(200).json({ astro });
    });
})

router.get("/read-by-name", (req, res) => {
    try {
        if (!req.body.name) throw "name astro required"

        const {
            body: { name } } = req;

        return Astro.find({ name }).then(astro => {
            if (astro.length === 0) return res.status(422).json({ erreur: { message: "Astro not found" } })
            return res.status(200).json({ astro })
        })

    } catch (err) {
        return res.status(422).json({ erreur: { message: err } })
    }
})

router.patch("/update-one", (req, res) => {
    try {
        if (!req.body.name || !req.body.yearOld || !req.body._id) throw "id, name or age required"

        const {
            body: { name, yearOld, _id },
        } = req;

        return Astro.findByIdAndUpdate(_id, { name, yearOld }, { new: true }).then(result => {
            if (!result) return res.status(422).json({ erreur: { message: "modification failed" } })
            return res.status(200).json({ astro: result })
        })

    } catch (err) {
        return res.status(422).json({ erreur: { message: err } })
    }
})

router.delete("/delete-one", (req, res) => {
    try {
        if (!req.body._id) throw "id required"

        const { body: { _id } } = req

        return Astro.findByIdAndDelete(_id).then(result => {
            if (!result) return res.status(422).json({ erreur: { message: "delete failed" } })
            return res.status(200).json({ message: "delete succes" })
        })
    } catch (err) {
        return res.status(422).json({ erreur: { message: err } })
    }
})

module.exports = router; 