import type { NextPage } from "next";
import Link from "next/link";

const Home: NextPage = () => {
  return (
    <>
      <h1>Bonjour</h1>
      <nav>
        <Link href="/about">
          <a>About Page</a>
        </Link>
      </nav>
    </>
  );
};

export default Home;
