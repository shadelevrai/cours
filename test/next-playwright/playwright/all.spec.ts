import { test, expect } from "@playwright/test";

test.beforeAll(async () => {
  console.log("Before tests");
});

test.afterAll(async () => {
  console.log("After tests");
});

test("Je peux voir un h1 avec le mot bienvenue (unit)", async ({ page }) => {
  await page.goto("/");
  await expect(page.locator("h1")).toContainText("Bonjour");
});

test("Je peux aller sur une autre page en cliquant sur un bouton de redirection (unit)", async ({ page }) => {
  await page.goto("/");
  await page.click("text=About Page");
  await expect(page).toHaveURL("/about");
});

test('my test', async ({ page }) => {
  await page.goto('/about');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/lalala/);

  // // Expect an attribute "to be strictly equal" to the value.
  await expect(page.locator('text=Get Started').first()).toHaveAttribute('href', '/docs/intro');

  // Expect an element "to be visible".
  await expect(page.locator('text=Learn more').first()).toBeVisible();

  // await page.click('text=Get Started');
  // // Expect some text to be visible on the page.
  // await expect(page.locator('text=Introduction').first()).toBeVisible();
});

test("Le premier element 'title' est présent et a comme texte 'je suis le premier element title'", async ({ page }) => {
  await page.goto("/about")
  //Il y a un element title et il a comme texte "lalala"
  await expect(page).toHaveTitle(/lalala/);
  // Le premier element avec le texte "Get Started" a un attribut href "/docs/intro"
  await expect(page.locator('text=Get Started').first()).toHaveAttribute('href', '/docs/intro');
})

test("Il y a un premier element visible qui a comme texte 'Learn more' ", async ({ page }) => {
  await page.goto("/about")
  await expect(page.locator('text=Learn more').first()).toBeVisible();
})

test("il y a un object avec les valeur que je veux dans une variable", async () => {
  const lala = { name: "ryan", "age": 30 }
  expect(lala).toMatchObject({
    name: "ryan"
  })
})