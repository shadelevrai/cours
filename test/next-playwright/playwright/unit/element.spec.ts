import {test,expect} from "@playwright/test"

test("it should have a title",async ({page})=>{
    await page.goto("/")
    const title = page.locator("h1")
    await expect(title).toHaveText("Bonjour")
})