<!-- https://www.atlassian.com/fr/continuous-delivery/software-testing/types-of-software-testing -->

### Différent types de test

## End to end

End-to-end tests are a form of software testing whose goal is to test the application as real users use it. It’s called end-to-end because the test starts by exercising one end of the application (the user interface) and going all the way through the other end (the database or whatever storage mechanism the app uses), exercising all the layers in between. When it comes to a web application, you would navigate to its URL, perform some action on it, and verify whether it behaves as intended.



Les tests de bout en bout reproduisent le comportement d'un utilisateur avec le logiciel dans un environnement applicatif complet. Ils vérifient que les différents flux d'utilisateurs fonctionnent comme prévu et peuvent être aussi simples que le chargement d'une page Web ou la connexion. Des scénarios beaucoup plus complexes peuvent aussi vérifier les notifications par e-mail, les paiements en ligne, etc.
Les tests de bout en bout sont très utiles, mais ils sont coûteux à réaliser et peuvent être difficiles à gérer lorsqu'ils sont automatisés. Il est recommandé d'avoir quelques tests clés de bout en bout et de s'appuyer davantage sur des tests de niveau inférieur (tests unitaires et d'intégration) pour être en mesure d'identifier rapidement les changements de dernière minute.




## Intégration

Les tests d'intégration testent une fonctionnalité plus qu'on composant. Pour une API qui expose des données venant d'une base de données, un test d'intégration consiste à appeler une route d'API avec certains paramètres, et vérifier si le résultat correspond à la réponse attendue.

Les tests d'intégration permettent de vérifier que tous les bouts de code isolés fonctionnent bien ensemble.

Les tests d'intégration vérifient que les différents modules ou services utilisés par votre application fonctionnent bien ensemble. Par exemple, ils peuvent tester l'interaction avec la base de données ou s'assurer que les microservices fonctionnent ensemble comme prévu. Ces types de tests sont plus coûteux à exécuter, car ils nécessitent que plusieurs parties de l'application soient fonctionnelles.




## unit

Les tests unitaires permettent de tester un composant, ou un bout de code, isolé de ses dépendances.




## Test fonctionnel


Les tests fonctionnels se concentrent sur les exigences métier d'une application. Ils vérifient uniquement la sortie d'une action et non les états intermédiaires du système lors de l'exécution de cette action.

Il y a parfois une certaine confusion entre les tests d'intégration et les tests fonctionnels, car ils nécessitent tous les deux plusieurs composants pour interagir. La différence réside dans le fait qu'un test d'intégration peut simplement vérifier que vous pouvez interroger la base de données, tandis qu'un test fonctionnel s'attend à obtenir une valeur spécifique de la base de données, telle que définie par les exigences du produit.






## Tests d'acceptation

Les tests d'acceptation sont des tests formels exécutés pour vérifier si un système répond à ses exigences métier. Ils nécessitent que l'application soit entièrement opérationnelle et se concentrent sur la simulation du comportement des utilisateurs. Ils peuvent aussi aller plus loin et mesurer la performance du système et rejeter les changements si certains objectifs ne sont pas atteints.




## Tests de performance

Les tests de performance vérifient les comportements du système lorsqu'il est soumis à une charge élevée. Ces tests sont non fonctionnels et peuvent prendre diverses formes pour comprendre la fiabilité, la stabilité et la disponibilité de la plateforme. Par exemple, il peut s'agir d'observer les temps de réponse lors de l'exécution d'un nombre important de requêtes, ou le comportement du système face à une quantité élevée de données.
Les tests de performance sont par nature assez coûteux à mettre en œuvre et à exécuter, mais ils peuvent vous aider à comprendre si de nouveaux changements vont dégrader votre système.
