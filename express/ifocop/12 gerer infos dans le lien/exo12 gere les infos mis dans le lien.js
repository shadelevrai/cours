/***********************************************************
****************GÉRER LES VALEURS DANS L'URL****************
***********************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
Il existe plusieurs façons de traiter des informations fournies dans l'url.
Parmis celles-ci, il y a les propriétées params et query de l'objet req.

------ 1 ------
Si je saisis dans l'url http://www.monsite.com/infos/truc/machin
et que je traite du côté serveur, je peux récupérer les données de la façon suivante :
app.get('/infos/:un/:deux', function(req,res)){
  console.log(req.params.un); //affiche truc
  console.log(req.params.deux); //affiche machin
}

------ 2 ------
Si je saisis dans l'url http://www.monsite.com/question?r=chose&t=bidule
et que je traite du côté serveur, je peux récupérer les données de la façon suivante :
app.get('/question', function(req,res)){
  console.log(req.query.r); //affiche chose
  console.log(req.query.t); //affiche bidule
}
*/


/*********************************
*************Exercice*************
*********************************/
/*

Modifiez votre site pour changer de page en passant la référence de la page 
dans l'url en traitant avec req.params ou req.query.

*/
var express = require('express');
var app = express();

app.use(express.static(__dirname + '/html4')); //Dire qu'on va utiliser ce que contient le dossier html

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './html4'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/accueil/:un/:deux',function(req,res){ // on ecrit les deux infos qu'on veut récupérer lors de la saisie de l'url.
  res.render('home', {variable1 : 'un texte', variable2 : 'un autre texte'}); // on envoit le fichier index.jade du dossier html3 et on donne des valeurs a des variables.
    console.log(req.params.un); //On affiche l'info 'un' dans la console
    console.log(req.params.deux); // On affiche l'info 'deux' dans la console
});
app.get('/page01',function(req,res){
    res.render('page1', {variable : 'un texte', variable2 : 'un autre texte'});
    console.log(req.query.bidule);
    console.log(req.query.machin); // Si on saisie './page01?bidule=ca&machin=va', alors la console va afficher 'ca va'.
});
app.get('/page02',function(req,res){
    res.render('page2',{variable1 : 'un texte', variable2:'un autre texte'});
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});

//gérer des requete dans le lien