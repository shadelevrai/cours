/************************************************************
************UTILISATION DES TEMPLATES - MINI SITE************
************************************************************/

/*********************************
*************Exercice*************
*********************************/
/*
------ 1 ------
Reprenez le code de l'exercice 6.

------ 2 ------
Utilisez Jade pour transformer vos trois documents HTML en template.

*/
var express = require('express');
var app = express();

app.use(express.static(__dirname + '/html4')); //Dire qu'on va utiliser ce que contient le dossier html

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './html4'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/accueil',function(req,res){
  res.render('home', {variable1 : 'un texte', variable2 : 'un autre texte'}); // on envoit le fichier index.jade du dossier html3 et on donne des valeurs a des variables.
});
app.get('/page01',function(req,res){
    res.render('page1', {variable : 'un texte', variable2 : 'un autre texte'});
});
app.get('/page02',function(req,res){
    res.render('page2',{variable1 : 'un texte', variable2:'un autre texte'});
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});
