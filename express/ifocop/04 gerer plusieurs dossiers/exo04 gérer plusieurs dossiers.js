/***********************************************************
************UTILISATION DES FICHIERS STATIQUES 2************
***********************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
Vous pouvez déclarer autant de dossier statique que vous le souhaitez.
*/

/*********************************
*************Exercice*************
*********************************/
/*

------ 1 ------
Créez deux dossiers à côté de votre fichier. Placez une image dans chaque dossier.

------ 2 ------
Déclarez successivement ces deux dossiers dans votre fichier JavaScript.

------ 3 ------
Quand l'utilisateur se connecte à votre serveur, envoyez-lui la balise image  correspondant au premier dossier.

------ 4 ------
S'il saisit le texte 'image' dans l'URL, affichez lui l'image correspondant au second dossier.
*/

var express = require('express');
var app = express();

app.use("/img", express.static(__dirname + '/img')); // on déclare où se trouvera le dossier qu'on va utiliser par la suite. Ici, il sera dans un dossier qui se nomme 'img'.
app.use('/img2', express.static(__dirname + '/img2'));

//Gestion des deux routes
app.get('/',function(req,res){
  res.send('<img src="/img/image.png">');
});

app.get('/image', function(req,res){
    res.send('<img src="/img2/vert.png">');
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});