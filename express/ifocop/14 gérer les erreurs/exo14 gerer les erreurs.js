/**********************************************************
********************TRAITER LES ERREURS********************
**********************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
Il est important de prendre en compte les cas où l'utilisateur ne saisit aucune adresse valide. Quand cela arrive, il faut générer un en-tête avec le code 404 suivi d'une page particulière.
L'ensemble des recommandations se trouve ici : http://expressjs.com/en/starter/faq.html#how-do-i-handle-404-responses
*/


/*********************************
*************Exercice*************
*********************************/
/*
Ajoutez la gestion de la page 404.
*/

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(express.static(__dirname + '/html5')); //Dire qu'on va utiliser ce que contient le dossier html
app.use(bodyParser.urlencoded({ extended: false }));//Dire qu'on va utiliser le middleware Bodyparser

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './html5'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/accueil',function(req,res){ 
  res.render('home'); 
});

app.post('/reponse1111', function (req, res) { // '.post' veut dire que la page peut être modifiée. 'reponse1111' est associé au formulaire html/jade avec 'action' dans le 'form'. en gros quand on va cliquer sur 'Envoyer', le lien sera ..../reponse1111.
  res.render('reponse',{ titrePage: 'Réponse du formulaire',titreH1: 'Une page de réponse', reponse1111: req.body.info}); // 'reponse' sera le fichier html/jade qui sera appelé. Dans les données, nous avons plusieurs objets. Et dans les objets, nous avons l'objet 'response1111' qui aura comme information le 'info', 'info' qui est dans le formulaire (qui est dans le body).
});

app.get('/404', function(req, res, next){ //gérer une erreur
  next();
});

app.use(function(req,res,next){
    res.status(404); 
    if (req.accepts('html')) {
    res.render('404',{ titrePage: 'Erreur', titreH1: 'Des erreurs'});
    return;
  }
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});
