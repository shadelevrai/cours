/*************************************************************
*****UTILISATION DES TEMPLATES - UTILISATION DE VARIABLES*****
*************************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
Jade permet d'afficher le contenu d'une variable.
Dans la méthode res.render, on fournit un second argument sous la forme d'un objet. Chaque propriété de cet objet peut être utilisée dans le template de la façon suivante :
    fichier JavaScript :
      res.render(<nom du fichier>, {valeur1: 'un texte', valeur2 : 'un autre texte'})
    template jade :
      p= valeur1

    Ceci donnera <p>un texte</p> dans le document HTML fournit au client.
*/


/*********************************
*************Exercice*************
*********************************/
/*
------ 1 ------
Reprenez les documents de l'exercice précédent.

------ 2 ------
Passez le nom de la page et le titre du h1 dans l'objet fournit en second argument de la méthode res.render et affichez les dans votre document HTML fourni au client.
*/

var express = require('express');
var app = express();

app.use(express.static(__dirname + '/html3')); //Dire qu'on va utiliser ce que contient le dossier html

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './html3'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/accueil',function(req,res){
  res.render('home', {variable1 : 'un texte', variable2 : 'un autre texte', lalala : 'okpjhjbh'}); // on envoit le fichier index.jade du dossier html3 et on donne des valeurs a des variables.
    lalala = 'je suis là';
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});

/*
JADE

doctype html(lang='en')
head
    meta(charset='UTF-8')
    |     
    title Home
    |     
    link(rel='stylesheet', href='style.css')
  body
    img(src='bandeau.jpg')
    |     
    h1 LE TITRE !
    |     
    ul
      li Page d&apos;accueil
      |         
      li Page 1
      |         
      li Page 2
    |   
    p= variable1

*/