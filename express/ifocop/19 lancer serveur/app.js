const express = require('express');
const app = express();
var server = require('http').createServer(app);
var port = process.env.PORT || 8080;

app.get('/api/hello', (req, res) => {
    res.send({ express: 'Hello From Express' });
  });

var server = app.listen(port, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
  });

  //pour lancer ce serveur, il faut faire 'npm run dev'
  //Dans le package.json, ça va lancer l'objet {scripts:{dev:blablabla}}
  //Dans ce cas de figure, le 'npm run dev' lancera le module concurrently (à installer en -g) qui lancera la ligne client et server. L'option --kill-others-on-fail permet de fermer l'un si l'autre ne démarre pas