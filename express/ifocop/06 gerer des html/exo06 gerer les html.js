/***********************************************************
****************ROUTES ET FICHIERS STATIQUES****************
***********************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
En utilisant la gestion des routes et l'utilisation des fichiers statiques, vous allez réaliser un mini site.
*/


/*********************************
*************Exercice*************
*********************************/
/*
------ 1 ------
Créez trois documents HTML contenant un titre et un menu. Chacun appelle le même fichier CSS. Prévoyez une image d'en-tête.

------ 2 ------
Un menu comportera trois liens : page d'accueil, page 1 et page 2.
Utilisez un app.use et trois app.get.

*/
var express = require('express');
var app = express();

app.use(express.static(__dirname + '/html2')); //Dire qu'on va utiliser ce que contient le dossier html

//Gestion des deux routes
app.get('/accueil',function(req,res){
  res.sendFile('home.html',{root:'html2'}); // on envoit le fichier index du dossier html
});
app.get('/page1', function(req,res){
    res.sendFile('page1.html', {root:'html2'})
});
app.get('/page2', function(req,res){
    res.sendFile('page2.html', {root:'html2'})
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});