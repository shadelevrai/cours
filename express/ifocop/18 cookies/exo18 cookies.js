var express = require('express');
var app = express();
var cookieParser = require('cookie-parser');

app.use(cookieParser());

app.get('/mettreCookie', (req, res) => {
    res.cookie('test', 'cookie_value').send('Cookie is set');
})
app.get('/afficherCookie', (req, res) => {
    console.log("Cookies :  ", req.cookies);
});
app.get('/supprimerCookie', (req, res) => {
    res.clearCookie('test');
    res.send('Cookie deleted');
})
//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888, function () { 
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
});