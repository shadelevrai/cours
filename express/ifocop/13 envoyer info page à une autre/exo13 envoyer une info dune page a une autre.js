/**********************************************************
**********************GÉRER LES POSTS**********************
**********************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
Le traitement des requêtes en post se fait d'une façon un peu différente.
Il faut utiliser le module body-parser. Après avoir fait un require de ce dernier, on utilise la fonction middleware :
app.use(bodyParser.urlencoded({
  extended: false
}));
Vous trouverez l'ensemble des options sur la page suivante : https://github.com/expressjs/body-parser

À partir de là, on peut utiliser dans la fonction de retour de la gestion du post (app.post) la propriété req.body qui contient autant de propriétés que de champs dans le formulaire.

*/


/*********************************
*************Exercice*************
*********************************/
/*
------ 1 ------
Reprenez le code de l'exercice précédent.
------ 2 ------
Créez une nouvelle page correspondant à un nouveau template comprenant un formulaire avec deux champs (nom et prénom).
------ 3 ------
Traitez l'envoi du formulaire pour afficher dans une autre page le prénom et le nom saisis.
*/
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(express.static(__dirname + '/html5')); //Dire qu'on va utiliser ce que contient le dossier html
app.use(bodyParser.urlencoded({ extended: false }));//Dire qu'on va utiliser le middleware Bodyparser

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './html5'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/accueil',function(req,res){ 
  res.render('home'); 
});

app.post('/reponse1112', function (req, res) { // '.post' veut dire que la page peut être modifiée. 'reponse1111' est associé au formulaire html/jade avec 'action' dans le 'form'. en gros quand on va cliquer sur 'Envoyer', le lien sera ..../reponse1111.
  res.render('reponse',{ titrePage: 'Réponse du formulaire',titreH1: 'Une page de réponse', reponse1112: req.body.info}); // 'reponse' sera le fichier html/jade qui sera appelé. Dans les données, nous avons plusieurs objets. Et dans les objets, nous avons l'objet 'response1111' qui aura comme information le 'info', 'info' qui est dans le formulaire (qui est dans le body).
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});


/*

doctype html(lang='en')
head
    meta(charset='UTF-8')
    |     
    title Home200
    |     
    link(rel='stylesheet', href='style.css')
  body  
    h1 Formulaire
    |     
    p voici le formulaire
    |  
    img(src='/form.jpg')
    |  
 form(action='reponse1111' method='post')
 p
 label Votre nom
  input(type='text' name='info')
  p
  label Votre prénom
  input(type='text' name='info')
  input(type='submit' value='Envoyer')
 
 

*/