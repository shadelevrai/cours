/***********************************************************
************UTILISATION DES FICHIERS STATIQUES 1************
***********************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
Un serveur délivre de nombreux fichiers statiques : CSS, JavaScript, image, son, vidéo …
Express permet de faire référence à ces fichiers en les plaçant dans un sous-dossier. Celui-ci est déclaré dans le fichier js, avant la gestion des routes (app.get(), …) grâce à la déclaration suivante :
app.use("/<nom du dossier>", express.static(__dirname + '/<nom du dossier>'));
Vous pouvez ensuite appeler l'élément dans document page html envoyé au client en spécifiant une URL relative.
*/

/*********************************
*************Exercice*************
*********************************/
/*
Vous devez envoyer au client avec la méthode send de l'objet res une balise image.

------ 1 ------
Reprenez le code de l'exercice 1.

------ 2 ------
Créez un dossier à côté de votre fichier contenant une image. Déclarez ce dossier dans votre fichier JavaScript grâce à la méthode ci-dessus.

------ 3 ------
Quand l'utilisateur se connecte à votre serveur, envoyez-lui une balise image qui affichera l'image contenue dans le dossier dans le navigateur.
*/

var express = require('express');
var app = express();

app.use("/img", express.static(__dirname + '/img')); // on déclare où se trouvera le dossier qu'on va utiliser par la suite. Ici, il sera dans un dossier qui se nomme 'img'.

//Gestion des deux routes
app.get('/',function(req,res){
  res.send('<img src="/img/image.png">');
});


//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});
