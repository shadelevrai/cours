/*******************************************************
 **********************LES SESSIONS**********************
 *******************************************************/

/*********************************
 ***********Présentation***********
 *********************************/
/*
La gestion des sessions fait partie des fondements d'un site web.
Pour gérer les sessions, nous utilisons les module (1) cookieParser et (2) session de la façon suivante :

var express      = require('express')
var session = require('express-session');
var cookieParser = require('cookie-parser')

var app = express()
app.use(cookieParser())

app.use(session({
    secret:'123456789SECRET',
    saveUninitialized : false,
    resave: false
}));

app.get('/', function(req, res) {
  console.log("Cookies: ", req.cookies)
  console.log(req.session);
})
*/


/*********************************
 *************Exercice*************
 *********************************/
/*
Créez un fichier js avec Express qui affiche le contenu d'un template Jade.

------ 1 ------
Créez dans la variable de session un compteur en utilisant req.session.
------ 2 ------
Pour chaque connexion, incrémentez le compteur et affichez la valeur dans le navigateur.
*/

var session = require('express-session');
var cookieParser = require('cookie-parser');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(express.static(__dirname + '/html6')); //Dire qu'on va utiliser ce que contient le dossier html
app.use(bodyParser.urlencoded({
    extended: false
})); //Dire qu'on va utiliser le middleware Bodyparser

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './html6'); // dire où seront les fichiers Jade

app.use(session({
    secret: '123456789SECRET',
    saveUninitialized: false,
    resave: false,
    cookie: {
        maxAge: 100000
    }
}));

app.use(cookieParser())

//Gestion des deux routes
app.get('/accueil', function (req, res, next) {
    var sess = req.session;
    if (sess.views) {
        sess.views++;
        res.render('home');
        console.log(sess.views);
    } else {
        sess.views = 1;
        res.render('page01');
    }
});


app.get('/404', function (req, res, next) { //gérer une erreur
    next();
});

app.use(function (req, res, next) {
    res.status(404);
    if (req.accepts('html')) {
        res.render('404', {
            titrePage: 'Erreur',
            titreH1: 'Des erreurs'
        });
        return;
    }
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888, function () {
    var adressHost = server.address().address;
    var portHost = server.address().port;
    console.log('Ecoute à l\'adresse http://%s:%s', adressHost, portHost);
});