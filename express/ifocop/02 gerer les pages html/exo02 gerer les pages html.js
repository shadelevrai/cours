/*********************************************************
*******************GESTION DES ROUTES 1*******************
*********************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
Un serveur Web doit pouvoir répondre de façon appropriée en fonction de l'URL envoyée par
 le client.
Dans un premier temps, nous allons nous intéresser à la gestion simple de la 
soumission avec la méthode get.
*/


/*********************************
*************Exercice*************
*********************************/
/*
Dans le premier exercice, nous avons envoyé un unique message.

------ 1 ------
Reprenez le code de l'exercice précédent.À l'instar de celui-ci, 
si l'utilisateur saisit l'URL de base, affichez un message.

------ 2 ------
À l'aide d'une seconde méthode get de l'objet contenu dans app, 
affichez un autre message dans le navigateur quand l'utilisateur 
ajoute le mot fin à l'URL de base.
*/

var express = require('express');
var app = express();

//Route de base et envoie d'une chaine de caractères
app.get('/', function(req,res){ // si le lien ne possède rien (/)
  res.send('Bonjour à chacun !'); // On envoit (send) un message
});

app.get('/fin',function (req,res){
    res.send('Tu es à la fin');
});

app.get('*',function(req,res){
  res.send('Le reste');
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});
