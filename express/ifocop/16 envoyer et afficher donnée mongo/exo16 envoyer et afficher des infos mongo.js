/*********************************************************
 ********************LA BASE DE DONNÉES********************
 *********************************************************/

/*********************************
 ***********Présentation***********
 *********************************/
/*
Il existe de plusieurs modules pour interfacer NodeJS avec MongoDB. Nous utiliserons le pilote officiel(https://github.com/mongodb/node-mongodb-native).

Pour se connecter à la base de donnée, il faut écrire en haut du fichier :
var MongoClient = require('mongodb').MongoClient;
…
et en bas :
MongoClient.connect(URL, function(err, db) {
  if (err) {
    return;
  }
  app.listen …
}
*/


/*********************************
 *************Exercice*************
 *********************************/
/*

Créez une base de donnée avec une collection en console. 
------ 1 ------
Connectez votre fichier JavaScript à votre base de donnée.
------ 2 ------
Prévoyez une variable globale dans laquelle vous pourrez stocker l'argument db de la méthode connect.
------ 3 ------
Quand l'utilisateur accède à la racine du site, utilisez le code suivant pour afficher le contenu de votre base (maDb est la variable globale, cf. point 2).
  var collection = maDb.collection(<nom de la collection>);
  collection.find().toArray(function(err, data){
    //utilisez data qui est un objet contenant les différentes valeurs retournée par find.
  });
});
*/

/*
var MongoClient = require('mongodb').MongoClient;
var express = require('express');
var app = express();
var URL = 'mongodb://localhost:27017/tutoriel'

app.get('/', function (req, res) {
  res.send('Accueil du site');
});

MongoClient.connect(URL, function(err, db) {
  if (err) {
    return;
  }
  app.listen(8080, function() {
    console.log('Le serveur est disponible sur le port 8080');
  });
});

//AVEC UN FICHIER A PART

//db.js

var MongoClient = require('mongodb').MongoClient

var state = {
  db: null,
};

exports.connect = function(url, done) {
  if (state.db) {
    return done();
  }

  MongoClient.connect(url, function(err, db) {
    if (err) {
      return done(err);
    }
    state.db = db;
    done();
  });
};

exports.get = function() {
  return state.db;
};

exports.close = function(done) {
  if (state.db) {
    state.db.close(function(err, result) {
      state.db = null;
      state.mode = null;
      if (err) {
        done(err);
      }
    });
  };
};


//principal
var express = require('express');
var app = express();
var db = require('./db');

app.engine('jade', require('jade').__express);
app.set('view engine', 'jade');

app.get('/', function (req,res) {
  var collection = db.get().collection('articles');
  var date = new Date();
  collection.insert({titre: 'Premier titre', texte: 'Du texte', auteur : 'Moi', date : date.getTime()}, function(err, result) {
    collection.find().toArray(function(err, data) {
      res.send(data[0]);
      db.close();
    });
  });
});

// Gestion de la connexion au démarrage
db.connect('mongodb://localhost:27017/blog', function(err) {
  if (err) {
    console.log('Impossible de se connecter à la base de données.');
    process.exit(1);
  } else {
    app.listen(8080, function() {
      console.log('Le serveur est disponible sur le port 8080');
    });
  }
});
*/


var express = require('express');
var bodyParser = require("body-parser");
var app = express();
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/tutoriel2';

app.use(express.static(__dirname + '/html7')); //Dire qu'on va utiliser ce que contient le dossier html
app.use(bodyParser.urlencoded({
    extended: true
}));

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './html7'); // dire où seront les fichiers Jade

MongoClient.connect(url, function (err, db) { //Se connecter à mongo avec en argument la variable url qui pointe la base de donnée
    if (err) {
        console.log('Unable to connect to the mongoDB server. Error:', err);
    } else {
        //HURRAY!! We are connected. :)
        console.log('Connection established to', url);
        // do some work here with the database.

        // Get the documents collection
        collection = db.collection('users'); //La variable 'collection'. Elle est globale.

        //Create some users
        /* var user1 = {
             name: 'modulus admin',
             age: 42,
             roles: ['admin', 'moderator', 'user']
         };
         var user2 = {
             name: 'modulus user',
             age: 22,
             roles: ['user']
         };
         var user3 = {
             name: 'modulus super admin',
             age: 92,
             roles: ['super-admin', 'admin', 'moderator', 'user']
         };req.body

         // Insert some users
         collection.insert([user1, user2, user3], function (err, result) {
             if (err) {
                 console.log(err);
             } else {
                 console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
             }*/

        // Search some users
        /*collection.find({
            name: 'modulus user'
        }).toArray(function (err, result) {
            if (err) {
                console.log(err);
            } else if (result.length) {
                console.log('Found:', result);
            } else {
                console.log('No document(s) found with defined "find" criteria!');
            }
            //Close connection
            db.close();
        })*/

        //Afficher les résultats
        db.collection('users').find().toArray(function (err, result) {
            if (err) {
                throw err;
            }
            //console.log(result[0].age, result[1].name);
            //console.log(result.length);
            //lalala = result[6].name;
            //lololo = result[6].age;
            resultat = result;
            console.log(result.length);
            global = result.length - 1;
            globalName = result[global].name;
        });
    }
});
var chiffre = 0;

app.get('/', function (req, res) {

    res.render('home');

})

app.post('/azerty', function (req, res) {
    // Quand le 'action' du 'form' est appelé en cliquant sur envoyer, ça appelle la fonction 'post'. 'app.post' veut dire que ça sera une réponse à une action, et non une page qui peut être appelé via l'url.
    var p1 = req.body.p1; // stock la réponse du 'input' dans la variable 'p1'. on associe le 'input' avec le 'name'.
    var p2 = req.body.p2;
    //console.log("p1=" + p1); // On affiche la valeur du 'p1' dans la console.
    //console.log("p2=" + p2);
    console.log(p1 + p2);
    res.render('page03');
})

app.listen(8888);
console.log('Le serveur 8888 est lancé');
