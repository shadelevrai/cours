/*********************************************************
***********UTILISATION DES FICHIERS STATIQUES 3***********
*********************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
La méthode sendFile de l'objet res permet d'envoyer un fichier spécifique au client.
Elle s'utilise comme suit :
res.sendFile('<nom du fichier>', options);
options: un objet contenant le dossier racine. Par exemple : {root: 'files'}
*/


/*********************************
*************Exercice*************
*********************************/
/*
------ 1 ------
Créez un document HTML. Intégrez au-moins un titre et une balise image.
Placez ce document et l'image dans un dossier.

------ 2 ------
Utilisez la méthode sendFiles pour envoyer le fichier au client.


*/

var express = require('express');
var app = express();

app.use(express.static(__dirname + '/html')); //Dire qu'on va utiliser ce que contient le dossier html

//Gestion des deux routes
app.get('/azerty',function(req,res){
  res.sendFile('index.html',{root:'html'}); // on envoit le fichier index du dossier html
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});