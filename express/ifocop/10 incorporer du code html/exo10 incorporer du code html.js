/**********************************************************
*****************TEMPLATE JADE - INCLUSION*****************
**********************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
Pour ne pas répéter plusieurs fois les mêmes lignes de code dans différents template Jade, 
il est possible d'utiliser l'inclusion.
Cela consiste à déporter la partie du code qui se répète dans un autre fichier pour 
l'appeler ensuite dans les fichiers qui en ont l'usage.
On utilise pour se faire le mot clef include 
(cf. http://jade-lang.com/reference/includes/)
*/


/*********************************
*************Exercice*************
*********************************/
/*
------ 1 ------
Reprenez le code de l'exercice précédent.

------ 2 ------
Utilisez l'inclusion pour mettre à part l'en-tête de la page et le menu.
*/

var express = require('express');
var app = express();

app.use(express.static(__dirname + '/html4')); //Dire qu'on va utiliser ce que contient le dossier html

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './html4'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/accueil',function(req,res){
  res.render('home', {variable1 : 'un texte', variable2 : 'un autre texte'}); // on envoit le fichier index.jade du dossier html3 et on donne des valeurs a des variables.
});
app.get('/page01',function(req,res){
    res.render('page1', {variable : 'un texte', variable2 : 'un autre texte'});
});
app.get('/page02',function(req,res){
    res.render('page2',{variable1 : 'un texte', variable2:'un autre texte'});
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});


doctype html(lang='en')
head
    meta(charset='UTF-8')
    |     
    title Home
    |     
    link(rel='stylesheet', href='style.css')
  body
    img(src='bandeau.jpg')
    |     
    h1 LE TITRE !
    |     
    ul
      li Page d&apos;accueil
      |         
      li Page 1
      |         
      li Page 2
    |   
    p= variable1
    include ./page1.jade // pour incorporer du code