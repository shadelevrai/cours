/*********************************************************
*********UTILISATION DES TEMPLATES - PREMIERS PAS*********
*********************************************************/

/*********************************
***********Présentation***********
*********************************/
/*
L'utilisation de template permet de simplifier l'écriture des pages en HTML et de gérer facilement l'affichage des données contenues dans une variable.

Le template le plus utilisé avec express.js est jade. Vous trouverez l'ensemble des possibilités de ce moteur de template dans la documentation : http://jade-lang.com/reference/
Même si la connaissance de l'écriture en Jade est nécessaire, un convertisseur de HTML vers Jade vous donnera une première idée de la syntaxe : http://html2jade.aaron-powell.com/.

Pour utilisez Jade, vous devez , tout d'abord, installer le module correspondant (npm install Jade).
*/


/*********************************
*************Exercice*************
*********************************/
/*

------ 1 ------
Vous définissez un dossier pour les fichiers statiques.

------ 2 ------
Vous spécifiez que vous utilisez le module Jade : app.set('view engine', 'jade').
Pour utilisez le dossier des fichiers statiques pour les fichiers Jade, vous devez utilisez la méthode suivante : app.set('views','<nom de dossier>');

------ 3 ------
Pour appelez le fichier jade, vous utilisez la méthode res.render('<nom du fichier')
*/

var express = require('express');
var app = express();

app.use(express.static(__dirname + '/html3')); //Dire qu'on va utiliser ce que contient le dossier html

app.set('view engine', 'jade'); // Dire qu'on va utiliser la template Jade
app.set('views', './html3'); // dire où seront les fichiers Jade

//Gestion des deux routes
app.get('/accueil',function(req,res){
  res.render('home'); // on envoit le fichier index.jade du dossier html3
});

//Mise en place du serveur qui écoute sur le port indiqué
var server = app.listen(8888,function(){
  var adressHost = server.address().address;
  var portHost = server.address().port;
  console.log('Ecoute à l\'adresse http://%s:%s',adressHost, portHost);
});