import { Schema, model, Document } from "mongoose";
import crypto from "crypto"
import jwt from "jsonwebtoken"

interface User extends Document {
    name: string;
    _id: string;
    username: string;
    email: string;
    password: string;
    setPassword: (password: string) => void;
    validPassword: (password: string) => boolean;
    toAuthJSON: () => any;
    token: string;
}

const UsersSchema: Schema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    customField: String,
    hash: String,
    salt: String,
})

UsersSchema.methods.setPassword = function (password: any): void {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UsersSchema.methods.validatePassword = function (password: any): boolean {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
};

UsersSchema.methods.generateJWT = function (): any {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);

    return jwt.sign({
        // email: this.email,
        id: this._id,
        // @ts-ignore
        exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, 'secret');
}

UsersSchema.methods.toAuthJSON = function (): Pick<User, "name" | "_id" | "token"> {
    return {
        name: this.name,
        _id: this._id,
        // email: this.email,
        // lastName: this.lastName,
        // firstName: this.firstName,
        // birthDay: this.birthDay,
        // image: this.image,
        token: this.generateJWT(),
        // articles:this.articles
    };
};

const Users = model<User>("Users", UsersSchema)

export default Users