import mongoose from "mongoose"
import passport from "passport"
import { Strategy } from "passport-local"

const Users = mongoose.model("Users")

passport.use("user", new Strategy({
  usernameField: 'user[name]',
  passwordField: 'user[password]',
}, (name, password, done) => {
  Users.findOne({ name })
    .then((user) => {
      if (!user || !user.validatePassword(password)) {
        return done(null, false, { message:'name or password is invalid'  });
      }

      return done(null, user);
    }).catch(done);
}));

// passport.use("admin",new Strategy({
//   usernameField: 'admin[name]',
//   passwordField: 'admin[password]',
// }, (name, password, done) => {
//   Admins.findOne({ name })
//     .then((user) => {
//       if(!user || !user.validatePassword(password)) {
//         return done(null, false, { message: 'name or password is invalid'  });
//       }

//       return done(null, user);
//     }).catch(done);
// }));