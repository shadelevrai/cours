import request from "supertest";
import express, { Router } from "express"
import app from "../../index"

import generateString from "../../../lib/generateString";

const router: Router = express.Router()
router.use('/', app);

describe("API user",()=>{
    const password:string = "azerty"
    const name:string = generateString(10)
    let token:string;
    let id:string;

    test("POST /", (done)=>{
        request(router)
        .post("/api/users/")
        .send({ user: { name, password } })
        .expect("Content-Type", /json/)
        .expect(res => {
            expect(res.body.user.name).toEqual(name)
            expect(res.body.user.token).toBeTruthy()
            expect(res.statusCode).toEqual(200)
            token = res.body.user.token
            id = res.body.user.id
        })
        .end(async (err, res) => {
            if (err) return done(err);
            return done();
        });
    })
})
const usersAPITest = describe
export default usersAPITest