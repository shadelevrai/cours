import express, { Router } from 'express';
import routes from "./api/index"
// import { validationResult, body } from "express-validator"
// import zlib from "zlib"

// import data from "../data/bigData"
const router: Router = express.Router()

router.use("/api",routes)

// router.get("/", (req: Request, res: Response) => {
//     console.log("lala")
//     return res.json({ lala: "lala" })
// })

// router.post("/", body("mama").exists(), (req: Request, res: Response) => {

//     const errors = validationResult(req)
//     if (!errors.isEmpty()) {
//         return res.json({ errors: errors.array() });
//     }

//     console.log(req.body.mama)
//     return zlib.gzip(JSON.stringify(data),(err,buffer)=>{
//         if(err) return err
//         res.json({message:buffer})
//     })
//     // return res.json({ mama: data })
// })

// module.exports = router;
export default router

