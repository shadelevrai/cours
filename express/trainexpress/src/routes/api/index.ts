import express, { Router } from 'express';
import routeUser from "./users"
const router: Router = express.Router()

router.use('/users', routeUser);
// router.use("/questions",require("./questions"))
// router.use("/admins",require("./admins"))

export default router