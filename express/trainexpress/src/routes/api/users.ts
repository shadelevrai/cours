import express, { Router, Request, Response } from 'express';
import mongoose from 'mongoose';
import passport from "passport"
import { validationResult, Result } from 'express-validator';
import { validator_post, validator_postLogin, validator_delete } from "../../validators/users"
import auth from "../auth"
import Users from '../../model/users';

const router: Router = express.Router()

interface User {
    name: string,
    salt: string,
    hash: string,
    token: string,
    generateJWT: () => string,
    toAuthJSON: () => Object
}

router.post("/", auth.optional, validator_post, async (req: Request, res: Response) => {

    const errors: Result = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const { name, password }: { name: string, password: string } = req.body.user

    const existingUser = await Users.findOne({ name })
    if (existingUser) {
        return res.status(409).json({ error: "Cet utilisateur existe déjà." });
    }
    const user = new Users({ name, password })
    user.setPassword(password)

    try {
        await user.save()
        res.json({ user: user.toAuthJSON() });
    } catch (error) {
        res.status(500).json({ error: "Erreur lors de l'enregistrement de l'utilisateur." });
    }

})

router.post("/login", auth.optional, validator_postLogin, (req: Request, res: Response, next: any) => {

    const errors: Result = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }


    return passport.authenticate("user", { session: false }, (err: any, passportUser: Pick<User, "generateJWT" | "toAuthJSON" | "token">) => {
        if (err) return res.status(422).json({
            errors: {
                message: "Mauvais log",
            },
        });

        if (passportUser) {
            const user: Pick<User, "token" | "toAuthJSON"> = passportUser;
            user.token = passportUser.generateJWT();

            return res.json({ user: user.toAuthJSON() });
        }
    })(req, res, next);


});

router.delete("/", validator_delete, auth.required, async (req: Request, res: Response) => {

    try {
        const errors: Result = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        const { body: { _id } } = req

        const userDeleted: Pick<User, "name" | "salt" | "hash"> | null = await Users.findByIdAndDelete(_id)
        if (!userDeleted) {
            return res.status(422).json({ erreur: { message: "La suppression n'a pas été effectuée" } })
        }

        return res.status(200).json({ message: "La suppression a été effectuée" })

    } catch (err) {
        return res.status(422).json({ erreur: { message: err } })
    }
})

export default router