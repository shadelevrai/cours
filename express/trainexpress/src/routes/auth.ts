const { expressjwt: expressjwt } = require('express-jwt');
// import {expressjwt} from "express-jwt";
import { Request } from "express";

const getTokenFromHeaders = (req:Request) => {
    const {
        headers: {
            authorization
        }
    } = req;

    if (authorization && authorization.split(' ')[0] === 'Token') {
        return authorization.split(' ')[1];
    }
    return null;
};

const auth = {
    required: expressjwt({
        secret: 'secret',
        algorithms: ['sha1', 'RS256', 'HS256'],
        userProperty: 'payload',
        getToken: getTokenFromHeaders,
        exp: 100
    }),
    optional: expressjwt({
        secret: 'secret',
        algorithms: ['sha1', 'RS256', 'HS256'],
        userProperty: 'payload',
        getToken: getTokenFromHeaders,
        credentialsRequired: false,
    }),
};

export default auth