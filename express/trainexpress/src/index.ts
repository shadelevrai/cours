require('dotenv').config()

import express, { Express } from 'express';
import bodyParser from "body-parser"
import path from "path"
import helmet from "helmet"
import cors from "cors"
import mongoose from "mongoose"
import session from "express-session"
import routes from "./routes"

const app: Express = express();
const PORT: Number | String | undefined = process.env.PORT
const MONGO_CONNECT:any = process.env.MONGO_CONNECT

app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({
    extended: false
}));

mongoose.set("strictQuery", false);
mongoose.connect(MONGO_CONNECT);

app.use(helmet())
app.use(cors())
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'passport',
    cookie: {
        maxAge: 60000
    },
    resave: false,
    saveUninitialized: false
}));

// app.use(require('./routes'))
app.use("/",routes)

require("./config/passport")


app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}/`));

export default app