import { body, check } from "express-validator"

const validator_post: Array<any> = [
    body("user").isObject(),
    body("user.name").isLength({ min: 3 }),
    body("user.password").isLength({ min: 6 })
]

const validator_postLogin:Array<any> = [
    body("user").isObject(),
    body("user.name").isLength({ min: 3 }),
    body("user.password").isLength({ min: 6 })
]

const validator_delete:Array<any>=[
    check("Authorization").notEmpty(),
    body("_id").not().isEmpty().withMessage("id absent")
]

// module.exports = {
//     validator_post
// }

export { validator_post,validator_postLogin,validator_delete }