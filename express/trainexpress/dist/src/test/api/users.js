"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const supertest_1 = __importDefault(require("supertest"));
const express_1 = __importDefault(require("express"));
const index_1 = __importDefault(require("../../index"));
const generateString_1 = __importDefault(require("../../../lib/generateString"));
const router = express_1.default.Router();
router.use('/', index_1.default);
describe("API user", () => {
    const password = "azerty";
    const name = (0, generateString_1.default)(10);
    let token;
    let id;
    test("POST /", (done) => {
        (0, supertest_1.default)(router)
            .post("/api/users/")
            .send({ user: { name, password } })
            .expect("Content-Type", /json/)
            .expect(res => {
            expect(res.body.user.name).toEqual(name);
            expect(res.body.user.token).toBeTruthy();
            expect(res.statusCode).toEqual(200);
            token = res.body.user.token;
            id = res.body.user.id;
        })
            .end((err, res) => __awaiter(void 0, void 0, void 0, function* () {
            if (err)
                return done(err);
            return done();
        }));
    });
});
const usersAPITest = describe;
exports.default = usersAPITest;
