"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const passport_1 = __importDefault(require("passport"));
const passport_local_1 = require("passport-local");
const Users = mongoose_1.default.model("Users");
passport_1.default.use("user", new passport_local_1.Strategy({
    usernameField: 'user[name]',
    passwordField: 'user[password]',
}, (name, password, done) => {
    Users.findOne({ name })
        .then((user) => {
        if (!user || !user.validatePassword(password)) {
            return done(null, false, { message: 'name or password is invalid' });
        }
        return done(null, user);
    }).catch(done);
}));
// passport.use("admin",new Strategy({
//   usernameField: 'admin[name]',
//   passwordField: 'admin[password]',
// }, (name, password, done) => {
//   Admins.findOne({ name })
//     .then((user) => {
//       if(!user || !user.validatePassword(password)) {
//         return done(null, false, { message: 'name or password is invalid'  });
//       }
//       return done(null, user);
//     }).catch(done);
// }));
