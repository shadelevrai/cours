"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const passport_1 = __importDefault(require("passport"));
const express_validator_1 = require("express-validator");
const users_1 = require("../../validators/users");
const auth_1 = __importDefault(require("../auth"));
const users_2 = __importDefault(require("../../model/users"));
const router = express_1.default.Router();
router.post("/", auth_1.default.optional, users_1.validator_post, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const { name, password } = req.body.user;
    const existingUser = yield users_2.default.findOne({ name });
    if (existingUser) {
        return res.status(409).json({ error: "Cet utilisateur existe déjà." });
    }
    const user = new users_2.default({ name, password });
    user.setPassword(password);
    try {
        yield user.save();
        res.json({ user: user.toAuthJSON() });
    }
    catch (error) {
        res.status(500).json({ error: "Erreur lors de l'enregistrement de l'utilisateur." });
    }
}));
router.post("/login", auth_1.default.optional, users_1.validator_postLogin, (req, res, next) => {
    const errors = (0, express_validator_1.validationResult)(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    return passport_1.default.authenticate("user", { session: false }, (err, passportUser) => {
        if (err)
            return res.status(422).json({
                errors: {
                    message: "Mauvais log",
                },
            });
        if (passportUser) {
            const user = passportUser;
            user.token = passportUser.generateJWT();
            return res.json({ user: user.toAuthJSON() });
        }
    })(req, res, next);
});
router.delete("/", users_1.validator_delete, auth_1.default.required, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const errors = (0, express_validator_1.validationResult)(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        const { body: { _id } } = req;
        const userDeleted = yield users_2.default.findByIdAndDelete(_id);
        if (!userDeleted) {
            return res.status(422).json({ erreur: { message: "La suppression n'a pas été effectuée" } });
        }
        return res.status(200).json({ message: "La suppression a été effectuée" });
    }
    catch (err) {
        return res.status(422).json({ erreur: { message: err } });
    }
}));
exports.default = router;
