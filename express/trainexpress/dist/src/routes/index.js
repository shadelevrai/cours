"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const index_1 = __importDefault(require("./api/index"));
// import { validationResult, body } from "express-validator"
// import zlib from "zlib"
// import data from "../data/bigData"
const router = express_1.default.Router();
router.use("/api", index_1.default);
// router.get("/", (req: Request, res: Response) => {
//     console.log("lala")
//     return res.json({ lala: "lala" })
// })
// router.post("/", body("mama").exists(), (req: Request, res: Response) => {
//     const errors = validationResult(req)
//     if (!errors.isEmpty()) {
//         return res.json({ errors: errors.array() });
//     }
//     console.log(req.body.mama)
//     return zlib.gzip(JSON.stringify(data),(err,buffer)=>{
//         if(err) return err
//         res.json({message:buffer})
//     })
//     // return res.json({ mama: data })
// })
// module.exports = router;
exports.default = router;
