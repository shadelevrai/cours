"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const crypto_1 = __importDefault(require("crypto"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const UsersSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    customField: String,
    hash: String,
    salt: String,
});
UsersSchema.methods.setPassword = function (password) {
    this.salt = crypto_1.default.randomBytes(16).toString('hex');
    this.hash = crypto_1.default.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};
UsersSchema.methods.validatePassword = function (password) {
    const hash = crypto_1.default.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
};
UsersSchema.methods.generateJWT = function () {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);
    return jsonwebtoken_1.default.sign({
        // email: this.email,
        id: this._id,
        // @ts-ignore
        exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, 'secret');
};
UsersSchema.methods.toAuthJSON = function () {
    return {
        name: this.name,
        _id: this._id,
        // email: this.email,
        // lastName: this.lastName,
        // firstName: this.firstName,
        // birthDay: this.birthDay,
        // image: this.image,
        token: this.generateJWT(),
        // articles:this.articles
    };
};
const Users = (0, mongoose_1.model)("Users", UsersSchema);
exports.default = Users;
