"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validator_delete = exports.validator_postLogin = exports.validator_post = void 0;
const express_validator_1 = require("express-validator");
const validator_post = [
    (0, express_validator_1.body)("user").isObject(),
    (0, express_validator_1.body)("user.name").isLength({ min: 3 }),
    (0, express_validator_1.body)("user.password").isLength({ min: 6 })
];
exports.validator_post = validator_post;
const validator_postLogin = [
    (0, express_validator_1.body)("user").isObject(),
    (0, express_validator_1.body)("user.name").isLength({ min: 3 }),
    (0, express_validator_1.body)("user.password").isLength({ min: 6 })
];
exports.validator_postLogin = validator_postLogin;
const validator_delete = [
    (0, express_validator_1.check)("Authorization").notEmpty(),
    (0, express_validator_1.body)("_id").not().isEmpty().withMessage("id absent")
];
exports.validator_delete = validator_delete;
