"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require('dotenv').config();
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const path_1 = __importDefault(require("path"));
const helmet_1 = __importDefault(require("helmet"));
const cors_1 = __importDefault(require("cors"));
const mongoose_1 = __importDefault(require("mongoose"));
const express_session_1 = __importDefault(require("express-session"));
const routes_1 = __importDefault(require("./routes"));
const app = (0, express_1.default)();
const PORT = process.env.PORT;
const MONGO_CONNECT = process.env.MONGO_CONNECT;
app.use(body_parser_1.default.json({ limit: "10mb" }));
app.use(body_parser_1.default.urlencoded({
    extended: false
}));
mongoose_1.default.set("strictQuery", false);
mongoose_1.default.connect(MONGO_CONNECT);
app.use((0, helmet_1.default)());
app.use((0, cors_1.default)());
app.use(express_1.default.static(path_1.default.join(__dirname, 'public')));
app.use((0, express_session_1.default)({
    secret: 'passport',
    cookie: {
        maxAge: 60000
    },
    resave: false,
    saveUninitialized: false
}));
// app.use(require('./routes'))
app.use("/", routes_1.default);
require("./config/passport");
app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}/`));
exports.default = app;
