"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const characters = 'abcdefghijklmnopqrstuvwxyz';
function generateString(length) {
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
exports.default = generateString;
