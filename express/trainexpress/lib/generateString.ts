const characters ='abcdefghijklmnopqrstuvwxyz';

function generateString(length:number):string{
    let result:string = '';
    const charactersLength:number = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

export default generateString