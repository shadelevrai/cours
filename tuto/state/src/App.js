import React, { useState } from 'react';

function App() {

  const [lastname, setLastname] = useState(null)
  const [firstname, setFirstname] = useState(null)
  const [city, setCity] = useState(null)

  const changeLastName = e => {
    setLastname(e.target.value)
  }

  const changeFirstName = e => {
    setFirstname(e.target.value)
  }

  const changeCity = e => {
    setCity(e.target.value)
  }

  const confirm = e => {
    e.preventDefault()
    console.log(`On envoit ${lastname}, ${firstname} et ${city}`)
  }

  return (
    <div>
      <h1>Mon petit formulaire à moi</h1>
      <form onSubmit={confirm}>
        <label htmlFor="lastname">Nom :</label> 
        <input type="text" id="lastname" onChange={changeLastName}/>
        <label htmlFor="firstname">Prénom :</label> 
        <input type="text" id="firstname" onChange={changeFirstName}/>
        <label htmlFor="city">Ville :</label> 
        <input type="text" id="city" onChange={changeCity}/>
        <button type="submit">Valider</button>
      </form>
    </div>
  );
}

export default App;
