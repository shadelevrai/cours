// function test(lala, ...lili){
//     console.log("test -> lala", lala)
//     console.log("test -> lili", lili)
//     console.log("test -> arguments", arguments)
// }

// test("mimi","mama","momo")

// const computer = {
//     name: "My PC",
//     year: 2018,
//     processor: "i5 7600",
//     ram: 8,
//     graphic: "GTX 1060",
//     hard: "SSD 256"
// }

// const newComputer = {
//     ...computer,
//     owner : "Djoher"
// }
// console.log("newComputer", newComputer)


// const {
//     name,
//     year,
//     ...rest
// } = computer

// console.log("rest", rest)

// const computer = {
//     data: {
//         name: "My PC",
//         year: 2018,
//         processor: "i5 7600",
//         ram: 8,
//         graphic: "GTX 1060",
//         hard: "SSD 256"
//     }
// }

// const newComputer = {
//     ...computer, 
//     data : {
//         ...computer.data,
//         owner : "Djoher"
//     }
// }

const obj1 = {
    name : "Djoher",
    age : 35,
    city : null,
    telephone : null
}

const obj2 = {
    city : "paris",
    telephone : 123456
}

const obj3 = {
    ...obj2,
    ...obj1
}
console.log("obj3", obj3)
