const express = require('express');
const port = 1234;
const bodyParser = require("body-parser");
const { ObjectID, ObjectId } = require('mongodb');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017";
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/html'));
app.set('views', './html');

app.get('/', (res) => res.render('index'))

app.post('/add-user', (req, res) => {
  MongoClient.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }, (err, client) => {
    if (err) console.log(err);
    // var db = client.db('nomDeLaCollection');
    client.db('djoherdev').collection('users').insertOne({
      "name": req.body.nom,
      "lastName": req.body.prenom
    }, function (err) {
      console.log('Enregistré !');
    })
  })
  res.redirect('/')
})

app.post('/add-manga', (req, res) => {
  MongoClient.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }, (err, client) => {
    if (err) console.log(err);
    // var db = client.db('nomDeLaCollection');
    client.db('djoherdev').collection('manga').insertOne({
      "title": req.body.title,
    }, function (err) {
      console.log("Enregistré !");
    })
  })
  res.redirect('/')
})

app.post('/add-manga-user', (req, res) => {
  MongoClient.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }, (err, client) => {
    if (err) console.log(err);
    let search = {
      'name': req.body.user,
      "title": req.body.manga
    };
    let userId;
    client.db('djoherdev').collection('users').find(search.name).toArray(function (err, result) {
      if (err) throw err;
      userId = result._id 
    })
    client.db('djoherdev').collection('manga').find(search.title).toArray(function (err, result) {
      if (err) throw err;
      console.log(result);
    })
  })
})


app.listen(port, () => console.log(`Server running on http://localhost:${port}/`));


{
  "_id" : ObjectId("541fs5dfdsf546sd2f1ds"),
  "name" : "Ryan",
  "email" : "jaimelesmanga@gmail.com",
  "lang" : "fr"
  "zipcode": 75001,
  "mangaAlreadyRead": []
}
{
  "_id" : ObjectId("8d7f87ds96s66ssfddf"),
  "title":"Berserk",
  "author": "Kentaro Miura",
  "tome":40
}

{
  "_id" : ObjectId("43d45s64dsf1215e6z6"),
  "title":"Berserk",
  "author": "Kentaro Miura",
  "tome":40
}

{
  {
    "_id" : ObjectId("541fs5dfdsf546sd2f1ds"),
    "name" : "Ryan",
    "email" : "jaimelesmanga@gmail.com",
    "lang" : "fr"
    "zipcode": 75001,
    "mangaAlreadyRead": [{
      "_id" : ObjectId("8d7f87ds96s66ssfddf"),
      "title":"Berserk",
      "author": "Kentaro Miura",
      "tome":40
    }]
  }

  {
    "_id" : ObjectId("541fs5dfdsf546sd2f1ds"),
    "name" : "Ryan",
    "email" : "jaimelesmanga@gmail.com",
    "lang" : "fr"
    "zipcode": 75001,
    "mangaAlreadyRead": [ ObjectId("8d7f87ds96s66ssfddf")]
  }