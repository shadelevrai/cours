import React from 'react'

function NavBar(){
return(
    <nav>
<div className='navbar' role="navigation" aria-label="main navigation">
    <div className='navbar-band'>
        <a className='navbar-item' href='https://bulma.io'>
            <img src='https://bulma.io/images/bulma-logo.png'></img>
        </a>
        <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false"
            data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>
    <div id="navbarBasicExample" className="navbar-menu is-active">
        <div className="navbar-start">
            <div className="navbar-item has-dropdown is-hoverable">
                <a className="navbar-link">
                    More
                </a>
                

                <div className="navbar-dropdown">
                    <a className="navbar-item">
                        About
                    </a>
                    <a className="navbar-item">
                        Jobs
                    </a>
                    <a className="navbar-item">
                        Contact
                    </a>
                </div>
            </div>
            <div className="navbar-item has-dropdown is-hoverable">
                <a className="navbar-link">
                    Lala
                </a>

                <div className="navbar-dropdown">
                    <a className="navbar-item">
                        About
                    </a>
                    <a className="navbar-item">
                        Jobs
                    </a>
                    <a className="navbar-item">
                        Contact
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar-end">
      <div class="navbar-item">
        <img src='https://img.icons8.com/officel/2x/avatar.png' width='80px'></img>
        <button>deco</button>
      </div>
    </div>
</div>
</nav>
)
}

export default NavBar;