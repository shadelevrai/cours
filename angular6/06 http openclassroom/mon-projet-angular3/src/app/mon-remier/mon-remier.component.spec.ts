import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonRemierComponent } from './mon-remier.component';

describe('MonRemierComponent', () => {
  let component: MonRemierComponent;
  let fixture: ComponentFixture<MonRemierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonRemierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonRemierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
