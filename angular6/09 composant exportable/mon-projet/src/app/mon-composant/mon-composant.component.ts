import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mon-composant',
  templateUrl: './mon-composant.component.html',
  styleUrls: ['./mon-composant.component.css']
})
export class MonComposantComponent implements OnInit {

  @Input() title = '';

  constructor() { }

  ngOnInit() {
  }

}
