import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonSecondComponent } from './mon-second.component';

describe('MonSecondComponent', () => {
  let component: MonSecondComponent;
  let fixture: ComponentFixture<MonSecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonSecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
