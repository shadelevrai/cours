# Angluar

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

##############################

## Install angular/material

To install angular/material in your project, you need to 'npm install --save @angular/material'

## Install MatStepperModule

If you want to use MatStepperModule, you need to declare it in the 'app.module' with 'import { MatStepperModule } from '@angular/material' and insert it in the import array of ngModule.

## Install ReactiveFormsModule

import { ReactiveFormsModule } from '@angular/forms', and add it in the import array of ngModule

## Install MatFormFieldModule

import {MatFormFieldModule} from '@angular/material/form-field', and add it in the import array of ngModule


## note de la rétrospéctive 

