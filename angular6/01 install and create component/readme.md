Pour créer un nouveau projet Angular, installer angular avec cette ligne de commande :
npm install -g @angular/cli@latest 
Naviguez vers le dossier souhaité depuis une ligne de commande et saisissez la commande suivante : 
ng new mon-premier-projet --style=scss --skip-tests=true
(Le premier flag (élément de la commande suivant un double tiret --) crée des fichiers  .scss  pour les styles plutôt que des fichiers  .css .  Le second flag annule la création des fichiers test.)

Lancer le serveur avec :
ng-serve
Le projet sera compilé et disponible sur le port 4200

Creer un Component
Pour créer un component, il faut faire 'ng generate component mon-component'
Le component sera automatiquement crée et le fichier app.module sera automatiquement mis à jour