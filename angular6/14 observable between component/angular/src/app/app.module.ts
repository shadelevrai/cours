import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ManualSubscriptionsComponent } from './manual-subscriptions/manual-subscriptions.component';

@NgModule({
  declarations: [
    AppComponent,
    ManualSubscriptionsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
