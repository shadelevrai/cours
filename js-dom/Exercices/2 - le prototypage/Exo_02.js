var mammouth = function () {
    this.age = 150;
    this.poids = 1000;
    this.espece = 'gros poilu';
    this.fourrure = true;
    this.genre = 'mâle';
    this.cri = function () {
        alert('barrir');
    }
};

var elephant = function(poids, espece, fourrure){
    this.poids = poids;
    this.espece = espece;
    this.fourrure = fourrure;
    this.manger = function(){
        alert('Il bouffe');
    };
}

elephant.prototype = new mammouth();

var elephant1 = new elephant (700, 'petit poilu', false);
var elephant2 = new elephant (500, 'moyen poilu', false);
elephant1.cri();
elephant1.manger();


/*Fonctions constructeur
var ConstructeurDeMaisonKaufmanBroad = function(){
  this.superficie = 120;
  this.nombreDePiece = 6;
  this.piece = {
    rezDeChaussee:['entrée','toilettes','salon','cuisine'],
    etage:['chambre 1','chambre 2', 'salle d\'eau']
  };
  this.sonne = function(){
    alert('Dring !!!');
  };
};
// Instances de la fonction constructeur : Mot clé new
var laMaisonDesMartins = new ConstructeurDeMaisonKaufmanBroad();
var laMaisonDesDuponts = new ConstructeurDeMaisonKaufmanBroad();
var laMaisonDesDurands = new ConstructeurDeMaisonKaufmanBroad();
// On a créé 3 objets à l'aide de la fonction constructeur

//Prototypage avec Object.create()
laMaisonDesMartinsApresOuragan = Object.create(laMaisonDesMartins);

laMaisonDesMartinsApresOuragan.piece.etage = null;

//Autre technique de prototypage
var ConstructeurDeMarcelDurand = function(){
  this.prenom = 'Marcel';
  this.nom = 'Durand';
  this.age = 89;
  this.yeux = 'gris';
};

var ConstructeurDePapa = function(pr, a){
  this.age = a;
  this.prenom = pr;
}

ConstructeurDePapa.prototype = new ConstructeurDeMarcelDurand();

var papa1 = new ConstructeurDePapa('Marc', 43);
var papa2 = new ConstructeurDePapa('Raoul', 36);
*/