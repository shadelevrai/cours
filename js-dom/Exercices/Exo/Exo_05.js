var ConstructionRobot = function(){
    this.numeroDeSerie = '000';
    this.numeroModele = 404;
    this.nom = 'Monsieur Robot';
    this.couleur = 'Rouge';
    this.matiere = 'Metal';
    this.garantie = "01/01/2020";
};
var robot = function(serie, coul){
    this.numeroDeSerie = serie;
    this.couleur = coul;
}
robot.prototype = new ConstructionRobot();

var robot1 = new robot(1,'vert');
var robot2 = new robot(2, 'bleu');
var robot3 = new robot(3, 'blanc');
var robot4 = new robot(4, 'gris');
var robot5 = new robot(5, 'noir');
var robot6 = new robot(6, 'cyan');
var robot7 = new robot(7, 'magenta');
var robot8 = new robot(8, 'saumon');
var robot9 = new robot(9, 'marron');
var robot10 = new robot(10, 'ciel');
var robot11 = new robot(11, 'rose');
var robot12 = new robot(12, 'violet');

var magasin = [robot1, robot2, robot3, robot4, robot5, robot6, robot7, robot8, robot9, robot10, robot11, robot12];

var compteur = 0;
var nomRobot = function () {
    while (compteur < magasin.length) {
        magasin[compteur].nom = magasin[compteur].numeroDeSerie + magasin[compteur].numeroModele + compteur;
        compteur++;
    }
}
nomRobot();