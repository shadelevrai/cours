var prof = {
    age: 30,
    presence: true,
    nom: 'Orle',
    prenom: 'Jona',
    discipline: 'Sport'
};

var classe1 = {
    numero: 40,
    specialite: "combat",
    prof: {
        age: 25,
        presence: true,
        nom: 'Gallic',
        prenom: 'alan',
        discipline: 'Assassinat'
    }
};

var eleve1 = {
    age: 15,
    presence: true,
    nom: "Bechiki",
    prenom: "Gahouti"
};

var eleve2 = {
    age: 14,
    presence: true,
    nom: "Bechiki",
    prenom: "Billal"
};

var eleve3 = {
    age: 16,
    presence: true,
    nom: "Hadri",
    prenom: "Momo"
};

classe1.lesEleves = [eleve1, eleve2, eleve3];

classe1.lesEleves[1].presence = false;

var appel1 = function () {
    if (classe1.lesEleves[0].presence == true) {
        alert(classe1.lesEleves[0].nom + ' ' + classe1.lesEleves[0].prenom + ' est là');
    } else {
        alert(classe1.lesEleves[0].nom + ' ' + classe1.lesEleves[0].prenom + ' est pas là');
    };
};
var appel2 = function () {
    if (classe1.lesEleves[1].presence == true) {
        alert(classe1.lesEleves[1].nom + ' ' + classe1.lesEleves[1].prenom + ' est là');
    } else {
        alert(classe1.lesEleves[1].nom + ' ' + classe1.lesEleves[1].prenom + ' est pas là');
    };
};
var appel3 = function () {
    if (classe1.lesEleves[2].presence == true) {
        alert(classe1.lesEleves[2].nom + ' ' + classe1.lesEleves[2].prenom + ' est là');
    } else {
        alert(classe1.lesEleves[2].nom + ' ' + classe1.lesEleves[2].prenom + ' est pas là');
    };
};
appel1();
appel2();
appel3();

/*alert ('je suis ' + eleve1.nom + ' ' + eleve1.prenom + ' et je suis ' + classe1.lesEleves[0].presence);
alert ('je suis ' + eleve2.nom + ' ' + eleve2.prenom + ' et je suis ' + classe1.lesEleves[1].presence);
alert ('je suis ' + eleve3.nom + ' ' + eleve3.prenom + ' et je suis ' + classe1.lesEleves[2].presence); */


/*
var enHaut = {
  enHaut:[{
    enHaut:function(){},
    enBas:'Ailleurs'
  },{
    enHaut:1,
    enBas:3
  }],
  enBas:{
    enHaut:{
      enBas:true
    },
    enBas:{
      enHaut:[false, function(){}],
      enBas:{
        enHaut:'ici',
        enBas:'là'
      }
    }
  }
};

enHaut.enBas.enBas.enHaut[1]();

enHaut.enBas.enBas.enHaut[0] = 45;

enHaut.enBas.enBas.enBas.choucroute = enHaut.enBas.enBas.enHaut[0] * 2;

var billy = {
  etagereDuHaut: 'mes livres',
  etagereDuMilieu: 'mes magazines',
  etagereDuBas: 'mes papiers administrateur'
};

var maMaison = {
  rezDeChaussee:['salon', 'cuisine', 'toilettes'],
  etage:['chambre des parents', 'chambre des enfants', 'salle d\'eau']
};
*/