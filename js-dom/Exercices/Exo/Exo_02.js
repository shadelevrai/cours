var blender = {
    mixerDesNombres: function (a, b, c) {
        if (typeof a == 'number' && typeof b == 'number' && typeof c == 'number') {
            var d = (a * b) + c;
            alert(d);
            return d;
        } else {
            alert('Les types de données fournis sont incorrects !');
        };
    },
    mixerDesChaines: function (i, j) {
        if (typeof i == 'string' && typeof j == 'string') {
            var k = 'Bonjour ' + i + j + '!';
            alert(k);
            return k;
        } else {
            alert('Les types de données fournis sont incorrects !');
        };
    },
    mixerDesBooleens: function (x, y, z) {
        if (typeof x == 'boolean' && typeof y == 'boolean' && typeof z == 'boolean') {
            var message = '';
            if (x && y && z) {
                message = 'Tout est vrai !';
            } else {
                message = 'Au moins un argument n\'est pas vrai !';
            };
            alert(message);
            return message;
        } else {
            alert('Les types de données fournis sont incorrects !');
        };
    },
    mixerDesTableaux: function (g, z) {
        if (typeof g == 'object' && typeof z == 'object') {
            var t = g[2] + z[1];
            var u = g[1] + z[2];
            var v = t * u;
            alert('Si ceci : ' + v + ' est un nombre vous avez réussi !');
            return v;
        } else {
            alert('Les types de données fournis sont incorrects !');
        };
    },
    mixerDesObjets: function (o, t) {
        if (typeof o == 'object' && typeof t == 'object') {
            var p = (o.a * o.b) / (t.x * t.y);
            alert('Si ceci : ' + p + ' est un nombre vous avez réussi !');
            return p;
        } else {
            alert('Les types de données fournis sont incorrects !');
        };
    },
    mixerDesFonctions: function (z, w) {
        if (typeof z == 'function' && typeof w == 'function') {
            z();
            w();
            alert('Bravo ! Les types de données fournis sont corrects .');
        } else {
            alert('Les types de données fournis sont incorrects !');
        };
    },
    mixerDeTout: function (k, l, m, n) {
        if (typeof k == 'string' && typeof l == 'object' && typeof m == 'function' && typeof n == 'number') {
            alert('Voici un message : ' + k);
            alert('Voici un message trouvé dans un objet : ' + l.message);
            m();
            alert('Mais ceci est un nombre : ' + n);
            return n;
        } else {
            alert('Les types de données fournis sont incorrects !');
        };
    }
};
blender.mixerDesNombres (2, 3, 4);
blender.mixerDesChaines ('naruto ', 'sasuke');
blender.mixerDesBooleens (true, true, true);
blender.mixerDesTableaux ([5, 4, 5], [4, 6, 7]);
blender.mixerDesObjets ({a:5,b:7},{x:9,y:7});
blender.mixerDesFonctions ((function(){alert(5)}),(function(){alert(6)}));
blender.mixerDeTout ('bonjour', {message:'lol'}, (function(){alert(10)}), 8);
