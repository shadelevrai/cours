/*

<body onload="window.alert('Ce message est affiché au chargement du document !')"> = window > document > body > attribute > 0 > nodeValue = "window.alert('Ce message est affiché au chargement du document !')" 


<li onmouseover="window.alert('Ce message est affiché lorsqu\'on survole ce point.');">Premier point</li> = document.body.children[1].children[0].attributes[0].nodeValue = "window.alert('Ce message est affiché lorsqu\\'on survole ce point.');"


<span onmouseout="window.alert('Ce message est affiché lorsqu\'on ne survole plus cet élément.')">1.2</span> = document.body.children[2].children[0].children[2].children[0].attributes[0].nodeValue = "window.alert('Ce message est affiché lorsqu\\'on ne survole plus cet élément.')"